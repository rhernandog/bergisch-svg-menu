/*
*******************************************************************************************
		BUTTONS MODULE
*******************************************************************************************
*/
// levels module
import {
	currentLevel, _changeCurrentLevelValue, menuTweensArray,
	reverseLevelsTweens, _setCurrentLevelSegmentsAmount,
	currentFirstLevelSelection
} from "./levels-module";
// assets menu module
import {
	isAssetVisible, assetsMenuTween, _resetAssetVisible,
	assetTriangle
} from "./assets-menu-module";
// text module
import { setTextSource } from "./text-module";
// draggable knob module
import {
	hideDragElements, placeKnob, knobClickHandler,
	updateLevelSegment
} from "./draggable-knob-module";
// image module
import { refImageTween } from "./image-module";
// pagination module
import {
	setPaginationData, showPaginationControls
} from "./pagination-module";
// data module
import { updateRefObject } from "./data-module";

export let closeButton;
// the position of the close button for each menu level
const closeBtnPositions = [
	null,
	{ x: 730, y: 250 },
	{ x: 338, y: 110 }
];
// the positions of the close button when the assets menu is visible
const closeAssetsPositions = {
	2: { x: 850, y: 550 },
	3: { x: 870, y: 680 }
};
/* Bool to check if the menu instances are reversing because
 * the close button was clicked or the first level element was
 * clicked.
*/
export let isCloseClicked = false;

/** Method to set the closed clicked boolean
 * @param {boolean} v the bool value for the variable
 * @private
*/
export const _setClosedClicked = v => isCloseClicked = v;

/** Method to position the close button.
 * Uses the current level to set the position of the close button
 * according to the coordinates in the positions array.
 * @private
*/
export const setCloseButton = () => {
	// get the position for this level
	// check if the assets menu is visible, in that case get the position
	// for the current level 
	const currentBtnPosition = isAssetVisible ? closeAssetsPositions[currentLevel] : closeBtnPositions[currentLevel];
	TweenLite.set(closeButton, {
		x: currentBtnPosition.x, y: currentBtnPosition.y
	});
};

/** Method for the close button click handler.
 * When the user clicks on the close button, the drag path, knob and the close
 * button should fade out.
 * Then the current level tween should be reversed.
 * Then the drag paths should be updated and the knob position should change
 * for the current level.
 * Finally the current level index should be updated.
 * @private
*/
const closeBtnHandler = () => {
	/* If the assets menu was visible and the user clicked the close button
	 * then hide the assets menu, the drag elements and the close button,
	 * set the new position of the close button and drag elements and finally
	 * show the drag elements and the close button
	*/
	if (isAssetVisible) {
		TweenLite.set(assetTriangle, { autoAlpha: 0 });
		// hide the elements
		hideDragElements();
		// hide the assets menu
		assetsMenuTween.reverse();
		// update level index
		_changeCurrentLevelValue(false);
		updateLevelSegment(false);
		// set the assets menu bool to false
		_resetAssetVisible( false );
		// show the pagination controls
		showPaginationControls();
		// place the drag and close button
		return placeKnob();
	}
	isCloseClicked = true;
	// hide the elements
	hideDragElements();
	// reverse the current level
	menuTweensArray[currentLevel] ? reverseLevelsTweens(null, true) : null;
	// update the current level index
	_changeCurrentLevelValue(false);
	// update the text source
	setTextSource();
	// set the pagination data
	setPaginationData(true);
	updateLevelSegment(false);
	_setCurrentLevelSegmentsAmount();
	// check the current level and hide the image wrapper if the level is 0
	// hide the image if the first level is customer, since customers don't
	// have a thumb in their data, just references have thumb
	if (
		currentLevel === 0 ||
		( currentLevel === 1 && currentFirstLevelSelection == "customer" )
	) refImageTween.reverse();
	// if the current level is 0, means that the reference object should be empty
	if ( currentLevel === 0 ) updateRefObject(null);
};

/* PLAY BUTTON */
let playBtn;
/** Play Button Click Handler.
 * Check the current menu level and uses the knob click handler
 * @private
*/
const playBtnHandler = () => {
	// first check if the current level is more than 0
	// if the current level is more than 0, the use the knob click handler
	// other wise do nothing
	if (currentLevel > 0) {
		knobClickHandler();
	}
};

/** Method to init the Buttons Module.
 * Creates the variables after the DOM nodes are present.
 * @private
*/
export const _initBtnModule = () => {
	// create the references once the DOM elements are placed
	closeButton = document.getElementById("close-button");
	playBtn = document.getElementById("play-btn");
	// add the close button click handler
	closeButton.onclick = closeBtnHandler;
};
