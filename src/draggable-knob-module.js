/*
*******************************************************************************************
		DRAGGABLE KNOB MODULE
*******************************************************************************************
*/
// data module
import {
	createNewRefs, _setSelectedCustomer, updateRefObject,
	customersReferences, selectedCustomer
} from "./data-module";
// buttons module
import { closeButton, setCloseButton, _setClosedClicked } from "./buttons-module";
// levels module
import {
	currentLevel, levelPathsArrays,
	currentLevelSegmentsAmount, currentFirstLevelSelection,
	createArcSegments, _changeCurrentLevelValue
} from "./levels-module";
// text module
import { applyElementText, currentTextSource, setTextSource } from "./text-module";
// image module
import { selectedLevelImages, updateImageSrc, refImageTween } from "./image-module";
// assets menu module
import {
	assetsDragArrays, assetsDragPaths, createAssetsMenu,
	_resetAssetVisible, assetsLines, totalAssets,
	isAssetVisible, updateTrianglePos
} from "./assets-menu-module";
// pagination module
import {
	setPaginationData, paginationData,
	showPaginationControls
} from "./pagination-module";

// draggable knob
let draggableKnob;
// drag path container
let dragPathContainer;
// drag path track, this is always visible
let dragPathTrack;
// drag path reveal, this is revealed as the user drags the knob
let dragPathReveal;
// the timeline for the bezier and draw
let dragLine = new TimelineLite({ paused: true });
// the path reveal tween
let pathRevealTween;


/* Array with all the drag path strings.
 * Each element is the strig of each drag path tag, that's used in
 * the d attribute of the drag path. When a new level is selected,
 * we update the attribute of the drag path track, position the drag
 * knob and then reveal the elements.
*/
const dragPathStrings = [
	// level 0 no drag knob visible
	null,
	// level 1
	"M312.744,269.045 C352.505,200.273, 426.849,154, 512,154 c85.151,0, 159.495,46.273, 199.257,115.046",
	// level 2
	"M371.976,626.527C288.297,578.11,232,487.629,232,384 c0-103.63,56.297-194.111,139.977-242.527",
	// level 3
	"M702.029,54.854 C815.595,120.562,892,243.358,892,384c0,69.228-18.512,134.132-50.856,190.032"
];

/* Each level has it's own drag path, that drag path is described
 * as a cubic bezier path, so we get those paths and create the arrays
 * for each path, in order to use them for the draggable knob when
 * each level is visible.
*/
const dragPathsArrays = [
	null, // level 0
	// level 1
	[
		{ x: "282.744", y: "239.045" }, // start point
		{ x: "322.505", y: "170.273" }, { x: "396.849", y: "124" }, // first set of control points
		{ x: "482", y: "124" }, // second point
		//    c85.151,0,                       159.495,46.273,
		{ x: "567.151 ", y: "124" }, { x: "641.495", y: "170.273" }, // second ste of control points
		// 199.257          115.046
		{ x: "677.657", y: "235.446" } // final point
	],
	// level 2
	[
		{ x: "341.976", y: "596.527" }, // start point
		{ x: "258.297", y: "548.11" }, { x: "202", y: "457.629" },// first set of control points
		{ x: "202", y: "354" }, // second point
		{ x: "202", y: "250.37" }, { x: "258.297", y: "159.889" }, // second set of control points
		{ x: "341.977", y: "111.472" } // final point
	]
];

/* When the user selects a reference to show it's assets
 * store that reference in order to access that info when the
 * users selects a specific asset
*/
let selectedRef;

/* When the knob position is updated, the corresponding element
 * of the current level shold be white and the previous one should
 * go back to the grey color.
*/
export let currentLevelElement = 0;
// same as current level element is the current asset element
export let currentAssetsElement = 0;

/** Method to toggle the knob disable state
 * Changes the state of the draggable knob to disabled or not
 * depending on the value passed.
 * @param {boolean} disable
 * @private
*/
const toggleKnobDisableState = disable => {
	TweenLite.set(draggableKnob, { opacity: disable ? 0.35 : 1 });
};

/** Method to reset the current asset element.
 * After the close button is clicked, the current assets element
 * index vaule reflects the last ref's assets. If a new ref is
 * selected they update could trigger an error.
 * Use this method when the assets menu is created.
 * @private
*/
export const resetCurrentAssetsElement = () => {
	currentAssetsElement = 0;
};
/* Object to store the index of the segment of each level.
 * When the user calls the drag knob click handler the index
 * value of the current level will be updated. This will be 
 * used then to remove the highlight color and glow from the
 * segment of the corresponding level.
 * When the close button is clicked, the value of the current
 * level will be set to 0.
 * By default the values are 0 for each level
*/
let levelsSegmentIndex = { 1: 0, 2: 0 };
/** Method to reset the current level element
 * Sets the current level element to 0 when a new level is opened
 * In case of a paginatin event executing this method, instead of 0
 * we set the value to the target. This because in a new group the
 * target index will be the first element of the new group, but the
 * data is just one array, so while the current target could be 39
 * the next group first element's index is 40 and not 0. Going to a
 * previous group is the same, the index of thefirst element of that
 * group is 40 but the first of the previous group is 0.
 * @param {number} target the number the element should be set
 * @private
*/
export const _resetCurrentLevelElement = target => {
	currentLevelElement = target || 0;
};

// the draggable dummy element
const dragDummy = document.createElement('div');

/** Method to update the element for the current level.
 * When the user drags the knob, the level segments will change
 * color to indicate the selected one, and the level's text container
 * will be updated with the corresponding text.
 * @param {number} target the index of the target element
 * @private
*/
export const updateLevelSegment = target => {
	// check if the update is because the close button was clicked
	// set the color of the target element to white
	if ( target === false ) {
		// if the current level is 0, the menu base reset the current element index
		// and stop the code
		if ( currentLevel === 0 ) {
			// hide the image wrapper
			refImageTween.reverse();
			return currentLevelElement = 0;
		}
		// if the current level is 1 or 2, we need to remove the style
		// for the selected element and update the level's segment index 
		// value in the levels segment index
		if ( currentLevel === 1 || currentLevel === 2 ) {
			// update the text for the level
			applyElementText(true);
			// update the image source to the first element of the selected
			// references.
			updateImageSrc( currentTextSource[0].thumb );
			// the current level's path array
			const currentPathArray = levelPathsArrays[currentLevel];
			// remove the style of the selected element
			TweenLite.set(currentPathArray[levelsSegmentIndex[currentLevel]], {
				className: "-=selected-segment"
			});
			// set the first segment of the level as the selected element
			TweenLite.set(currentPathArray[0], {
				className: "+=selected-segment"
			});
			// set the level's segment index to 0
			levelsSegmentIndex[currentLevel] = 0;
		}
		return currentLevelElement = 0;
	}
	if (target !== currentLevelElement) {
		/* Since the app has pagination, we need to correct the target value
		 * with the current pagination group index in order to reflect the
		 * real target of the target path array. The path array has all the elemenets
		 * of the current level/selection (could be more than 40), but the visible
		 * elements are never more than 40, so we correct that. The visual update
		 * is for the visible element but the text and reference object is for the
		 * element contained in the 
		*/
		const correctedTarget = target + (
			40 * (paginationData[currentLevel].currentGroup - 1)
		);
		const targetPathArray = levelPathsArrays[currentLevel];
		// the new target reference/customer
		const targetRefCus = currentTextSource[correctedTarget];
		/* Check if the target element (from the text source) has assets in it.
		 * This also means that we have to check if the current level is either
		 * 1 or 2 and also that the current text source has an assets property
		 * in it. If the assets array is empty, set the knob state to disabled
		 * by changing it's alpha channel to 0.75
		*/
		if ( targetRefCus.assets ) {
			toggleKnobDisableState( targetRefCus.assets.length === 0 );
		}
		// update the knob position
		// update the reference object
		updateRefObject( currentTextSource[correctedTarget] );
		// set the color of the target element to white
		TweenLite.set(targetPathArray[target], {
			className: "+=selected-segment"
		});
		// set the color of the current level to the default
		TweenLite.set(targetPathArray[currentLevelElement], {
			className: "-=selected-segment"
		});
		// set the current level var
		currentLevelElement = target;
		// update the text for the level
		applyElementText(false, correctedTarget);
		// update the image source
		updateImageSrc( currentTextSource[correctedTarget].thumb );
	}
};


/** Method to update the second level drag Timeline.
 * Updates the timeline using the x position of the draggable instance
 * and the width of the drag path container.
 * @private
*/
const updateHorDragLine = function () {
	const dragVal = this.x;
	const pathContainerDim = dragPathContainer.getBBox().width;
	const currentProgress = dragVal / pathContainerDim;
	// update the progress of the GSAP instances
	dragLine.progress(currentProgress);
	pathRevealTween.progress(currentProgress);
	const targetElement = Math.floor(
		(currentProgress * (currentLevelSegmentsAmount - 1)) + 0.4
	);
	// update the segment
	updateLevelSegment(targetElement);
};


/** Method to update the third level drag Timeline.
 * This updates the progress of the drag timeline using the y value
 * of the draggable instance and the height of the drag path container.
 * Since the knob in this level goes from the bottom of the path to the
 * top, we use the negative value of the instance.
 * @private
*/
const updateVertDragLine = function () {
	const dragVal = -this.y;
	const pathContainerDim = dragPathContainer.getBBox().height;
	const currentProgress = dragVal / pathContainerDim;
	// update the progress of the GSAP instances
	dragLine.progress(currentProgress);
	pathRevealTween.progress(currentProgress);
	const targetElement = Math.floor(
		(currentProgress * (currentLevelSegmentsAmount - 1)) + 0.4
	);
	// update the segment
	updateLevelSegment(targetElement);
};

/** Method to update the assets drag Timeline.
 * This updates the progress of the drag timeline using the y value
 * of the instance and the height of the drag path container.
 * Since the knob goes from the top to the bottom, we use the positive
 * vale of the instance.
 * @private
*/
const updateAssetsDragLine = function () {
	const dragVal = this.y;
	const pathContainerDim = dragPathContainer.getBBox().height;
	const currentProgress = dragVal / pathContainerDim;
	// update the progress of the GSAP instances
	dragLine.progress(currentProgress);
	pathRevealTween.progress(currentProgress);
	const targetElement = Math.floor( (currentProgress * totalAssets) + 0.25);
	// check that the target is different from the current selected element
	if ( targetElement > 0 && targetElement !== currentAssetsElement ) {
		TweenLite.set(assetsLines[targetElement], { className: "+=selected" });
		TweenLite.set(assetsLines[currentAssetsElement], { className: "-=selected" });
		updateTrianglePos(targetElement);
		currentAssetsElement = targetElement;
		// update the reference object
		updateRefObject( selectedRef.assets[targetElement - 1] );
		// update the image with the asset thumb
		updateImageSrc( selectedRef.assets[targetElement - 1].thumb );
	}
};

/** Method to create the Drag bounds.
 * Sets the drag bounds of the knob draggable instance, depending
 * on the level and if the assets menu is visible or not.
 * @param {boolean} assets if the assets menu is visible or not
 * @returns {object} the object with the bounds
 * @private
*/
const createDragBounds = assets => {
	if (currentLevel === 1) {
		return {
			minX: 0, maxX: dragPathContainer.getBBox().width,
			minY: 0, maxY: dragPathContainer.getBBox().height
		};
	} else if (currentLevel > 1 && !assets) {
		return {
			minX: 0, maxX: dragPathContainer.getBBox().width,
			minY: -dragPathContainer.getBBox().height, maxY: 0
		};
	} else if (currentLevel > 1 && assets) {
		return {
			minX: 0, maxX: dragPathContainer.getBBox().width,
			minY: 0, maxY: dragPathContainer.getBBox().height
		};
	}
};

/** Method to place the knob
 * This is used to place the knob when a new level is available.
 * After a new level is made available, call this method to place
 * the knob at the starting point of that level's drag path.
 * After placing the knob, create the draggable instance and then
 * create the live-snap array values considering the amount of elements
 * of that level.
 * The param passed indicates if the string and array for the path and
 * bezier are the ones of the menu level or the assets for that particular
 * level.
 * @param {bool} assets wheater the assets menu will be visible
 * @private
*/
export const placeKnob = assets => {
	/* update the reference object only if the level is 1
	 * if the level is not 1, the ref object will be updated in the
	 * knob click handler, so there's no need to run that code here
	 * as well, since creates a duplicate.
	*/
	if ( currentLevel === 1 ) updateRefObject( currentTextSource[currentLevelElement] );
	/* Check if the assets menu should be shown.
	 * Check the current level, in order to set the start position
	 * of the knob.
	*/
	let dragBezierArray, dragPathCurve;
	if (assets) {
		// set the selected reference
		selectedRef = currentTextSource[currentLevelElement];
		// reset the current asset element
		currentAssetsElement = 0;
		// set the target bezier curves
		dragBezierArray = assetsDragArrays[currentLevel];
		dragPathCurve = assetsDragPaths[currentLevel];
		// create the assets menu
		createAssetsMenu( selectedRef.assets, selectedRef.asset_count );
	} else {
		dragBezierArray = dragPathsArrays[currentLevel];
		dragPathCurve = dragPathStrings[currentLevel];
		// reset the current level element index
		currentLevelElement = 0;
	}
	/* In order to avoid an issue when the drag knob jumps to an unexpected position
	 * clear the drag dummy props. If a new Draggable instance is created and the
	 * position of the knob at that moment is not the 
	*/
	TweenLite.set([dragDummy], { clearProps: "all" });
	// place the knob in the start of the drag path
	TweenLite.set(draggableKnob, {
		x: dragBezierArray[0].x,
		y: dragBezierArray[0].y
	});
	// add the string to the d attribute in the drag path containers
	dragPathTrack.setAttribute("d", dragPathCurve);
	dragPathReveal.setAttribute("d", dragPathCurve);
	// clear the start values of the drag reveal tween
	TweenLite.set(dragPathReveal, { drawSVG: "100%" });
	pathRevealTween.kill();
	pathRevealTween = TweenLite.from(dragPathReveal, 1, {
		drawSVG: "0%", ease: Linear.easeNone, paused: true
	});
	/* Clear and assign the drag timeline to the new instances
	 * based on the target level
	*/
	dragLine.kill().clear();
	pathRevealTween.pause(0);
	// test a bezier tween
	dragLine
		.to(draggableKnob, 1, {
			bezier: { type: "cubic", values: dragBezierArray },
			ease: Linear.easeNone
		}, 0);
	//
	// depending on the level, set the callback of the instance
	const dragCallback = (currentLevel === 1 ? updateHorDragLine : (
		currentLevel > 1 && !assets ? updateVertDragLine : updateAssetsDragLine
	));
	// now create the draggable instance
	Draggable.create(dragDummy, {
		trigger: draggableKnob,
		// dragResistance: 0.05,
		onClick: knobClickHandler,
		type: "x, y",
		// bounds: dragBounds,
		bounds: createDragBounds(assets),
		onDrag: dragCallback
	}); // draggable
	// position the close button
	setCloseButton();
	// now show the drag elements
	showDragElements();
};
// the instances that show and hide the drag elements
// we include the close button
let showDragTween;

/** Method to hide the drag path and knob
 * 
*/
const showDragElements = () => {
	// reset the close button bool
	_setClosedClicked(false);
	// show the drag elements
	showDragTween.play();
};

/** Method to show the drag path and knob
 * 
*/
export const hideDragElements = () => showDragTween.reverse();

/** Draggable Knob Click Handler
 * Increases the current level index.
 * The knob is visible starting at the second level and depending
 * on the first level selection, the type of third level it'll create.
 * If the first level selection is "references" then the third level
 * will be the assets of that particular reference. If the first level
 * selection is customer or highlight, then the third level will be either
 * the references of that customer, or all references.
 * If the current level is the third then the click handler will create the
 * fourth level with the assets of that particular reference.
 * @private
*/
export const knobClickHandler = () => {
	/* Update the reference object of the global element
	 * If the assets menu is visible, then update with the corresponding
	 * asset. If the assets menu is not visible, update with the currently
	 * selected reference.
	 * Also if the assets menu is visible, hide the pagination controls.
	*/
	if ( isAssetVisible ) return updateRefObject(selectedRef);
	// if the level is 1 or 2 store the index of the selected segment
	// also store the selected image url
	if ( currentLevel === 1 || currentLevel === 2 ) {
		levelsSegmentIndex[currentLevel] = currentLevelElement;
		// update the image level in the object
		selectedLevelImages[currentLevel] = currentTextSource[currentLevelElement].thumb;
	}
	// increase the level
	_changeCurrentLevelValue(true);
	// hide the drag knob/paths and the close button
	hideDragElements();
	// the first level is refs or the current level is the third
	// show the assets menu for the selected item
	if (currentFirstLevelSelection === "references" || currentLevel > 2) {
		// update the reference object
		updateRefObject(currentTextSource[currentLevelElement]);
		// if the current element doesn't have assets, don't create the menu
		// and show an alert
		if ( currentTextSource[currentLevelElement].assets.length === 0 ) {
			_changeCurrentLevelValue(false);
			return showDragElements();
		}
		// toggle the assets visible bool
		_resetAssetVisible( true );
		// hide the pagination controls
		showPaginationControls();
		// place the drag elements and close button
		return placeKnob(true);
	}

	/* Set the items for the level depending on the first level selection.
	 * For costumers, get the customer id and get the customer's references.
	 * For highlight get the index of the current level, create a new array for the
	 * references with the selection as the first element (which will be removed)
	*/
	/* If the selection is highlight, create a new references array
	 * Since the first element in the next level will be the same reference
	 * there is no need to update the reference object
	*/
	if (currentFirstLevelSelection === "highlight") {
		createNewRefs();
	}
	// if the selection is customer set the selected customer
	if (currentFirstLevelSelection === "customer") {
		// set the selected customer id to the selected element
		_setSelectedCustomer( currentTextSource[currentLevelElement].id );
		/* Update the reference object, using the first ref from the 
		 * references of the currently selected customer. All the customers
		 * references are placed in an object. Get the first array element
		 * of the selected customer in the object
		*/
		updateRefObject(customersReferences[selectedCustomer][0]);
	}

	// update the text source
	setTextSource();
	// before creating the segments set the pagination data
	// in case the current selection has more than 40 elements
	setPaginationData();
	// create the segments of the current level
	createArcSegments();
	// place the drag elements and close button
	placeKnob();
};

/** Method to init the knob module.
 * Sets the initial state of the knob and creates the GSAP instance
 * to show/hide the drag elements and the close button
 * @private
*/
export const _initKnobModule = () => {
	// create the DOM elements vars
	// draggable knob
	draggableKnob = document.getElementById("draggable-knob");
	// drag path container
	dragPathContainer = document.getElementById("drag-path-container");
	// drag path track, this is always visible
	dragPathTrack = document.getElementById("drag-path-track");
	// drag path reveal, this is revealed as the user drags the knob
	dragPathReveal = document.getElementById("drag-path-reveal");
	// the path reveal tween
	pathRevealTween = TweenLite.from(dragPathReveal, 1, {
		drawSVG: "0%", ease: Linear.easeNone, paused: true
	});
	// by default the drag path container and the knob are hidden
	TweenLite.set([draggableKnob, dragPathContainer, closeButton], {
		autoAlpha: 0
	});
	// create the instance to show the elements
	showDragTween = TweenLite.to([draggableKnob, dragPathContainer, closeButton], 0.2, {
		autoAlpha: 1, paused: true
	});
};
