/*
*******************************************************************************************
		TEXT MODULE
*******************************************************************************************
*/
// get the levels module
import { currentLevel, currentFirstLevelSelection } from "./levels-module";
// get the references and customers
import {
	appReferences, appCustomers, highlightRefs, newHighlightRefs,
	customersReferences, selectedCustomer
} from "./data-module";
// get the current level element
import { currentLevelElement } from "./draggable-knob-module";

/* When a new level is selected the level's segments are visible.
	 * By defatul the first segmente is selected and it's text is displayed
	 * along the level's text guide path.
	*/
/* Level menu text containers
 * These wrap the text elements and will be animated with the menu
 * containers after a selection is made.
*/
export let secondLevelTextWrap, thirdLevelTextWrap, secondLevelText, thirdLevelText;

/* Depending on the first level selection, the text source for the 
 * current level of the menu. We store the target data source in order
 * to prevent selecting it everytime the drag update selects a new
 * item of the current level
*/
export let currentTextSource = null;

const logTextSource = () => {
	// console.log( "---------------\nset text source" );
	// console.log( "current level => ", currentLevel );
	// console.log( currentTextSource );
};

/** Method to set the current text source.
 * Depending on the first level selection the text source.
 * Everytime a new first level selection is made the text source
 * is updated.
 * @private
*/
export const setTextSource = () => {
	// for the second and third level the text source will be either
	// the modified highlight references or the selected customer
	// references
	if (currentLevel > 1 && currentFirstLevelSelection === "customer") {
		currentTextSource = customersReferences[selectedCustomer];
		return logTextSource();
	}
	if (currentLevel > 1 && currentFirstLevelSelection === "highlight") {
		currentTextSource = newHighlightRefs;
		return logTextSource();
	}
	switch (currentFirstLevelSelection) {
		case "references":
			currentTextSource = appReferences;
			return logTextSource();
		case "highlight":
			currentTextSource = highlightRefs;
			return logTextSource();
		case "customer":
			currentTextSource = appCustomers;
			return logTextSource();
	} // switch
};

/** Method to update the current level's text
 * Gets the current level and the current level element or segmen
 * and applies the string from the data collection corresponding to 
 * the level and the element.
 * The index param is beacuse with the pagination system in place 
 * @param {boolean} close
 * @param {number} index the index value.
 * @private
*/
export const applyElementText = (close, target) => {
	// first get the target text element depending on the level
	let targetTextContainer;
	if (currentLevel === 1) {
		targetTextContainer = secondLevelText;
	} else if (currentLevel === 2) {
		targetTextContainer = thirdLevelText;
	} // level conditional
	// clear the text of the target
	targetTextContainer.innerHTML = "";
	// get the text for the current element of the level
	// for the index value use either 0(when the method is executed from the close
	// handler) or the passed target (for a pagiantion )
	// const targetText = secondLevelDummyText[currentFirstLevelSelection];
	const targetText = currentTextSource[close ? 0 : (target || currentLevelElement)].name;
	// apply the text to the container
	targetTextContainer.innerHTML = targetText;
}; // apply element text

/** Method to Init the Text Module
 * Creates the variables after the DOM nodes are present
 * @private 
*/
export const _initTextModule = () => {
	secondLevelTextWrap = document.getElementById("second-level-text-wrap");
	thirdLevelTextWrap = document.getElementById("third-level-text-wrap");
	secondLevelText = document.getElementById("second-level-text");
	thirdLevelText = document.getElementById("third-level-text");
};
