/*
*******************************************************************************************
		ASSETS MENU MODULE
*******************************************************************************************
*/
import { currentLevel } from "./levels-module";
import { resetCurrentAssetsElement } from "./draggable-knob-module";


// the asset menu and element containers
// this holds the menu base arc
let assetMenuContainer;
// this holds the menu elements and separator lines
let assetElsContainer;
// the assets menu wrapper
export let fourthLevelMenu;
// the assets separator labels
let assetsLabels;
// the asset triangle indicator
export let assetTriangle;
/* The triangle should be positioned as the user drags the knob while the
 * assets menu is open. For this we need the position of each asset element
 * as well as the rotation. Use an array to add those points as objects.
*/
export let trianglePoints = [];
/* Since some references won't have some asset type, we need a way to 
 * know if some label shouldn't be visible for the selected ref.
 * In this array we hold the a copy of the label elements that should
 * be visible in the assets menu so the getPoints method uses this instead
 * of the entire labels collection. This array is set when the seprartor
 * marks are created.
*/
let visibleLabels = [];
// the assets array for the selected reference
let assestArray;
// assets lines array
// used to update the line stye as the use drags the knob
export let assetsLines = [{}];
// assets amount to update the style of the line when dragging the knob
export let totalAssets = 0;
/* the assets menu animation, this will be added to the menu tweens array
 * in case the user has a particular reference visible and wants to change
 * the first level selection.
*/
export let assetsMenuTween;

/* Boolean to indicate if the assets menu is being created. This will be
 * used in the draggable create method in order to accomodate the value
 * of the onDrag callback. In level 3 uses the negative value of y because
 * the knob is dragged from the bottom up. In the assets menu draggable, the
 * knob is dragged from the top to the bottom, so we need the positive value
 * of y in the callback. So instead of doing a conditional check on each drag
 * update, we'll use a different method on the callback.
*/
export let isAssetVisible = false;
/** Method to reset the asset menu visible boolean.
 * Sets the boolean to the passed value
 * @param {boolean} v the value for the boolean.
 * @private
*/
export const _resetAssetVisible = v => isAssetVisible = v;

/* Depending on the current level the assets menu can have two radius
 * for it's path. Also depending on the amount of items, each path could
 * be short or long. For each level there's an array with the short and 
 * long paths.
*/
const assetsMenuPaths = {
	2: "M622.082,193.479 C687.795,231.53,732,302.603,732,384c0,40.106-10.732,77.706-29.48,110.083",
	/* [
		"M622.082,193.479 C687.795,231.53,732,302.603,732,384c0,40.106-10.732,77.706-29.48,110.083",
		"M622.082,193.479 C687.795,231.53,732,302.603,732,384c0,81.423-44.234,152.516-109.982,190.557"
	], */
	3: "M709.988,186.008 C760.659,236.678,792,306.679,792,384c0,77.32-31.34,147.32-82.01,197.99"
	/* [
		"M651.997,141.511 c62.059,35.829,110.45,95.333,130.462,170.019c20.012,74.685,7.857,150.411-27.972,212.469"
	] */
};

/* In order to draw each element's line in the assets menu, we need the exact
 * point (x,y) on the specific curve. Since the test with the circle parametric
 * equation didn't worked, we'll use three different bezier tweens in order to
 * get the points for each level.
*/
const assetsLineBeziers = {
	// level two, first level is references
	2: [
		// menu base
		[
			{ x: 622.024, y: 193.479 },
			{ x: 687.795, y: 231.53 }, { x: 732, y: 302.603 },
			{ x: 732, y: 384 },
			{ x: 732, y: 424.106 }, { x: 721.268, y: 461.706 },
			{ x: 702.52, y: 494.083 }
		],
		// assets line
		[
			{ x: 632.021, y: 176.12 },
			{ x: 703.745, y: 217.62 }, { x: 752, y: 295.175 },
			{ x: 752, y: 384 },
			{ x: 752, y: 427.723 }, { x: 740.308, y: 468.715 },
			{ x: 719.881, y: 504.02 }
		],
		// separators line
		[
			{ x: 637.022, y: 167.458 },
			{ x: 714.262, y: 205.989 }, { x: 762, y: 291.474 },
			{ x: 762, y: 384 },
			{ x: 762, y: 429.544 }, { x: 749.821, y: 472.244 },
			{ x: 728.543, y: 509.02 }
		],
		// triangle position
		[
			{ x: 619.519, y: 197.774 },
			{ x: 683.772, y: 234.951 }, { x: 727, y: 304.427 },
			{ x: 727, y: 384 },
			{ x: 727, y: 423.168 }, { x: 716.526, y: 459.89 },
			{ x: 698.226, y: 491.518 }
		]
	],
	// level three, first level is highlight or costumer
	3: [
		// menu base
		[
			{ x: 709.988, y: 186.008 },
			{ x: 760.659, y: 236.678 }, { x: 792, y: 306.679 },
			{ x: 792, y: 384 },
			{ x: 792, y: 461.32 }, { x: 760.66, y: 531.32 },
			{ x: 709.99, y: 581.99 }
		],
		// assets line
		[
			{ x: 724.13, y: 171.865 },
			{ x: 778.42, y: 226.155 }, { x: 812, y: 301.156 },
			{ x: 812, y: 384 },
			{ x: 812, y: 466.843 }, { x: 778.421, y: 541.843 },
			{ x: 724.132, y: 596.132 }
		],
		// separators line
		[
			{ x: 731.201, y: 164.794 },
			{ x: 787.301, y: 220.893 }, { x: 822, y: 298.394 },
			{ x: 822, y: 384 },
			{ x: 822, y: 469.604 }, { x: 787.302, y: 547.104 },
			{ x: 731.203, y: 603.203 }
		],
		// triangle points
		[
			{ x: 706.452, y: 189.543 },
			{ x: 756.219, y: 239.309 }, { x: 787, y: 308.06 },
			{ x: 787, y: 384 },
			{ x: 787, y: 459.939 }, { x: 756.22, y: 528.689 },
			{ x: 706.454, y: 578.454 }
		]
	]
};

/* To get the points we use this arrays in a GSAP instance and for a specific
 * progress amount (that coresponds to each asset or separator) we get the
 * points. For each level there are 3 objects one to get the base point, the
 * asset final point (short line) and the separator final point (long line)
*/
const assetCurveObjects = {
	2: {
		base: { x: 0, y: 0 },
		asset: { x: 0, y: 0 },
		separator: { x: 0, y: 0 },
		triangle: { x: 0, y: 0 },
		rotation: 0
	},
	3: {
		base: { x: 0, y: 0 },
		asset: { x: 0, y: 0 },
		separator: { x: 0, y: 0 },
		triangle: { x: 0, y: 0 },
		rotation: 0
	}
};

/* The assets menu has a start and end separator that are always in the same
 * place, so we don't need to get those points from the bezier curves.
*/
const assetMenuExtremeLines = {
	2: [
		{ x1: 622.024, y1: 193.479, x2: 637.022, y2: 167.458 },
		{ x1: 702.52, y1: 494.083, x2: 728.543, y2: 509.02 }
	],
	3: [
		{ x1: 709.988, y1: 186.008, x2: 731.201, y2: 164.794 },
		{ x1: 709.99, y1: 581.99, x2: 731.203, y2: 603.203 }
	]
};

/* Finally create a GSAP instance (timeline) for each level in order to
 * update it's progress and get the points for the base and the type of
 * line, at a given angle.
*/
// second level
const levelTwoBezierLine = new TimelineLite({ paused: true });
// add the instances to the second level line
levelTwoBezierLine
	.to(assetCurveObjects[2].base, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[2][0], autoRotate: true
		}
	}, 0)
	.to(assetCurveObjects[2].asset, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[2][1]
		}
	}, 0)
	.to(assetCurveObjects[2].separator, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[2][2], autoRotate: true
		}
	}, 0)
	.to(assetCurveObjects[2].triangle, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[2][3], autoRotate: true
		}
	}, 0);
// 

// third level
const levelThreeBezierLine = new TimelineLite({ paused: true });
levelTwoBezierLine
	.to(assetCurveObjects[3].base, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[3][0], autoRotate: true
		}
	}, 0)
	.to(assetCurveObjects[3].asset, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[3][1]
		}
	}, 0)
	.to(assetCurveObjects[3].separator, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[3][2], autoRotate: true
		}
	}, 0)
	.to(assetCurveObjects[3].triangle, 1, {
		ease: Linear.easeNone, bezier: {
			type: "cubic", values: assetsLineBeziers[3][3], autoRotate: true
		}
	}, 0);
// 

/** Method to position the triangle.
 * @param {number} index the index position of the current target
 * @private
*/
export const updateTrianglePos = index => {
	if ( index > 0 ) {
		TweenLite.set( assetTriangle, {
			...trianglePoints[index], autoAlpha: 1
		});
	}
};

/** Method to create the asset menu lines
 * Draws the lines using the start and end point of each line, and
 * adds them to the asset menu container. All the container's child
 * elements must be removed before calling this code for the first time.
 * @param {object} points an object with the start and end points
 * @private
*/
const createElementLines = points => {
	const elementLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
	TweenLite.set(elementLine, {
		className: "+=asset-element",
		attr: {
			x1: points.x1, x2: points.x2,
			y1: points.y1, y2: points.y2
		}
	});
	assetElsContainer.appendChild(elementLine);
	return elementLine;
};

/** Method to get the points for each asset line.
 * Uses the assets count object to get the total assets and loops through
 * the total amount of lines (asset and separator) to get the points of the
 * bezier curves, corresponding to that specific element.
 * @param {object} assetsObject the asset count object
 * @private
*/
const getPoints = assetsObject => {
	// clear the triangle position points array
	trianglePoints = [{}];
	// reset the asset lines array
	assetsLines = [{}];
	// the separators marks
	const sepratatorMarks = createSeparators(assetsObject);
	/* To position the separator labels, we need to keep track of the
	 * current separator that has to be placed. After placing it we go
	 * to the next label in the collection. The collection is 0 index
	 * and the first label is placed when the menu is created, so 
	 * we start at 1.
	*/
	let currentLabel = 1;
	// total assets is the count plus 3, because of the separators
	totalAssets = getTotalAssets(assetsObject);
	const totalLines = totalAssets + ( visibleLabels.length - 1 );
	// the progress fraction according to the total lines
	const progressUnit = 1 / (totalLines + 1);
	// let lineIndex = 0;
	for (let i = 0; i < totalLines; i++) {
		// set the progress of the timeline
		levelTwoBezierLine.progress(progressUnit * (i + 1));
		// draw the line using the start and end points
		if ( !sepratatorMarks[i] ) {
			const { base, asset, triangle } = assetCurveObjects[currentLevel];
			// add the points to the triangle position array
			// only for the assets, not the indicators
			trianglePoints.push({
				x: triangle.x,
				y: triangle.y,
				rotation: triangle.rotation,
				transformOrigin: "top center"
			});
			// add the asset line element to the array
			assetsLines.push(
				createElementLines({
					x1: base.x,
					y1: base.y,
					x2: asset.x,
					y2: asset.y
				})
			);
		} else {
			if ( !visibleLabels[currentLabel] ) return;
			// place the separator label
			TweenLite.set(visibleLabels[currentLabel], {
				x: assetCurveObjects[currentLevel].separator.x + 5,
				y: assetCurveObjects[currentLevel].separator.y + (2 * (currentLabel - 1)),
				rotation: assetCurveObjects[currentLevel].separator.rotation - 90,
				transformOrigin: "left center",
				display: "block"
			});
			// go to the next label
			currentLabel++;
			// draw the separator line
			createElementLines({
				x1: assetCurveObjects[currentLevel].base.x,
				y1: assetCurveObjects[currentLevel].base.y,
				x2: assetCurveObjects[currentLevel].separator.x,
				y2: assetCurveObjects[currentLevel].separator.y
			});
		}
	} // loop
};

/* The assets menu has two different drag paths radius depending on the
 * current level. Also has a short and long drag paths depending on the
 * amount of items.
*/
export const assetsDragPaths = {
	2: "M682.029,89.503C783.639,148.295,852,258.164,852,384 c0,61.941-16.563,120.013-45.502,170.028",
	3: "M808.984,87.014 C884.99,163.019,932,268.02,932,384c0,115.98-47.01,220.98-123.015,296.985"
};

// the bezier arrays for each level assets drag
export const assetsDragArrays = {
	2: [
		{ x: 652.029, y: 59.503 },
		{ x: 753.639, y: 118.295 }, { x: 822, y: 228.164 },
		{ x: 822, y: 354 },
		{ x: 822, y: 415.94 }, { x: 805.437, y: 474.012 },
		{ x: 776.498, y: 524.028 }
	],
	3: [
		{ x: 778.984, y: 57.014 },
		{ x: 854.99, y: 133.019 }, { x: 902, y: 238.02 },
		{ x: 902, y: 354 },
		{ x: 902, y: 469.98 }, { x: 854.99, y: 574.98 },
		{ x: 778.985, y: 650.985 }
	]
};



/** Method to calculate the total assets.
 * Gets the object with the asset count of the reference
 * and returns the total amount of assets.
 * @param {object} assetCount object with the amount of ach asset type
 * @returns {number} total assets
 * @private
*/
const getTotalAssets = assetCount => {
	let totalAssets = 0;
	for (let type in assetCount) {
		totalAssets += assetCount[type];
	}
	return totalAssets;
};

/** Method to create the separators of the assets menu.
 * This method gets the assets count object and based on that data creates
 * an array with numbers, that indicates if the asset menu should add a line
 * for an asset or a separator (indicator) for a specific asset type.
 * @param {object} assets the assets count object for the reference
 * @returns {array} an array with numbers indicating asset or separator
 * @private
*/
const createSeparators = assets => {
	const lines = [];
	visibleLabels = [];
	if ( assets.image > 0 ) {
		// add to the visible labels
		visibleLabels.push(assetsLabels[0]);
		// add the asset lines
		for ( let i = 1; i <= assets.image; i++ ) {
			lines.push(0);
		}
	}
	if ( assets.video > 0 ) {
		// add separator only if labels array lenght is more than 0
		if ( visibleLabels.length > 0 ) {
			// add the separator
			lines.push(1);
		}
		// add label
		visibleLabels.push(assetsLabels[1]);
		// add asset lines
		for ( let i = 1; i <= assets.video; i++ ) {
			lines.push(0);
		}
	}
	if ( assets[360] > 0 ) {
		// add separator only if labels array lenght is more than 0
		if ( visibleLabels.length > 0 ) {
			// add the separator
			lines.push(1);
		}
		// add label
		visibleLabels.push(assetsLabels[2]);
		// add asset lines
		for ( let i = 1; i <= assets[360]; i++ ) {
			lines.push(0);
		}
	}
	if ( assets.document > 0 ) {
		// add separator only if labels array lenght is more than 0
		if ( visibleLabels.length > 0 ) {
			// add the separator
			lines.push(1);
		}
		// add label
		visibleLabels.push(assetsLabels[3]);
		// add asset lines
		for ( let i = 1; i <= assets.document; i++ ) {
			lines.push(0);
		}
	}
	
	return lines;
};

/** Method to create the assets menu.
 * Gets the selected reference data. With that we have the amount
 * of assets of that particular reference and the separation between
 * them.
 * The method then uses the same principle of the method to create the
 * segments, with the difference that this method only uses a start angle
 * but two different radius to get the points for the lines. An asset 
 * has a specific separation between each radius and the separator or
 * indicator of the asset type, has a bigger distance between radius.
 * @param {array} assets the assets array
 * @param {object} assetCount the asset count object
 * @private
*/
export const createAssetsMenu = (assets, assetCount) => {
	// get the separation between assets
	// the amount of images
	const refImages = _.filter(assets, ["type", "image"]);
	// the amount of videos
	const refVideos = _.filter(assets, ["type", "video"]);
	// the amount of 360
	const ref360 = _.filter(assets, ["type", "360"]);
	// the amount of documents
	const refDocs = _.filter(assets, ["type", "document"]);

	// remove the path from the asset base menu and the element lines
	// before creating the menu again
	assetMenuContainer.setAttribute("d", "");
	assetElsContainer.innerHTML = "";

	// add the d attr to the asset menu base based on the level
	assetMenuContainer.setAttribute("d", assetsMenuPaths[currentLevel]);

	// hide the labels
	TweenLite.set(assetsLabels, { display: "none" });

	// create the first and last separators
	createElementLines(assetMenuExtremeLines[currentLevel][0]);
	createElementLines(assetMenuExtremeLines[currentLevel][1]);
	// reset the current assets element index
	resetCurrentAssetsElement();
	// get the points of the current ref to draw the lines
	getPoints(assetCount);
	/* Place the label for the first separator.
	 * For this we use the points already defined for the current level.
	*/
	TweenLite.set(visibleLabels[0], {
		x: assetMenuExtremeLines[currentLevel][0].x2 + 5,
		y: assetMenuExtremeLines[currentLevel][0].y2 - 5,
		rotation: currentLevel === 2 ? -60 : -45, transformOrigin: "left center",
		display: "block"
	});
	// now animate the assets menu in
	assetsMenuTween.play();
};

/** Method to init the Assets Menu Module
 * Creates the references after the DOM nodes are placed.
 * Sets the initial state and position of the elements after the to
 * prevent errors.
 * Creates the GSAP instances that will be used through the app's lifecycle.
 * @private
*/
export const _initAssetsMenu = () => {
	// create the reference to the DOM elements
	assetMenuContainer = document.getElementById("asset-menu-base");
	assetElsContainer = document.getElementById("asset-menu-element");
	fourthLevelMenu = document.getElementById("fourth-level-menu");
	assetsLabels = document.querySelectorAll(".asset-label");
	assetTriangle = document.getElementById("asset-triangle");

	// create the assets menu animation
	assetsMenuTween = TweenLite.to(fourthLevelMenu, 0.2, {
		autoAlpha: 1, paused: true
	});
};
