/*
*******************************************************************************************
		PAGINATION MODULE
*******************************************************************************************
*/
// text module
import { currentTextSource } from "./text-module";
// levels module
import {
	currentLevel, menuLevelsContainers, createArcSegments
} from "./levels-module";
// assets module
import { isAssetVisible } from "./assets-menu-module";


// pagination buttons
export let previousGroup, nextGroup, nextGroupBtn, prevGroupBtn;

/** Method to reset the fade animations.
 * This method kills the GSAP instances of the segments wrappers
 * for level 1 and 2 and clear the props of the target elements.
 * @private
*/
export const resetSegmentWrappersTween = () => {
	TweenLite.set([ menuLevelsContainers[1], menuLevelsContainers[2] ], {
		clearProps: "opacity"
	});
};


/* Pagination Data Object
 * Has the data for the amount of groups for each menu level.
 * When it comes to pagination the levels affected by pagination are
 * level 1 and 2.
 * Has the current pagination group for each level.
 * The data should be updated when a new element is selected and the
 * current level is either 1 or 2, and when the user goes to a next or
 * previous group in the current selection.
 * Level Containers Animations.
 * GSAP instances to fade in/out level 1 and 2 segment containers.
 * By default the segments wrappers are visible, so the animation
 * should hide the elements.
 * This data is checked before updating the segments in the current level.
 * To calculate the amount of segments for the level check if the group amount
 * is bigger from the current group. Then if the current group is less than the
 * amount, use 40. If it's equal use the final amount instead.
*/
export const paginationData = {
	// level 1
	1: {
		groupAmount: 0,
		currentGroup: 1,
		finalAmount: 0,
		controlsPos: [332, 691] // this are the x coords, the y coord is 384
	},
	// level 2
	2: {
		groupAmount: 0,
		currentGroup: 1,
		finalAmount: 0,
		controlsPos: [604, 164] // this are the y coords, the x coord is 512
	}
};


/** Method to reset the pagination data.
 * Sets the values of the pagination data to it default.
 * @private
*/
export const resetPaginationData = () => {
	const { 1: lev1, 2: lev2 } = paginationData;
	lev1.groupAmount = 0;
	lev1.currentGroup = 1;
	lev1.finalAmount = 0;
	lev2.groupAmount = 0;
	lev2.currentGroup = 1;
	lev2.finalAmount = 0;
	// also hide the controls
	showPaginationControls(true);
};

/** Method to set the pagination data
 * Sets the group amount and current group of the current level
 * based on the current text source and level.
 * When creating the pagination data we check if there group amount
 * for the current selection is more than one, place and show the
 * pagination controls, if the group amount is 1 don't show the
 * pagination controls.
 * @param {boolean} close if the pagination is set from the close
 * 				button handler
 * @private
*/
export const setPaginationData = close => {
	/* If the method is executed from the close button handler,
	 * check if the current level is 0. If it's 0 then reset the
	 * pagination data to it defaults values. Also if the level is 
	 * 0 hide the pagination buttons
	 */
	if ( currentLevel === 0 ) {
		showPaginationControls(close);
		return close ? resetPaginationData() : null;
	}
	// check the amount of elements and 
	let groupAmount = Math.ceil(currentTextSource.length / 40);
	// the amount of items for the final group
	const finalGroupAmount = currentTextSource.length % 40;
	// update the pagination data
	paginationData[currentLevel].groupAmount = groupAmount;
	paginationData[currentLevel].finalAmount = finalGroupAmount;
	// after setting the pagination data
	showPaginationControls();
};


/** Method to update the current group.
 * Changes the current group index value of the current selection.
 * If the user goes to a previous or next group, the data of the
 * current level is updated.
 * @param {boolean} next if true, go to the next level, false go to the previous
 * @private
*/
export const updatePaginationGroup = next => {
	// if the current level is 0 don't run the code
	if ( currentLevel === 0 || currentLevel === 3 ) return;
	const { groupAmount, currentGroup } = paginationData[currentLevel];
	// going to the next group, check if the current group is 
	// equal to the group amount
	if ( next && currentGroup < groupAmount ) {
		// the user is selected see the next group and the current group index
		// is less than the group amount, therefore there is another group to show
		// increase the current group index
		paginationData[currentLevel].currentGroup = currentGroup + 1;
		// after updating the current group hide the current level segments wrapper
		showHideSegmentsWrapper(false);
	} else if ( !next && currentGroup > 1 ) {
		// the user selected the previous group and the current group index
		// is more than 0, so there's a previous group to show decrease
		// the current group index
		paginationData[currentLevel].currentGroup = currentGroup - 1;
		// after updating the current group hide the current level segments wrapper
		showHideSegmentsWrapper(false);
	}
};


/** Method to show/hide the segments wrappers
 * Show/hide the current level's segments wrapper.
 * Depending on the param value, the tween of the current level segments
 * wrapper will be played or reversed.
 * @param {boolean} show true: show, false: hide
*/
export const showHideSegmentsWrapper = show => {
	// get the current segment wrapper tween based on the menu level
	if ( show === false ) {
		TweenLite.to(menuLevelsContainers[currentLevel], 0.2, {
			opacity: 0, 
			onComplete: createArcSegments, onCompleteParams: [true]
		});
	} else if( show === true ) {
		TweenLite.to(menuLevelsContainers[currentLevel], 0.2, {
			opacity: 1
		});
		// we're showing the segments wrapper from the levels module
		// after creating the segments for a new pagination group
		// so we show/hide the controls
		showPaginationControls();
	} 
	// after running the tween, create the segments for the new group
};


/** Method to get the segments amount.
 * Uses the pagination data and the current level, to set the amount of
 * segments for the current level.
 * This method should be called after updating the pagination data
 * @private
*/
export const getSegmentsAmount = () => {
	// the target
	const { groupAmount, currentGroup, finalAmount } = paginationData[currentLevel];
	// if the current group is less than the group amount the number of
	// segments is 40. If the current group is equal to the group amount
	// the number of segments is the final amount
	if ( currentGroup < groupAmount ) {
		return 40;
	} else if ( currentGroup === groupAmount ) {
		return finalAmount;
	}
};


/** Method to set the target index of the next/prev group.
 * When a new group is requested, the create segments method in the 
 * levels module will reset the current level element index, normally
 * the set value is 0 for a new level, but for a new group we need to
 * the index value of the first element in the target group in the data
 * array, that has all the elements of all the groups.
 * THIS METHOD SHOULD BE CALLED AFTER THE PAGINATION DATA HAS BEEN UPDATED!!
 * @returns {number} the index value of the first element of the new group
 * @returns {null} if the groups amount is 1 return null
 * @private
*/
export const getNewGroupFirstIndex = () => {
	// get the current group and group amount
	const { groupAmount, currentGroup } = paginationData[currentLevel];
	// console.log( "------------------------\nget new group index", groupAmount, currentGroup );
	// check if there are two or more groups
	if ( groupAmount > 1 ) {
		/* The index of the first element of the new group is the limit (40)
		 * multiplied by the current group minus 1. If there are 3 groups and
		 * the current group is 2 then the index is 40, so the app will display
		 * the data for the 40th element in the array.
		*/
		// console.log( "new group index", 40 * ( currentGroup - 1 ) );
		return 40 * ( currentGroup - 1 );
	}
	// if the group amount is 1, means there's no pagination for this level
	// and selection, in that case return null
	if ( groupAmount === 1 ) return null;

	return null;
};


/** Method to Hide the pagination controls.
 * This method hides both buttons.
 * @private
*/
export const hidePaginationControls = () => {
	TweenLite.to([nextGroupBtn, prevGroupBtn], 0.1, { autoAlpha: 0 });
};


/** Method to Show/Hide the pagination buttons.
 * Shows or hides the pagination buttons depending on the current
 * group and the groups amount.
 * Also places the pagination controls depending on the level.
 * @param {boolean} close if the method is called from the close handler
 * @private
*/
export const showPaginationControls = close => {
	// if the method is called from the close handler and is the first level
	// hide the controls
	if ( isAssetVisible || (close && currentLevel === 0) ) {
		return hidePaginationControls();
	}
	const { groupAmount, currentGroup } = paginationData[currentLevel];
	// if the current selection has only one group hide all controls
	if ( groupAmount === 1 ) {
		return hidePaginationControls();
	}
	// depending on the group amount and the current group we show/hide
	// the different buttons
	// we're on the first group, show the next button and hide the prev
	if ( currentGroup === 1 ) {
		TweenLite.to(prevGroupBtn, 0.1, { autoAlpha: 0 });
		TweenLite.to(nextGroupBtn, 0.1, { autoAlpha: 1 });
	}
	// the current group is not 1 and less than the group amount
	// show both buttons
	if ( currentGroup > 1 && currentGroup < groupAmount ) {
		TweenLite.to([nextGroupBtn, prevGroupBtn], 0.1, { autoAlpha: 1 });
	}
	// the current group is the last one, show the prev button and
	// hide the next button
	if ( currentGroup === groupAmount ) {
		TweenLite.to(nextGroupBtn, 0.1, { autoAlpha: 0 });
		TweenLite.to(prevGroupBtn, 0.1, { autoAlpha: 1 });
	}
};

/** Pagination module init
 * Starts the pagination module
 * @private
*/
export const _initPagination = () => {
	// previousGroup = document.getElementById("previous");
	// nextGroup = document.getElementById("next");
	nextGroupBtn = document.getElementById("next-group-btn");
	prevGroupBtn = document.getElementById("prev-group-btn");
	// for this test we use a single position for the buttons
	// depends on feedback
	TweenLite.set([nextGroupBtn, prevGroupBtn], { transformOrigin: "center" });
	TweenLite.set(prevGroupBtn, { x: 400, y: 680 });
	TweenLite.set(nextGroupBtn, { x: 609, y: 680 });
	// attach click handlers
	// nextGroup.onclick = updatePaginationGroup.bind(null, true);
	// previousGroup.onclick = updatePaginationGroup.bind(null, false);
	nextGroupBtn.onclick = updatePaginationGroup.bind(null, true);
	prevGroupBtn.onclick = updatePaginationGroup.bind(null, false);
};
