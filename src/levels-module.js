/*
*******************************************************************************************
		LEVELS MODULE
*******************************************************************************************
*/
// buttons
import { isCloseClicked } from "./buttons-module";
// draggable module
import {
	placeKnob, _resetCurrentLevelElement, hideDragElements,
	currentLevelElement
} from "./draggable-knob-module";
// text module
import {
	secondLevelTextWrap, thirdLevelTextWrap, setTextSource,
	applyElementText, currentTextSource
} from "./text-module";
// assets module
import { fourthLevelMenu, assetsMenuTween, _resetAssetVisible } from "./assets-menu-module";
// image module
import { refImageTween, updateImageSrc } from "./image-module";
// data module
import {
	firstLevel, customersReferences, selectedCustomer,
	newHighlightRefsCount
} from "./data-module";
// pagination module
import {
	setPaginationData, getSegmentsAmount,
	getNewGroupFirstIndex, showHideSegmentsWrapper,
	hidePaginationControls
} from "./pagination-module";

// the segments of the current level. Used to update the color as
// the user drags the knob along the path.
export let currentLevelSegmentsAmount = 0;

/** Method to set the current level segments.
 * @private
*/
export const _setCurrentLevelSegmentsAmount = () => currentLevelSegmentsAmount = getLevelSegments();

// the default color of the segments of each level
export const defaultLevelElementColor = "rgba(255,255,255,0.8)";

/* array with all the paths for each level each level has it's own
 * array with the paths. When the close button is clicked, the current
 * level is emptied before reducing the level index.
 * this works one level behind the current level, so the array for the
 * first level has an index 0.
*/
export let levelPathsArrays = [[], [], []];

/** Method to clean the current level's paths array
 * @param {bool} all if all the levels should be cleared
 * @private
*/
export const clearLevelPathsArray = all => {
	if (all) {
		levelPathsArrays = [[], [], []];
		return;
	}
	// get the array for the current level
	levelPathsArrays[currentLevel] = [];
};

// the current menu level
export let currentLevel = 0;

/** Method to update the current level value
 * @param {boolean} increase if the value should increase or not
 * @private
*/
export const _changeCurrentLevelValue = increase => increase ? currentLevel++ : currentLevel--;

/* The app structure is circular and we need the radius
 * of each level in order to calculate and position the elements
 * of each level on those cirular patterns and draw the rest of 
 * the guide circle 
*/
const levelRadius = [null, 150, 220, 290];
/* Each level of the app is positioned in different starting and
 * ending angles. 
*/
const levelAngles = [
	// level 0 is always visible
	null,
	// second level
	{
		small: {
			segments: [-58, 116], bottom: [61, 300]
		},
		big: {
			segments: [-118, 236], bottom: [121, 240]
		}
	},
	// third level
	{
		small: {
			segments: [212, -32], bottom: [-29, 210]
		},
		big: {
			segments: [-208, 28], bottom: [31, 150]
		}
	},
	// fourth level
	{
		small: {
			segments: [32, 148], bottom: [-209, 30]
		},
		big: {
			segments: [-28, 208], bottom: [-149, -30]
		}
	}
];
/* The containers for each level's segments
 * These are the DOM elements (<g> tags) that will hold the segments.
 * The containers are cleared before adding the new elements and
 * showing them.
*/
let secondLevelSegments, secondLevelBottom, thirdLevelSegments, thirdLevelBottom, fourthLevelSegments, fourthLevelBottom;
//			
export let levelArcContainers;
/* Level menu containers
 * This have the segments and bottom circles. This are the ones that
 * will be animated in and out (zoom and fade)
*/
let secondLevelMenu, thirdLevelMenu;

// the menu containers for each level, this will hold the segments and
// the close bottom arc
export let menuLevelsContainers;

/** Method to create the animations of each menu level.
 * Creates the scale and fade animation for each level that's played
 * after creating and adding the segments and close arc for the level.
 * Also creates the animation of the level's text wrap, in order to
 * animate all the level's elements at the same time.
 * @param {number} index the target element
 * @returns {object} the GSAP Timeline instance 
 * @private
*/
const createMenuTween = index => {
	// the first level menu is always visible, stop code
	if (index === 0) return;
	// create the timeline instance
	// when the timeline is complete, position the drag knob
	// the method to position the knob will show the drag path and knob
	const tl = new TimelineLite({
		paused: true,
		onComplete: placeKnob,
		onReverseComplete: () => {
			// check if the instance was reversed because of the close button or
			// because a first level selector. If the close button was clicked
			// then call the method to position the drag elements and then show them
			if (isCloseClicked && currentLevel > 0) placeKnob();
		}
	});
	// always get the level container
	const levelContainer = menuLevelsContainers[index];
	/* The second and third level have menu elements and text, while
	 * the foruth level has just menu elements. Create a timeline only
	 * with the menu elements for the fourth level (index = 3) and a 
	 * timeline with the menu elements and text for second and third.
	*/
	if (index === 3) {
		tl
			.to(levelContainer, 0.2, {
				autoAlpha: 1, scale: 1, transformOrigin: "center", paused: true
			});
		// tl
	} else if (index === 1 || index === 2) {
		// the level is 1 or 2, get the text wrap
		const textWrap = index === 1 ? secondLevelTextWrap : thirdLevelTextWrap;
		tl
			.to(levelContainer, 0.2, {
				autoAlpha: 1, scale: 1, transformOrigin: "center"
			})
			.to(textWrap, 0.2, {
				autoAlpha: 1
			}, 0.1);
		// tl
	}
	// add the timeline to the menu tweens array
	menuTweensArray.push(tl);
	// return the timeline
	return tl;
};
// an array to contain the menu levels animations
// this allows to play and reverse the level animations
// when needed
export const menuTweensArray = [null];


/** Method to create the arc string.
	 * Takes the circle center, radius and angles, and returns an arc
	 * string that is used in the <path> tag as the "d" attribute.
	 * @param {number} centerX the x coordinate of the circle center
	 * @param {number} centerY the y coordinate of the circle center
	 * @param {number} radius the circle radius
	 * @param {number} startAngle the angle in degs where the arc starts
	 * @param {number} endAngle the angle in degs where the arc ends
	 * @returns {string} the arc string
	 * @private
	*/
const createArcString = (centerX, centerY, radius, startAngle, endAngle) => {
	// the angle should be in rad
	var correctedAngleStart = (startAngle - 90) * Math.PI / 180;
	var correctedAngleEnd = (endAngle - 90) * Math.PI / 180;
	// first get the points
	var startX = centerX + (radius * Math.cos(correctedAngleEnd));
	var endX = centerX + (radius * Math.cos(correctedAngleStart));
	var startY = centerY + (radius * Math.sin(correctedAngleEnd));
	var endY = centerY + (radius * Math.sin(correctedAngleStart));
	// the arc sweep
	var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

	// set the arc string
	var arcString = "M " + startX + " " + startY + " A " + radius + " " + radius + " 0 " + arcSweep + " 0 " + endX + " " + endY;

	return arcString;
};


/** Method to create the Path tag
 * Creates the path tag with the corresponding d attribute for the arc
 * string
 * @param {number} centerX the x coordinate of the circle center
 * @param {number} centerY the y coordinate of the circle center
 * @param {number} radius the circle radius
 * @param {number} startAngle the angle in degs where the arc starts
 * @param {number} endAngle the angle in degs where the arc ends
 * @returns {object} the path tag
 * @private
*/
const createArcPath = (centerX, centerY, radius, startAngle, endAngle, close) => {
	// set the stroke width
	const strokeWidth = close ? " 1;" : " 10;";
	// create the path element
	const newPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
	newPath.setAttribute("style", "fill: none; stroke: rgba(255,255,255,0.8); stroke-width:" + strokeWidth);
	newPath.setAttribute("d", createArcString(centerX, centerY, radius, startAngle, endAngle));
	newPath.setAttribute("class", "segment-el");
	// add the path to the container
	// acrContainer.appendChild(newPath);
	// add the paths to the array
	close ? null : levelPathsArrays[currentLevel].push(newPath);
	return newPath;
};


/** Method to set the level segements.
 * Depending on the level and the first level selection, returns the amount
 * of segments for the current level and selection.
 * @returns {number} the number of segements
 * @private
*/
const getLevelSegments = () => {
	if (currentLevel === 1) {
		// get the segments of the corresponding selection
		return firstLevel[currentFirstLevelSelection];
	} else if (currentLevel === 2 && currentFirstLevelSelection === "customer") {
		// the first level is customer, get the selected customer and it's refs
		return customersReferences[selectedCustomer].length;
	} else if (currentLevel === 2 && currentFirstLevelSelection === "highlight") {
		// the first level is highlight, get the refs amount
		// return referencesCount; 
		return newHighlightRefsCount;
	}
};


/** Method to create the segments.
 * Gets the data of the current level and loops through the elements
 * and creates a <path> tag for each one and adds it to the parent
 * target.
 * In this method check the pagination data to set the number of elements
 * to show. Basically update the level segments based on the pagination
 * data, that is the group number, group amount and total elements.
 * If the method is called from a pagination change, don't run the get
 * level segements method, just 
 * @param {boolean} paginate if the method is called because a pagination
 * 				change or not
 * @private
*/
export const createArcSegments = paginate => {
	// console.log( "---------------\ncreate arcs" );
	// console.log( paginationData[currentLevel] );
	// console.log( "segments >", getSegmentsAmount() );
	// set the level segments
	let levelSegments = currentLevelSegmentsAmount = getSegmentsAmount();
	// if the method is being executed from a pagination method
	// reset the current level paths array
	paginate ? clearLevelPathsArray() : null;
	// use the current level to get the angles and radius for the segments
	// and close arc, depending on the amount of segments
	const currLevelRadius = levelRadius[currentLevel];
	const currLevelAngles = levelSegments > 20 ? (levelAngles[currentLevel].big) : (levelAngles[currentLevel].small);
	// depending on the segments amount the angles to be used
	const segmentAngles = currLevelAngles.segments;
	// the total angle for each segment
	const arcAngle = ((levelSegments > 20 ? 236 : 116) - (levelSegments - 2)) / levelSegments;
	// the parent element for each segment
	const segmentsParent = levelArcContainers[currentLevel].segments;
	// remove all the segments from the parent element
	segmentsParent.innerHTML = "";
	// with the amount of segments and the angles create the segments
	// and add them to the parent element
	for (let i = 0; i < levelSegments; i++) {
		const currentAngle = segmentAngles[0] + (i * arcAngle) + i;
		const newArc = createArcPath(512, 384, currLevelRadius, currentAngle, currentAngle + arcAngle);
		segmentsParent.appendChild(newArc);
	} // loop
	// then create the closing arc for the current level and add it to the container
	const closeArc = createArcPath(512, 384, currLevelRadius, currLevelAngles.bottom[0], currLevelAngles.bottom[1], true);
	const closeArcContainer = levelArcContainers[currentLevel].close;
	// add the bottom arc to the container
	closeArcContainer.innerHTML = "";
	closeArcContainer.appendChild(closeArc);
	/* reset the current level element before applying the text
	 * of the fist element of the new level.
	 * Run the method to get the index value of the new group if any.
	*/
	_resetCurrentLevelElement( getNewGroupFirstIndex() );
	// console.log( "----------------------\ncreate arcs" );
	// console.log( "current level element", currentLevelElement );
	/* If the method was executed from a pagination instance then after
	 * adding the new group of segments, play the tween to show the segments
	 * wrapper.
	*/
	if (paginate) {
		showHideSegmentsWrapper(true);
	}
	// now apply the text to the container
	applyElementText( false, getNewGroupFirstIndex() );
	// update the image source
	updateImageSrc(currentTextSource[currentLevelElement].thumb);
	// by default the first element of the current level should be white
	TweenLite.set(levelPathsArrays[currentLevel][0], {
		stroke: "#fff", className: "+=selected-segment"
	});
	// animate the entire level menu
	menuTweensArray[currentLevel].play();
	// if the current level is 1 and the selection is not customer show the image wrappper
	if ( currentLevel === 1 && currentFirstLevelSelection !== "customer" ) {
		refImageTween.play();
	}
	// if the current level is 2 and the selection is customer show the image wrapper
	if ( currentLevel > 1 && currentFirstLevelSelection === "customer" ) {
		refImageTween.play();
	}
}; // create arc segments

/*
	*******************************************************************************************
			FIRST LEVEL CLICK AND ANIMATION
	*******************************************************************************************
	*/
// the angles for each rotation
const firstLevelAngles = {
	highlight: 0,
	references: -120,
	customer: 120
};
// the current selected element
export let currentFirstLevelSelection = "";
// the previous first level selected element
let previousFirstLevelSelection = "#highlight";
// the first level main wrap
let firstLevelWrap;
// the elements of the first level
let firstLevelElements;
/** Method to create the rotation animation.
 * Since we might need to reverse other animations before starting the
 * first level rotation, we use this method to create and play the animation.
 * @param {number} angle the angle of rotation
 * @private
*/
const createRotationTween = angle => {
	// set the current level to 1
	currentLevel = 1;
	// set the current level source
	setTextSource();
	// set the pagination data
	setPaginationData();
	// add/remove the first level selected class
	TweenLite.set("#" + currentFirstLevelSelection, { className: "+=selected" });
	TweenLite.set(previousFirstLevelSelection, { className: "-=selected" });
	// set the new target as the previous
	previousFirstLevelSelection = "#" + currentFirstLevelSelection;
	// create the rotation tween applyElementText
	TweenLite.to(firstLevelWrap, 0.2, {
		rotation: angle + "_short", transformOrigin: "50% 54%",
		onComplete: createArcSegments
		// onComplete: applyElementText
	});
};

/** Method to reverse the levels animations.
 * Checks the current level and reverses the animations of all the
 * visible levels.
*/
export const reverseLevelsTweens = (angle, close) => {
	// if the close button was clicked, then we don't have to create the rotation tweens
	if (close) {
		// empty the paths array of the current closed level
		clearLevelPathsArray();
		// hide the drag elements
		hideDragElements();
		// reverse the menu GSAP instance
		menuTweensArray[currentLevel].reverse();
		return;
	}
	// empty all the paths arrays
	clearLevelPathsArray(true);
	// the close button wasn't clicked, but a first level selector
	// reverse all the levels instances
	menuTweensArray.forEach(e => e ? e.reverse() : null);
	// then create the rotation tween for the selected element
	TweenLite.delayedCall(0.35, createRotationTween, [angle]);
};


/** Method to init the levels module
 * Creates the animations for each menu level and also attaches the
 * click event to every first level element.
 * Sets the initial state of the 
 * @private
*/
export const _initLevelsModule = () => {
	// create the module variables for DOM elements
	// level segments containers
	secondLevelSegments = document.getElementById("second-level-segments");
	secondLevelBottom = document.getElementById("second-level-bottom");
	thirdLevelSegments = document.getElementById("third-level-segments");
	thirdLevelBottom = document.getElementById("third-level-bottom");
	fourthLevelSegments = document.getElementById("fourth-level-segments");
	fourthLevelBottom = document.getElementById("fourth-level-bottom");
	// level menu containers
	secondLevelMenu = document.getElementById("second-level-menu");
	thirdLevelMenu = document.getElementById("third-level-menu");
	// first level menu wrap
	firstLevelWrap = document.getElementById("first-level-wrap");
	// first level elements
	firstLevelElements = document.querySelectorAll(".first-level-element");
	// the menu level containers
	menuLevelsContainers = [null, secondLevelMenu, thirdLevelMenu, fourthLevelMenu];
	// level arc containers
	levelArcContainers = [
		null,
		{ segments: secondLevelSegments, close: secondLevelBottom },
		{ segments: thirdLevelSegments, close: thirdLevelBottom },
		{ segments: fourthLevelSegments, close: fourthLevelBottom }
	];

	// loop through the first level elements and add the click events
	firstLevelElements.forEach(e => {
		const targetEl = e.getAttribute("data-target");
		/* When the app first runs, check if the amount of elements of each 
		 * type is 0, in that case add a class to the element in order to 
		 * create a visual aid of the element being disabled
		*/
		if ( firstLevel[targetEl] === 0 ) {
			TweenLite.set(e, { className:"+=disabled" });
		}
		e.onclick = () => {// get the target element
			// if the target doesn't have any elements, don't execute the code
			// and send an alert to the user
			if ( firstLevel[targetEl] === 0 ) return;// alert("The selected target is empty!!");
			// if the target is not the selected one then animate
			// and if the current first level is not empty
			if (targetEl !== currentFirstLevelSelection && currentFirstLevelSelection !== "") {
				currentFirstLevelSelection = targetEl;
				// hide the drag elements
				hideDragElements();
				// hide the assets menu
				assetsMenuTween.reverse();
				// set the asset menu visible bool to false
				_resetAssetVisible(false);
				// hide the image wrapper
				refImageTween.reverse();
				// hide the pagination controls
				hidePaginationControls();
				/* First we check if the current level is more than 0.
				 * If the level is more than 0, then we reverse all the animations
				 * of the current visible levels.
				 * After reversing the animations, we should remove all the segments and
				 * close arcs from their containers.
				 * Then we run the rest of the code to create the new level.
				*/
				if (currentLevel > 0) return reverseLevelsTweens(firstLevelAngles[targetEl]);
				// create the rotation animation
				createRotationTween(firstLevelAngles[targetEl]);
			}// selected target conditional
			// if the current level is empty and the target is highlight
			// then just create the arc segments
			if (currentFirstLevelSelection === "") {
				// set the current level to 1
				currentLevel = 1;
				currentFirstLevelSelection = targetEl;
				// set the current level source
				setTextSource();
				// set the pagination data
				setPaginationData();
				if (targetEl == "highlight") return createArcSegments();
				createRotationTween(firstLevelAngles[targetEl]);
			}
			if (currentLevel === 0 && currentFirstLevelSelection === targetEl) {
				currentLevel = 1;
				currentFirstLevelSelection = targetEl;
				// set the current level source
				setTextSource();
				// set the pagination data
				setPaginationData();
				createArcSegments();
			}
		}; // click event
	}); // for each loop

	/* Set the initial state of the level containers to animate them when that
	 * element of the menu is selected.
	 * After the animation is complete, then we add the text of the first element
	 * of the level to the text component. Then we animate the 
	*/
	// initial state of the text wraps
	TweenLite.set([secondLevelTextWrap, thirdLevelTextWrap], {
		autoAlpha: 0
	});
	// initial state of the menu containers
	TweenLite.set([secondLevelMenu, thirdLevelMenu], {
		autoAlpha: 0, scale: 0.01, transformOrigin: "center", x: 507, y: 379
	});

	TweenLite.set(fourthLevelMenu, {
		autoAlpha: 0
	});

	for (let i = 1; i < 3; i++) {
		createMenuTween(i);
	}
};
