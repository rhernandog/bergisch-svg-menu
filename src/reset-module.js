/*
*******************************************************************************************
		RESET MODULE
*******************************************************************************************
*/
/* The reset module reverts all the menu state to it start default. For doing this,
 * the module needs to import all the app's reset and animation reverse
 * methods.
 * Reverses all animations.
 * Finally this module creates the menu reset method that then is added to the global
 * scope in the index file.
*/

/* ASSETS MENU */
import { assetsMenuTween, _resetAssetVisible } from "./assets-menu-module";
/* DRAGGABLE KNOB */
import {
	resetCurrentAssetsElement, _resetCurrentLevelElement, hideDragElements
} from "./draggable-knob-module";
/* IMAGE MODULE */
import { refImageTween } from "./image-module";
/* LEVELS MODULE */
import { reverseLevelsTweens } from "./levels-module";
// get the main init menu
import { _initSVGMenu } from "./index";

/** Reset Menu Method
 * Resets the menu, setting it's state to the initial defaults
 * and updates the menu's data.
 * @param {object} data the new data to be applied to the menu
 * @public
*/
export const resetSVGMenu = data => {
	// assets menu
	assetsMenuTween.reverse();
	_resetAssetVisible(false);
	// draggable knob
	resetCurrentAssetsElement();
	_resetCurrentLevelElement();
	hideDragElements();
	// ref image
	refImageTween.reverse();
	// levels tween
	reverseLevelsTweens(0, true);
	// finally restart the menu
	_initSVGMenu(data);
};
