/*
*******************************************************************************************
		DATA MODULE
*******************************************************************************************
*/
/* This module gets the data from the server and then separates it for each
 * type. 
*/
// get lodash
import { filter, map } from "lodash";

// text module
import { currentTextSource } from "./text-module";
// draggable knob module
import {
	currentLevelElement, currentAssetsElement
} from "./draggable-knob-module";
// assets menu module
import { isAssetVisible } from "./assets-menu-module";


// app references
export let appReferences, referencesCount;
// highlight references
export let highlightRefs, highlightCount;
// the object for all the highlight refs and it's child refs
export let highlightRefsObject = {};
export let newHighlightRefs = [];
export let newHighlightRefsCount = 0;
// customers
export let appCustomers, customersCount;
// separate the refs for each customer
export const customersReferences = {};
// the selected customer
export let selectedCustomer;

/** Method to set the selected customer.
 * @param {string} customerId the id of the selected customer
 * @private
*/
export const _setSelectedCustomer = customerId => selectedCustomer = customerId;


/** Method to separate the app data.
 * Gets the JSON data and stores each different data type on it's own constant
 * @param {object} data the JSON data from the request
 * @private
*/
export const _setAppData = data => {
	const start = Date.now();
	// set the app refs
	appReferences = data.references.filter( ref => ref.type !== "highlight" );
	referencesCount = appReferences.length;
	// set the customers
	appCustomers = data.allCustomers;
	customersCount = appCustomers.length;
	// loop through the customers and create a key:val pair for each customer
	// and the val should be an array with all the references for that customer
	appCustomers.forEach( customer => {
		// get all the refs for this customer
		customersReferences[customer.id] = filter(data.references, ref => ref.customer.id === customer.id);
	});
	// create the highlight references
	highlightRefs = filter(data.references, (ref, i) => {
		if (ref.type === "highlight") {
			// create the updated child array with the assets and
			// assets count from the parent
			const updatedRefs = map(ref.references, child => {
				const newChild = Object.assign({}, child);
				// newChild.assets = ref.assets;
				// newChild.asset_count = ref.asset_count;
				newChild.thumb = ref.thumb;
				return newChild;
			});
			// create the updated highlight
			// this has the updated child with the assets and thumb from
			// it's parent.
			const newRef = [].concat(ref, updatedRefs);
			// const newRef = [].concat(ref, ref.references);
			// now add the updated highlight to the highlights object
			highlightRefsObject[ref.id] = newRef;
			return true;
		}
	});
	highlightCount = highlightRefs.length;
	// set the first level counts
	firstLevel.customer = customersCount;
	firstLevel.highlight = highlightCount;
	firstLevel.references = referencesCount;
};

/** Method for the highlight ref selection
 * When the user selects a ref from the highlight list, we need a new array
 * that's a copy of the references array. The difference is that the selected
 * highlight is removed from the array and added as the first element.
 * @private
*/
/** Method for a Highlight Ref Selection
 * When the user selects a highlight ref, the next level should show the
 * selected ref and it's child references. For that we create an array
 * with the selected highlight as the first element and it's child refs
 * after that.
 * Child refs don't have assets, so we add the assets of the parent to them
 * in order to show those in case a child is selected.
 * @private
*/
export const createNewRefs = () => {
	// get the selected highlight
	const targetRef = currentTextSource[currentLevelElement];
	// console.log( highlightRefsObject[targetRef.id] );
	/* Add the parent's assets to each child ref
	 * Create a new array with the updated child references,
	 * at this point the child refs live in an object in the parent ref.
	*/
	const childRefs = map(targetRef.references, ref => {
		const newChild = Object.assign({}, ref);
		// newChild.assets = targetRef.assets;
		// newChild.asset_count = targetRef.asset_count;
		newChild.thumb = targetRef.thumb;
		return newChild;
	});
	// update the new highlights array
	newHighlightRefs = [].concat( targetRef, childRefs );
	// set the new amount of highlight references
	newHighlightRefsCount = newHighlightRefs.length;
};


/** Method to update the Window Reference Object
 * Checks if the parameter is a reference or a customer and
 * updates the global reference object.
 * @param {object} ref the reference object
 * @private
*/
export const updateRefObject = ref => {

	if ( ref === null ) {
		return window.referenceObj = {
			reference: null,
			type: null,
			parent: null,
			customer: null,
			mediaType: null,
			mediaIndex: null
		};
	}
	/* If the assets menu is visible a reference has been selected
	 * so we only update the media type and index properties
	*/
	if ( isAssetVisible ) {
		const newRefObject = {
			...window.referenceObj,
			mediaType: ref.type,
			mediaIndex: ref.sorting
		};
		return window.referenceObj = newRefObject;
		
	}
	// check if it's a reference or customer
	// a reference has a customer property a customer doesn't
	if ( ref.customer ) {
		// reference
		window.referenceObj = {
			reference: ref.id || null,
			type: ref.type || null,
			parent: ref.pivot ? ref.pivot.reference_parent_id : null,
			customer: ref.customer.id || null,
			mediaType: null,
			mediaIndex: ref.assets ?
				(ref.assets[currentAssetsElement - 1] ? ref.assets[currentAssetsElement - 1].sorting : null) : null
		};
	} else {
		// customer, just update the customer and 
		window.referenceObj = {
			reference: null,
			type: null,
			parent: null,
			customer: ref.id || null,
			mediaType: null,
			mediaIndex: null
		};
	}
};


/** Method to get the data from the server
 * @param {string} url the data url from the server
 * @private
*/
export const _fetchServerData = url => {
	// local - no server
	_setAppData(url);
};

// second level elements segments
export const firstLevel = {
	customer: customersCount,
	highlight: highlightCount,
	references: referencesCount
};
