/*
*******************************************************************************************
		IMAGE MODULE
*******************************************************************************************
*/
// levels module
import { currentFirstLevelSelection, currentLevel } from "./levels-module";
// text module
import { currentTextSource } from "./text-module";
// data module
import { appReferences } from "./data-module";
// draggable knob module
import { currentLevelElement } from "./draggable-knob-module";

// the image wrapper
let imageWrapper;
// the actual image, the src attribute will be updated
let referenceImage;

/* The GSAP instance that shows/hide the image wrapper
 * When the user selects a reference and the image wrapper is not visible
 * we show it after updating the src attribute.
*/
export let refImageTween;

/* Bool to indicate if the image wrapper is visible or not.
 * In some cases the image wrapper could be visible and there's
 * no need to play the image tween, after selecting a reference,
 * just update the image src attribute.
 * False by default.
*/
export let isImageVisible = false;

/* Object to store the image url of each level.
 * The user can select highlight for the first level and then an image
 * will be visible, but if selects a different reference for the next level
 * and goes to the assets menu of that reference, we need to update the 
 * image according to the selected refs when the user goes back to a previous
 * level.
*/
export const selectedLevelImages = {
	1 : "",
	2 : ""
};

/** Method to update the Image SRC
 * Changes the src attribute of the image based on the thumb
 * for the current reference.
 * @param {string} src the image source
 * @private
*/
export const updateImageSrc = src => {
	// console.log( "-------------------------\nupdate image src" );
	// console.log( "level => ", currentLevel );
	referenceImage.setAttribute("src", src);
};



/** Method to Init the Module
 * Creates the reference of the DOM element in the variable and
 * the GSAP instance to show/hide the element.
 * @private
*/
export const _initImageModule = () => {
	imageWrapper = document.getElementById("image-wrapper");
	referenceImage = document.getElementById("reference-image");
	refImageTween = TweenLite.to( imageWrapper, 0.2, {
		autoAlpha: 1, paused: true
	});
};
