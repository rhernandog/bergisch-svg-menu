// get the data module
import { _fetchServerData } from "./data-module";
// get the levels init method
import { _initLevelsModule } from "./levels-module";
// get the method to init the knob module
import { _initKnobModule } from "./draggable-knob-module";
// buttons init method
import { _initBtnModule } from "./buttons-module";
// text init method
import { _initTextModule } from "./text-module";
// assets menu init method
import { _initAssetsMenu } from "./assets-menu-module";
// image module init method
import { _initImageModule } from "./image-module";
// reset modult
import { resetSVGMenu } from "./reset-module";
// pagination module
import { _initPagination } from "./pagination-module";

/** General Init Method
 * Starts the app.
 * @param {object} data the data location in the global window object
 * @private
*/
export const _initSVGMenu = data => {
	// get the data
	// _fetchServerData("js/sample-data.json");
	_fetchServerData(data);
	// init the assets menu
	_initAssetsMenu();
	// init the text module
	_initTextModule();
	// init the image module
	_initImageModule();
	// init the levels
	_initLevelsModule();
	// init the buttons module
	_initBtnModule();
	// init the draggable knob
	_initKnobModule();
	// init pagination module
	_initPagination();
};

_initSVGMenu(window.SVGMenuData);

// create the property in the global object
window.SVGRingMenu = { _initSVGMenu, resetSVGMenu };
