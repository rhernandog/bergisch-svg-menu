# Bergisch Media SVG Menu App

## Description
SVG Circular Menu app project.

The app consists in a multi-level app in a circular disposition.

The first level has three elements any of which can be selected. After selecting an element from the first level, the second level for that element appears in a grow-from-center / fade-in animation, that shows a thin circle around the first level and blocks or dividers for each element of that level. This also shows a draggable knob that allows to select one of the elements of the current level and also to select that element by tapping it.

Finally when the user 

## Instructions
Clone download this repo and then install the dependencies:
```
$ npm install
```
Finally to bundle the code and start a local server run:
```
$ npm start
```
To create a minified version of the code run:
```
$ npm run build:p
```
In order to start the Ring Menu call the general init method, passing the object with the app's data as a parameter:
```js
// window.dataObject is the location of the app data
// in the global window object
_initSVGMenu(window.dataObject);
```

## ChangeLog

#### Version 1.7.3-a
- Creates smaller filter for drag path. Resolves granular look in the glow filter. Solves #20.
- Removes the no assets alert when the user selects a ref with no assets. Also creates the disabled state for the knob in those cases. Solves #21.
- Uses the non-filtered data to create the customers references object. Previously we were using the filtered data. If the current data set has only highlight references then the filtered data is empty and the object is created with empty arrays. Solves #22.

#### Version 1.7.2-a
- Removes the alert when an empty first level type is selected. Also adds a visual aid in order to set the state of the first level element as **disabled**. Solves #16.
- Updates the object reference in the level 0, after the close button is tapped/clicked. Solves #18.
- Fixes the **undefined** text when selecting a highlight reference. Solves #19.

#### Version 1.7.1-a
- Fixes the issue on OSX and iOS safari 10-11 with the SVG glow filter. Solves #12.
- The first level references are separated in two groups. One for the highlight references and another for the regular and complex references. The hightlight refs are no longer included in the group of the regular and complex references. Solves #13.
- The customer ID is correctly applied in the reference object. Solves #14.
- If the target element doesn't have assets, the assets menu is not shown. Solves #15.

#### Version 1.7.0-a
- Includes pagination when a level or selection has more than 40 references or customers (in the case the first level selection is customers). Solves #5.

#### Version 1.6.0-a
- Gets the child refs from a highlight correctly. Adds the parent's assets to the child refs, so if a user selects a child ref, the app shows the parent's assets. Solves #1 and #11.
- The play button click event handler has been removed. Solves #8.
- Added `reset` method to reset the app with a new data set. Solves #9.
- The init and reset methods are added to the global space in a specific object. Both methods are added to a specific object `window.SVGRingMenu`. In order to use them just call `window.SVGRingMenu._initSVGMenu(data)` and `window.SVGRingMenu.resetSVGMenu(data)`. Solves #10.

#### Version 1.5.0-a
- Implements more detailed update of the reference object in the global object.Solves #2.
- Updates the second level text to the full white color. Solves #3.
- Updated the second level circle color to be rgba white with 0.8 alpha. Solves #4.
- Created method to reset the current assets line index. Solves #6.
- Splits the text and image update methods. They are called individually. Solves #7.

#### Version 1.4.1-a
- Changed to the color scheme.
- Changed the font family to Roboto.
- Minor updates in the HTML code in order to use classes instead of inline styles.
- The global object property is now updated on every selection when the user drags the knob in a menu, not just when the user clicks on the knob or the start button.

#### Version 1.4.0-a
- Updates a global object property `window.referenceObj` on each selectin, wheather is a reference or an asset.
- Fixes a structural problem that included all the app's references when a highlight was selected. Now it creates the second level menu with the selected highlight reference and it's child references as well.
- When the assets menu is visible, it updates the image element with the selected asset's thumb.

#### Version 1.3.0-a 
- Added the image module and the capacity to use the images from the file path indicated in the JSON data to show the image of each reference.

#### Version 1.2.0-a
- Creates a general **init** method to start the entire app `_initSVGMenu`.
- Reafctored some code in order to solve some bugs that appeared after creating the general init method, mainly because this required specific init methods for the different modules that are called in the right sequence to prevent errors.
- Added a Webpack config file to create a minified version of the code.
- Updated the **Instructions** section of the Readme file, to include the init method and the production bundling script.


#### Version 1.1.1-a
- Solves an issue when clicking the close button the selected segment was still highlighted, while the drag knob was in a different position.

#### Version 1.1.0-a
- Fixes the indicator triangle position.

#### Version 1.0.0-a
- First release of the complete app.

## Author
Rodrigo Hernando G.
Email: rodrigo@websnap.cl
Twitter: @websnapcl

## License
This code is property of Bergisch Media. All rights reserved, 2018.
