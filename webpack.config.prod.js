var path = require("path");
// webpack plugins
var CleanWebpackPlugin = require('clean-webpack-plugin');
var ManifestPlugin = require('webpack-manifest-plugin');
var htmlPlugin = require("html-webpack-plugin");
var CleanObsoleteChunks = require('webpack-clean-obsolete-chunks');


module.exports = {
	mode: "production",
	// entry
	entry: {
		app: "./src/index.js"
	},
	// modules
	module: {
		rules: [
			// babel
			{
				test: /\.js$/,
				use: "babel-loader",
				exclude: /node_modules/
			}
		]
	},
	// optimization
	optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: "vendors",
					chunks: "all"
				}
			}
		}
	},
	// source map
	devtool: "source-map",
	// plugins
	plugins: [
		new CleanObsoleteChunks(),
		new ManifestPlugin(),
		new CleanWebpackPlugin(["public/js/app/*.*"]),
		new htmlPlugin({
			template: "./src/template/index.html",
			filename: "index.html"
		})
	],
	// output
	output: {
		path: path.join( __dirname, "public" ),
		filename: "js/app/[name]-[chunkhash].min.js"
	}
};
