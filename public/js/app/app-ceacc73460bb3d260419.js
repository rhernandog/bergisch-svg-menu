/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/assets-menu-module.js":
/*!***********************************!*\
  !*** ./src/assets-menu-module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initAssetsMenu = exports.createAssetsMenu = exports.assetsDragArrays = exports.assetsDragPaths = exports.updateTrianglePos = exports._resetAssetVisible = exports.isAssetVisible = exports.assetsMenuTween = exports.totalAssets = exports.assetsLines = exports.trianglePoints = exports.assetTriangle = exports.fourthLevelMenu = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /*
                                                                                                                                                                                                                                                                  *******************************************************************************************
                                                                                                                                                                                                                                                                  		ASSETS MENU MODULE
                                                                                                                                                                                                                                                                  *******************************************************************************************
                                                                                                                                                                                                                                                                  */


var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

// the asset menu and element containers
// this holds the menu base arc
var assetMenuContainer = void 0;
// this holds the menu elements and separator lines
var assetElsContainer = void 0;
// the assets menu wrapper
var fourthLevelMenu = exports.fourthLevelMenu = void 0;
// the assets separator labels
var assetsLabels = void 0;
// the asset triangle indicator
var assetTriangle = exports.assetTriangle = void 0;
/* The triangle should be positioned as the user drags the knob while the
 * assets menu is open. For this we need the position of each asset element
 * as well as the rotation. Use an array to add those points as objects.
*/
var trianglePoints = exports.trianglePoints = [];
/* Since some references won't have some asset type, we need a way to 
 * know if some label shouldn't be visible for the selected ref.
 * In this array we hold the a copy of the label elements that should
 * be visible in the assets menu so the getPoints method uses this instead
 * of the entire labels collection. This array is set when the seprartor
 * marks are created.
*/
var visibleLabels = [];
// the assets array for the selected reference
var assestArray = void 0;
// assets lines array
// used to update the line stye as the use drags the knob
var assetsLines = exports.assetsLines = [{}];
// assets amount to update the style of the line when dragging the knob
var totalAssets = exports.totalAssets = 0;
/* the assets menu animation, this will be added to the menu tweens array
 * in case the user has a particular reference visible and wants to change
 * the first level selection.
*/
var assetsMenuTween = exports.assetsMenuTween = void 0;

/* Boolean to indicate if the assets menu is being created. This will be
 * used in the draggable create method in order to accomodate the value
 * of the onDrag callback. In level 3 uses the negative value of y because
 * the knob is dragged from the bottom up. In the assets menu draggable, the
 * knob is dragged from the top to the bottom, so we need the positive value
 * of y in the callback. So instead of doing a conditional check on each drag
 * update, we'll use a different method on the callback.
*/
var isAssetVisible = exports.isAssetVisible = false;
/** Method to reset the asset menu visible boolean.
 * Sets the boolean to the passed value
 * @param {boolean} v the value for the boolean.
 * @private
*/
var _resetAssetVisible = exports._resetAssetVisible = function _resetAssetVisible(v) {
	return exports.isAssetVisible = isAssetVisible = v;
};

/* Depending on the current level the assets menu can have two radius
 * for it's path. Also depending on the amount of items, each path could
 * be short or long. For each level there's an array with the short and 
 * long paths.
*/
var assetsMenuPaths = {
	2: "M622.082,193.479 C687.795,231.53,732,302.603,732,384c0,40.106-10.732,77.706-29.48,110.083",
	/* [
 	"M622.082,193.479 C687.795,231.53,732,302.603,732,384c0,40.106-10.732,77.706-29.48,110.083",
 	"M622.082,193.479 C687.795,231.53,732,302.603,732,384c0,81.423-44.234,152.516-109.982,190.557"
 ], */
	3: "M709.988,186.008 C760.659,236.678,792,306.679,792,384c0,77.32-31.34,147.32-82.01,197.99"
	/* [
 	"M651.997,141.511 c62.059,35.829,110.45,95.333,130.462,170.019c20.012,74.685,7.857,150.411-27.972,212.469"
 ] */
};

/* In order to draw each element's line in the assets menu, we need the exact
 * point (x,y) on the specific curve. Since the test with the circle parametric
 * equation didn't worked, we'll use three different bezier tweens in order to
 * get the points for each level.
*/
var assetsLineBeziers = {
	// level two, first level is references
	2: [
	// menu base
	[{ x: 622.024, y: 193.479 }, { x: 687.795, y: 231.53 }, { x: 732, y: 302.603 }, { x: 732, y: 384 }, { x: 732, y: 424.106 }, { x: 721.268, y: 461.706 }, { x: 702.52, y: 494.083 }],
	// assets line
	[{ x: 632.021, y: 176.12 }, { x: 703.745, y: 217.62 }, { x: 752, y: 295.175 }, { x: 752, y: 384 }, { x: 752, y: 427.723 }, { x: 740.308, y: 468.715 }, { x: 719.881, y: 504.02 }],
	// separators line
	[{ x: 637.022, y: 167.458 }, { x: 714.262, y: 205.989 }, { x: 762, y: 291.474 }, { x: 762, y: 384 }, { x: 762, y: 429.544 }, { x: 749.821, y: 472.244 }, { x: 728.543, y: 509.02 }],
	// triangle position
	[{ x: 619.519, y: 197.774 }, { x: 683.772, y: 234.951 }, { x: 727, y: 304.427 }, { x: 727, y: 384 }, { x: 727, y: 423.168 }, { x: 716.526, y: 459.89 }, { x: 698.226, y: 491.518 }]],
	// level three, first level is highlight or costumer
	3: [
	// menu base
	[{ x: 709.988, y: 186.008 }, { x: 760.659, y: 236.678 }, { x: 792, y: 306.679 }, { x: 792, y: 384 }, { x: 792, y: 461.32 }, { x: 760.66, y: 531.32 }, { x: 709.99, y: 581.99 }],
	// assets line
	[{ x: 724.13, y: 171.865 }, { x: 778.42, y: 226.155 }, { x: 812, y: 301.156 }, { x: 812, y: 384 }, { x: 812, y: 466.843 }, { x: 778.421, y: 541.843 }, { x: 724.132, y: 596.132 }],
	// separators line
	[{ x: 731.201, y: 164.794 }, { x: 787.301, y: 220.893 }, { x: 822, y: 298.394 }, { x: 822, y: 384 }, { x: 822, y: 469.604 }, { x: 787.302, y: 547.104 }, { x: 731.203, y: 603.203 }],
	// triangle points
	[{ x: 706.452, y: 189.543 }, { x: 756.219, y: 239.309 }, { x: 787, y: 308.06 }, { x: 787, y: 384 }, { x: 787, y: 459.939 }, { x: 756.22, y: 528.689 }, { x: 706.454, y: 578.454 }]]
};

/* To get the points we use this arrays in a GSAP instance and for a specific
 * progress amount (that coresponds to each asset or separator) we get the
 * points. For each level there are 3 objects one to get the base point, the
 * asset final point (short line) and the separator final point (long line)
*/
var assetCurveObjects = {
	2: {
		base: { x: 0, y: 0 },
		asset: { x: 0, y: 0 },
		separator: { x: 0, y: 0 },
		triangle: { x: 0, y: 0 },
		rotation: 0
	},
	3: {
		base: { x: 0, y: 0 },
		asset: { x: 0, y: 0 },
		separator: { x: 0, y: 0 },
		triangle: { x: 0, y: 0 },
		rotation: 0
	}
};

/* The assets menu has a start and end separator that are always in the same
 * place, so we don't need to get those points from the bezier curves.
*/
var assetMenuExtremeLines = {
	2: [{ x1: 622.024, y1: 193.479, x2: 637.022, y2: 167.458 }, { x1: 702.52, y1: 494.083, x2: 728.543, y2: 509.02 }],
	3: [{ x1: 709.988, y1: 186.008, x2: 731.201, y2: 164.794 }, { x1: 709.99, y1: 581.99, x2: 731.203, y2: 603.203 }]
};

/* Finally create a GSAP instance (timeline) for each level in order to
 * update it's progress and get the points for the base and the type of
 * line, at a given angle.
*/
// second level
var levelTwoBezierLine = new TimelineLite({ paused: true });
// add the instances to the second level line
levelTwoBezierLine.to(assetCurveObjects[2].base, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[2][0], autoRotate: true
	}
}, 0).to(assetCurveObjects[2].asset, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[2][1]
	}
}, 0).to(assetCurveObjects[2].separator, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[2][2], autoRotate: true
	}
}, 0).to(assetCurveObjects[2].triangle, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[2][3], autoRotate: true
	}
}, 0);
// 

// third level
var levelThreeBezierLine = new TimelineLite({ paused: true });
levelTwoBezierLine.to(assetCurveObjects[3].base, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[3][0], autoRotate: true
	}
}, 0).to(assetCurveObjects[3].asset, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[3][1]
	}
}, 0).to(assetCurveObjects[3].separator, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[3][2], autoRotate: true
	}
}, 0).to(assetCurveObjects[3].triangle, 1, {
	ease: Linear.easeNone, bezier: {
		type: "cubic", values: assetsLineBeziers[3][3], autoRotate: true
	}
}, 0);
// 

/** Method to position the triangle.
 * @param {number} index the index position of the current target
 * @private
*/
var updateTrianglePos = exports.updateTrianglePos = function updateTrianglePos(index) {
	if (index > 0) {
		TweenLite.set(assetTriangle, _extends({}, trianglePoints[index], { autoAlpha: 1
		}));
	}
};

/** Method to create the asset menu lines
 * Draws the lines using the start and end point of each line, and
 * adds them to the asset menu container. All the container's child
 * elements must be removed before calling this code for the first time.
 * @param {object} points an object with the start and end points
 * @private
*/
var createElementLines = function createElementLines(points) {
	var elementLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
	TweenLite.set(elementLine, {
		className: "+=asset-element",
		attr: {
			x1: points.x1, x2: points.x2,
			y1: points.y1, y2: points.y2
		}
	});
	assetElsContainer.appendChild(elementLine);
	return elementLine;
};

/** Method to get the points for each asset line.
 * Uses the assets count object to get the total assets and loops through
 * the total amount of lines (asset and separator) to get the points of the
 * bezier curves, corresponding to that specific element.
 * @param {object} assetsObject the asset count object
 * @private
*/
var getPoints = function getPoints(assetsObject) {
	// clear the triangle position points array
	exports.trianglePoints = trianglePoints = [{}];
	// reset the asset lines array
	exports.assetsLines = assetsLines = [{}];
	// the separators marks
	var sepratatorMarks = createSeparators(assetsObject);
	/* To position the separator labels, we need to keep track of the
  * current separator that has to be placed. After placing it we go
  * to the next label in the collection. The collection is 0 index
  * and the first label is placed when the menu is created, so 
  * we start at 1.
 */
	var currentLabel = 1;
	// total assets is the count plus 3, because of the separators
	exports.totalAssets = totalAssets = getTotalAssets(assetsObject);
	var totalLines = totalAssets + (visibleLabels.length - 1);
	// the progress fraction according to the total lines
	var progressUnit = 1 / (totalLines + 1);
	// let lineIndex = 0;
	for (var i = 0; i < totalLines; i++) {
		// set the progress of the timeline
		levelTwoBezierLine.progress(progressUnit * (i + 1));
		// draw the line using the start and end points
		if (!sepratatorMarks[i]) {
			var _assetCurveObjects$cu = assetCurveObjects[_levelsModule.currentLevel],
			    base = _assetCurveObjects$cu.base,
			    asset = _assetCurveObjects$cu.asset,
			    triangle = _assetCurveObjects$cu.triangle;
			// add the points to the triangle position array
			// only for the assets, not the indicators

			trianglePoints.push({
				x: triangle.x,
				y: triangle.y,
				rotation: triangle.rotation,
				transformOrigin: "top center"
			});
			// add the asset line element to the array
			assetsLines.push(createElementLines({
				x1: base.x,
				y1: base.y,
				x2: asset.x,
				y2: asset.y
			}));
		} else {
			if (!visibleLabels[currentLabel]) return;
			// place the separator label
			TweenLite.set(visibleLabels[currentLabel], {
				x: assetCurveObjects[_levelsModule.currentLevel].separator.x + 5,
				y: assetCurveObjects[_levelsModule.currentLevel].separator.y + 2 * (currentLabel - 1),
				rotation: assetCurveObjects[_levelsModule.currentLevel].separator.rotation - 90,
				transformOrigin: "left center",
				display: "block"
			});
			// go to the next label
			currentLabel++;
			// draw the separator line
			createElementLines({
				x1: assetCurveObjects[_levelsModule.currentLevel].base.x,
				y1: assetCurveObjects[_levelsModule.currentLevel].base.y,
				x2: assetCurveObjects[_levelsModule.currentLevel].separator.x,
				y2: assetCurveObjects[_levelsModule.currentLevel].separator.y
			});
		}
	} // loop
};

/* The assets menu has two different drag paths radius depending on the
 * current level. Also has a short and long drag paths depending on the
 * amount of items.
*/
var assetsDragPaths = exports.assetsDragPaths = {
	2: "M682.029,89.503C783.639,148.295,852,258.164,852,384 c0,61.941-16.563,120.013-45.502,170.028",
	3: "M808.984,87.014 C884.99,163.019,932,268.02,932,384c0,115.98-47.01,220.98-123.015,296.985"
};

// the bezier arrays for each level assets drag
var assetsDragArrays = exports.assetsDragArrays = {
	2: [{ x: 652.029, y: 59.503 }, { x: 753.639, y: 118.295 }, { x: 822, y: 228.164 }, { x: 822, y: 354 }, { x: 822, y: 415.94 }, { x: 805.437, y: 474.012 }, { x: 776.498, y: 524.028 }],
	3: [{ x: 778.984, y: 57.014 }, { x: 854.99, y: 133.019 }, { x: 902, y: 238.02 }, { x: 902, y: 354 }, { x: 902, y: 469.98 }, { x: 854.99, y: 574.98 }, { x: 778.985, y: 650.985 }]
};

/** Method to calculate the total assets.
 * Gets the object with the asset count of the reference
 * and returns the total amount of assets.
 * @param {object} assetCount object with the amount of ach asset type
 * @returns {number} total assets
 * @private
*/
var getTotalAssets = function getTotalAssets(assetCount) {
	var totalAssets = 0;
	for (var type in assetCount) {
		totalAssets += assetCount[type];
	}
	return totalAssets;
};

/** Method to create the separators of the assets menu.
 * This method gets the assets count object and based on that data creates
 * an array with numbers, that indicates if the asset menu should add a line
 * for an asset or a separator (indicator) for a specific asset type.
 * @param {object} assets the assets count object for the reference
 * @returns {array} an array with numbers indicating asset or separator
 * @private
*/
var createSeparators = function createSeparators(assets) {
	var lines = [];
	visibleLabels = [];
	if (assets.image > 0) {
		// add to the visible labels
		visibleLabels.push(assetsLabels[0]);
		// add the asset lines
		for (var i = 1; i <= assets.image; i++) {
			lines.push(0);
		}
	}
	if (assets.video > 0) {
		// add separator only if labels array lenght is more than 0
		if (visibleLabels.length > 0) {
			// add the separator
			lines.push(1);
		}
		// add label
		visibleLabels.push(assetsLabels[1]);
		// add asset lines
		for (var _i = 1; _i <= assets.video; _i++) {
			lines.push(0);
		}
	}
	if (assets[360] > 0) {
		// add separator only if labels array lenght is more than 0
		if (visibleLabels.length > 0) {
			// add the separator
			lines.push(1);
		}
		// add label
		visibleLabels.push(assetsLabels[2]);
		// add asset lines
		for (var _i2 = 1; _i2 <= assets[360]; _i2++) {
			lines.push(0);
		}
	}
	if (assets.document > 0) {
		// add separator only if labels array lenght is more than 0
		if (visibleLabels.length > 0) {
			// add the separator
			lines.push(1);
		}
		// add label
		visibleLabels.push(assetsLabels[3]);
		// add asset lines
		for (var _i3 = 1; _i3 <= assets.document; _i3++) {
			lines.push(0);
		}
	}

	return lines;
};

/** Method to create the assets menu.
 * Gets the selected reference data. With that we have the amount
 * of assets of that particular reference and the separation between
 * them.
 * The method then uses the same principle of the method to create the
 * segments, with the difference that this method only uses a start angle
 * but two different radius to get the points for the lines. An asset 
 * has a specific separation between each radius and the separator or
 * indicator of the asset type, has a bigger distance between radius.
 * @param {array} assets the assets array
 * @param {object} assetCount the asset count object
 * @private
*/
var createAssetsMenu = exports.createAssetsMenu = function createAssetsMenu(assets, assetCount) {
	// get the separation between assets
	// the amount of images
	var refImages = _.filter(assets, ["type", "image"]);
	// the amount of videos
	var refVideos = _.filter(assets, ["type", "video"]);
	// the amount of 360
	var ref360 = _.filter(assets, ["type", "360"]);
	// the amount of documents
	var refDocs = _.filter(assets, ["type", "document"]);

	// remove the path from the asset base menu and the element lines
	// before creating the menu again
	assetMenuContainer.setAttribute("d", "");
	assetElsContainer.innerHTML = "";

	// add the d attr to the asset menu base based on the level
	assetMenuContainer.setAttribute("d", assetsMenuPaths[_levelsModule.currentLevel]);

	// hide the labels
	TweenLite.set(assetsLabels, { display: "none" });

	// create the first and last separators
	createElementLines(assetMenuExtremeLines[_levelsModule.currentLevel][0]);
	createElementLines(assetMenuExtremeLines[_levelsModule.currentLevel][1]);
	// reset the current assets element index
	(0, _draggableKnobModule.resetCurrentAssetsElement)();
	// get the points of the current ref to draw the lines
	getPoints(assetCount);
	/* Place the label for the first separator.
  * For this we use the points already defined for the current level.
 */
	TweenLite.set(visibleLabels[0], {
		x: assetMenuExtremeLines[_levelsModule.currentLevel][0].x2 + 5,
		y: assetMenuExtremeLines[_levelsModule.currentLevel][0].y2 - 5,
		rotation: _levelsModule.currentLevel === 2 ? -60 : -45, transformOrigin: "left center",
		display: "block"
	});
	// now animate the assets menu in
	assetsMenuTween.play();
};

/** Method to init the Assets Menu Module
 * Creates the references after the DOM nodes are placed.
 * Sets the initial state and position of the elements after the to
 * prevent errors.
 * Creates the GSAP instances that will be used through the app's lifecycle.
 * @private
*/
var _initAssetsMenu = exports._initAssetsMenu = function _initAssetsMenu() {
	// create the reference to the DOM elements
	assetMenuContainer = document.getElementById("asset-menu-base");
	assetElsContainer = document.getElementById("asset-menu-element");
	exports.fourthLevelMenu = fourthLevelMenu = document.getElementById("fourth-level-menu");
	assetsLabels = document.querySelectorAll(".asset-label");
	exports.assetTriangle = assetTriangle = document.getElementById("asset-triangle");

	// create the assets menu animation
	exports.assetsMenuTween = assetsMenuTween = TweenLite.to(fourthLevelMenu, 0.2, {
		autoAlpha: 1, paused: true
	});
};

/***/ }),

/***/ "./src/buttons-module.js":
/*!*******************************!*\
  !*** ./src/buttons-module.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initBtnModule = exports.setCloseButton = exports._setClosedClicked = exports.isCloseClicked = exports.closeButton = undefined;

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

var _imageModule = __webpack_require__(/*! ./image-module */ "./src/image-module.js");

var _paginationModule = __webpack_require__(/*! ./pagination-module */ "./src/pagination-module.js");

var _dataModule = __webpack_require__(/*! ./data-module */ "./src/data-module.js");

// pagination module

// draggable knob module

// assets menu module
var closeButton = exports.closeButton = void 0;
// the position of the close button for each menu level

// data module

// image module

// text module
/*
*******************************************************************************************
		BUTTONS MODULE
*******************************************************************************************
*/
// levels module
var closeBtnPositions = [null, { x: 730, y: 250 }, { x: 338, y: 110 }];
// the positions of the close button when the assets menu is visible
var closeAssetsPositions = {
	2: { x: 850, y: 550 },
	3: { x: 870, y: 680 }
};
/* Bool to check if the menu instances are reversing because
 * the close button was clicked or the first level element was
 * clicked.
*/
var isCloseClicked = exports.isCloseClicked = false;

/** Method to set the closed clicked boolean
 * @param {boolean} v the bool value for the variable
 * @private
*/
var _setClosedClicked = exports._setClosedClicked = function _setClosedClicked(v) {
	return exports.isCloseClicked = isCloseClicked = v;
};

/** Method to position the close button.
 * Uses the current level to set the position of the close button
 * according to the coordinates in the positions array.
 * @private
*/
var setCloseButton = exports.setCloseButton = function setCloseButton() {
	// get the position for this level
	// check if the assets menu is visible, in that case get the position
	// for the current level 
	var currentBtnPosition = _assetsMenuModule.isAssetVisible ? closeAssetsPositions[_levelsModule.currentLevel] : closeBtnPositions[_levelsModule.currentLevel];
	TweenLite.set(closeButton, {
		x: currentBtnPosition.x, y: currentBtnPosition.y
	});
};

/** Method for the close button click handler.
 * When the user clicks on the close button, the drag path, knob and the close
 * button should fade out.
 * Then the current level tween should be reversed.
 * Then the drag paths should be updated and the knob position should change
 * for the current level.
 * Finally the current level index should be updated.
 * @private
*/
var closeBtnHandler = function closeBtnHandler() {
	/* If the assets menu was visible and the user clicked the close button
  * then hide the assets menu, the drag elements and the close button,
  * set the new position of the close button and drag elements and finally
  * show the drag elements and the close button
 */
	if (_assetsMenuModule.isAssetVisible) {
		TweenLite.set(_assetsMenuModule.assetTriangle, { autoAlpha: 0 });
		// hide the elements
		(0, _draggableKnobModule.hideDragElements)();
		// hide the assets menu
		_assetsMenuModule.assetsMenuTween.reverse();
		// update level index
		(0, _levelsModule._changeCurrentLevelValue)(false);
		(0, _draggableKnobModule.updateLevelSegment)(false);
		// set the assets menu bool to false
		(0, _assetsMenuModule._resetAssetVisible)(false);
		// show the pagination controls
		(0, _paginationModule.showPaginationControls)();
		// place the drag and close button
		return (0, _draggableKnobModule.placeKnob)();
	}
	exports.isCloseClicked = isCloseClicked = true;
	// hide the elements
	(0, _draggableKnobModule.hideDragElements)();
	// reverse the current level
	_levelsModule.menuTweensArray[_levelsModule.currentLevel] ? (0, _levelsModule.reverseLevelsTweens)(null, true) : null;
	// update the current level index
	(0, _levelsModule._changeCurrentLevelValue)(false);
	// update the text source
	(0, _textModule.setTextSource)();
	// set the pagination data
	(0, _paginationModule.setPaginationData)(true);
	(0, _draggableKnobModule.updateLevelSegment)(false);
	(0, _levelsModule._setCurrentLevelSegmentsAmount)();
	// check the current level and hide the image wrapper if the level is 0
	// hide the image if the first level is customer, since customers don't
	// have a thumb in their data, just references have thumb
	if (_levelsModule.currentLevel === 0 || _levelsModule.currentLevel === 1 && _levelsModule.currentFirstLevelSelection == "customer") _imageModule.refImageTween.reverse();
	// if the current level is 0, means that the reference object should be empty
	if (_levelsModule.currentLevel === 0) (0, _dataModule.updateRefObject)(null);
};

/* PLAY BUTTON */
var playBtn = void 0;
/** Play Button Click Handler.
 * Check the current menu level and uses the knob click handler
 * @private
*/
var playBtnHandler = function playBtnHandler() {
	// first check if the current level is more than 0
	// if the current level is more than 0, the use the knob click handler
	// other wise do nothing
	if (_levelsModule.currentLevel > 0) {
		(0, _draggableKnobModule.knobClickHandler)();
	}
};

/** Method to init the Buttons Module.
 * Creates the variables after the DOM nodes are present.
 * @private
*/
var _initBtnModule = exports._initBtnModule = function _initBtnModule() {
	// create the references once the DOM elements are placed
	exports.closeButton = closeButton = document.getElementById("close-button");
	playBtn = document.getElementById("play-btn");
	// add the close button click handler
	closeButton.onclick = closeBtnHandler;
};

/***/ }),

/***/ "./src/data-module.js":
/*!****************************!*\
  !*** ./src/data-module.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.firstLevel = exports._fetchServerData = exports.updateRefObject = exports.createNewRefs = exports._setAppData = exports._setSelectedCustomer = exports.selectedCustomer = exports.customersReferences = exports.customersCount = exports.appCustomers = exports.newHighlightRefsCount = exports.newHighlightRefs = exports.highlightRefsObject = exports.highlightCount = exports.highlightRefs = exports.referencesCount = exports.appReferences = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /*
                                                                                                                                                                                                                                                                  *******************************************************************************************
                                                                                                                                                                                                                                                                  		DATA MODULE
                                                                                                                                                                                                                                                                  *******************************************************************************************
                                                                                                                                                                                                                                                                  */
/* This module gets the data from the server and then separates it for each
 * type. 
*/
// get lodash


// text module

// draggable knob module

// assets menu module


var _lodash = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

// app references
var appReferences = exports.appReferences = void 0,
    referencesCount = exports.referencesCount = void 0;
// highlight references
var highlightRefs = exports.highlightRefs = void 0,
    highlightCount = exports.highlightCount = void 0;
// the object for all the highlight refs and it's child refs
var highlightRefsObject = exports.highlightRefsObject = {};
var newHighlightRefs = exports.newHighlightRefs = [];
var newHighlightRefsCount = exports.newHighlightRefsCount = 0;
// customers
var appCustomers = exports.appCustomers = void 0,
    customersCount = exports.customersCount = void 0;
// separate the refs for each customer
var customersReferences = exports.customersReferences = {};
// the selected customer
var selectedCustomer = exports.selectedCustomer = void 0;

/** Method to set the selected customer.
 * @param {string} customerId the id of the selected customer
 * @private
*/
var _setSelectedCustomer = exports._setSelectedCustomer = function _setSelectedCustomer(customerId) {
	return exports.selectedCustomer = selectedCustomer = customerId;
};

/** Method to separate the app data.
 * Gets the JSON data and stores each different data type on it's own constant
 * @param {object} data the JSON data from the request
 * @private
*/
var _setAppData = exports._setAppData = function _setAppData(data) {
	var start = Date.now();
	// set the app refs
	exports.appReferences = appReferences = data.references.filter(function (ref) {
		return ref.type !== "highlight";
	});
	exports.referencesCount = referencesCount = appReferences.length;
	// set the customers
	exports.appCustomers = appCustomers = data.allCustomers;
	exports.customersCount = customersCount = appCustomers.length;
	// loop through the customers and create a key:val pair for each customer
	// and the val should be an array with all the references for that customer
	appCustomers.forEach(function (customer) {
		// get all the refs for this customer
		customersReferences[customer.id] = (0, _lodash.filter)(data.references, function (ref) {
			return ref.customer.id === customer.id;
		});
	});
	// create the highlight references
	exports.highlightRefs = highlightRefs = (0, _lodash.filter)(data.references, function (ref, i) {
		if (ref.type === "highlight") {
			// create the updated child array with the assets and
			// assets count from the parent
			var updatedRefs = (0, _lodash.map)(ref.references, function (child) {
				var newChild = Object.assign({}, child);
				// newChild.assets = ref.assets;
				// newChild.asset_count = ref.asset_count;
				newChild.thumb = ref.thumb;
				return newChild;
			});
			// create the updated highlight
			// this has the updated child with the assets and thumb from
			// it's parent.
			var newRef = [].concat(ref, updatedRefs);
			// const newRef = [].concat(ref, ref.references);
			// now add the updated highlight to the highlights object
			highlightRefsObject[ref.id] = newRef;
			return true;
		}
	});
	exports.highlightCount = highlightCount = highlightRefs.length;
	// set the first level counts
	firstLevel.customer = customersCount;
	firstLevel.highlight = highlightCount;
	firstLevel.references = referencesCount;
};

/** Method for the highlight ref selection
 * When the user selects a ref from the highlight list, we need a new array
 * that's a copy of the references array. The difference is that the selected
 * highlight is removed from the array and added as the first element.
 * @private
*/
/** Method for a Highlight Ref Selection
 * When the user selects a highlight ref, the next level should show the
 * selected ref and it's child references. For that we create an array
 * with the selected highlight as the first element and it's child refs
 * after that.
 * Child refs don't have assets, so we add the assets of the parent to them
 * in order to show those in case a child is selected.
 * @private
*/
var createNewRefs = exports.createNewRefs = function createNewRefs() {
	// get the selected highlight
	var targetRef = _textModule.currentTextSource[_draggableKnobModule.currentLevelElement];
	// console.log( highlightRefsObject[targetRef.id] );
	/* Add the parent's assets to each child ref
  * Create a new array with the updated child references,
  * at this point the child refs live in an object in the parent ref.
 */
	var childRefs = (0, _lodash.map)(targetRef.references, function (ref) {
		var newChild = Object.assign({}, ref);
		// newChild.assets = targetRef.assets;
		// newChild.asset_count = targetRef.asset_count;
		newChild.thumb = targetRef.thumb;
		return newChild;
	});
	// update the new highlights array
	exports.newHighlightRefs = newHighlightRefs = [].concat(targetRef, childRefs);
	// set the new amount of highlight references
	exports.newHighlightRefsCount = newHighlightRefsCount = newHighlightRefs.length;
};

/** Method to update the Window Reference Object
 * Checks if the parameter is a reference or a customer and
 * updates the global reference object.
 * @param {object} ref the reference object
 * @private
*/
var updateRefObject = exports.updateRefObject = function updateRefObject(ref) {

	if (ref === null) {
		return window.referenceObj = {
			reference: null,
			type: null,
			parent: null,
			customer: null,
			mediaType: null,
			mediaIndex: null
		};
	}
	/* If the assets menu is visible a reference has been selected
  * so we only update the media type and index properties
 */
	if (_assetsMenuModule.isAssetVisible) {
		var newRefObject = _extends({}, window.referenceObj, {
			mediaType: ref.type,
			mediaIndex: ref.sorting
		});
		return window.referenceObj = newRefObject;
	}
	// check if it's a reference or customer
	// a reference has a customer property a customer doesn't
	if (ref.customer) {
		// reference
		window.referenceObj = {
			reference: ref.id || null,
			type: ref.type || null,
			parent: ref.pivot ? ref.pivot.reference_parent_id : null,
			customer: ref.customer.id || null,
			mediaType: null,
			mediaIndex: ref.assets ? ref.assets[_draggableKnobModule.currentAssetsElement - 1] ? ref.assets[_draggableKnobModule.currentAssetsElement - 1].sorting : null : null
		};
	} else {
		// customer, just update the customer and 
		window.referenceObj = {
			reference: null,
			type: null,
			parent: null,
			customer: ref.id || null,
			mediaType: null,
			mediaIndex: null
		};
	}
};

/** Method to get the data from the server
 * @param {string} url the data url from the server
 * @private
*/
var _fetchServerData = exports._fetchServerData = function _fetchServerData(url) {
	// local - no server
	_setAppData(url);
};

// second level elements segments
var firstLevel = exports.firstLevel = {
	customer: customersCount,
	highlight: highlightCount,
	references: referencesCount
};

/***/ }),

/***/ "./src/draggable-knob-module.js":
/*!**************************************!*\
  !*** ./src/draggable-knob-module.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initKnobModule = exports.knobClickHandler = exports.hideDragElements = exports.placeKnob = exports.updateLevelSegment = exports._resetCurrentLevelElement = exports.resetCurrentAssetsElement = exports.currentAssetsElement = exports.currentLevelElement = undefined;

var _dataModule = __webpack_require__(/*! ./data-module */ "./src/data-module.js");

var _buttonsModule = __webpack_require__(/*! ./buttons-module */ "./src/buttons-module.js");

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _imageModule = __webpack_require__(/*! ./image-module */ "./src/image-module.js");

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

var _paginationModule = __webpack_require__(/*! ./pagination-module */ "./src/pagination-module.js");

// draggable knob

// assets menu module

// text module

// buttons module
var draggableKnob = void 0;
// drag path container

// pagination module

// image module

// levels module
/*
*******************************************************************************************
		DRAGGABLE KNOB MODULE
*******************************************************************************************
*/
// data module
var dragPathContainer = void 0;
// drag path track, this is always visible
var dragPathTrack = void 0;
// drag path reveal, this is revealed as the user drags the knob
var dragPathReveal = void 0;
// the timeline for the bezier and draw
var dragLine = new TimelineLite({ paused: true });
// the path reveal tween
var pathRevealTween = void 0;

/* Array with all the drag path strings.
 * Each element is the strig of each drag path tag, that's used in
 * the d attribute of the drag path. When a new level is selected,
 * we update the attribute of the drag path track, position the drag
 * knob and then reveal the elements.
*/
var dragPathStrings = [
// level 0 no drag knob visible
null,
// level 1
"M312.744,269.045 C352.505,200.273, 426.849,154, 512,154 c85.151,0, 159.495,46.273, 199.257,115.046",
// level 2
"M371.976,626.527C288.297,578.11,232,487.629,232,384 c0-103.63,56.297-194.111,139.977-242.527",
// level 3
"M702.029,54.854 C815.595,120.562,892,243.358,892,384c0,69.228-18.512,134.132-50.856,190.032"];

/* Each level has it's own drag path, that drag path is described
 * as a cubic bezier path, so we get those paths and create the arrays
 * for each path, in order to use them for the draggable knob when
 * each level is visible.
*/
var dragPathsArrays = [null, // level 0
// level 1
[{ x: "282.744", y: "239.045" }, // start point
{ x: "322.505", y: "170.273" }, { x: "396.849", y: "124" }, // first set of control points
{ x: "482", y: "124" }, // second point
//    c85.151,0,                       159.495,46.273,
{ x: "567.151 ", y: "124" }, { x: "641.495", y: "170.273" }, // second ste of control points
// 199.257          115.046
{ x: "677.657", y: "235.446" // final point
}],
// level 2
[{ x: "341.976", y: "596.527" }, // start point
{ x: "258.297", y: "548.11" }, { x: "202", y: "457.629" }, // first set of control points
{ x: "202", y: "354" }, // second point
{ x: "202", y: "250.37" }, { x: "258.297", y: "159.889" }, // second set of control points
{ x: "341.977", y: "111.472" // final point
}]];

/* When the user selects a reference to show it's assets
 * store that reference in order to access that info when the
 * users selects a specific asset
*/
var selectedRef = void 0;

/* When the knob position is updated, the corresponding element
 * of the current level shold be white and the previous one should
 * go back to the grey color.
*/
var currentLevelElement = exports.currentLevelElement = 0;
// same as current level element is the current asset element
var currentAssetsElement = exports.currentAssetsElement = 0;

/** Method to toggle the knob disable state
 * Changes the state of the draggable knob to disabled or not
 * depending on the value passed.
 * @param {boolean} disable
 * @private
*/
var toggleKnobDisableState = function toggleKnobDisableState(disable) {
	TweenLite.set(draggableKnob, { opacity: disable ? 0.35 : 1 });
};

/** Method to reset the current asset element.
 * After the close button is clicked, the current assets element
 * index vaule reflects the last ref's assets. If a new ref is
 * selected they update could trigger an error.
 * Use this method when the assets menu is created.
 * @private
*/
var resetCurrentAssetsElement = exports.resetCurrentAssetsElement = function resetCurrentAssetsElement() {
	exports.currentAssetsElement = currentAssetsElement = 0;
};
/* Object to store the index of the segment of each level.
 * When the user calls the drag knob click handler the index
 * value of the current level will be updated. This will be 
 * used then to remove the highlight color and glow from the
 * segment of the corresponding level.
 * When the close button is clicked, the value of the current
 * level will be set to 0.
 * By default the values are 0 for each level
*/
var levelsSegmentIndex = { 1: 0, 2: 0 };
/** Method to reset the current level element
 * Sets the current level element to 0 when a new level is opened
 * In case of a paginatin event executing this method, instead of 0
 * we set the value to the target. This because in a new group the
 * target index will be the first element of the new group, but the
 * data is just one array, so while the current target could be 39
 * the next group first element's index is 40 and not 0. Going to a
 * previous group is the same, the index of thefirst element of that
 * group is 40 but the first of the previous group is 0.
 * @param {number} target the number the element should be set
 * @private
*/
var _resetCurrentLevelElement = exports._resetCurrentLevelElement = function _resetCurrentLevelElement(target) {
	exports.currentLevelElement = currentLevelElement = target || 0;
};

// the draggable dummy element
var dragDummy = document.createElement('div');

/** Method to update the element for the current level.
 * When the user drags the knob, the level segments will change
 * color to indicate the selected one, and the level's text container
 * will be updated with the corresponding text.
 * @param {number} target the index of the target element
 * @private
*/
var updateLevelSegment = exports.updateLevelSegment = function updateLevelSegment(target) {
	// check if the update is because the close button was clicked
	// set the color of the target element to white
	if (target === false) {
		// if the current level is 0, the menu base reset the current element index
		// and stop the code
		if (_levelsModule.currentLevel === 0) {
			// hide the image wrapper
			_imageModule.refImageTween.reverse();
			return exports.currentLevelElement = currentLevelElement = 0;
		}
		// if the current level is 1 or 2, we need to remove the style
		// for the selected element and update the level's segment index 
		// value in the levels segment index
		if (_levelsModule.currentLevel === 1 || _levelsModule.currentLevel === 2) {
			// update the text for the level
			(0, _textModule.applyElementText)(true);
			// update the image source to the first element of the selected
			// references.
			(0, _imageModule.updateImageSrc)(_textModule.currentTextSource[0].thumb);
			// the current level's path array
			var currentPathArray = _levelsModule.levelPathsArrays[_levelsModule.currentLevel];
			// remove the style of the selected element
			TweenLite.set(currentPathArray[levelsSegmentIndex[_levelsModule.currentLevel]], {
				className: "-=selected-segment"
			});
			// set the first segment of the level as the selected element
			TweenLite.set(currentPathArray[0], {
				className: "+=selected-segment"
			});
			// set the level's segment index to 0
			levelsSegmentIndex[_levelsModule.currentLevel] = 0;
		}
		return exports.currentLevelElement = currentLevelElement = 0;
	}
	if (target !== currentLevelElement) {
		/* Since the app has pagination, we need to correct the target value
   * with the current pagination group index in order to reflect the
   * real target of the target path array. The path array has all the elemenets
   * of the current level/selection (could be more than 40), but the visible
   * elements are never more than 40, so we correct that. The visual update
   * is for the visible element but the text and reference object is for the
   * element contained in the 
  */
		var correctedTarget = target + 40 * (_paginationModule.paginationData[_levelsModule.currentLevel].currentGroup - 1);
		var targetPathArray = _levelsModule.levelPathsArrays[_levelsModule.currentLevel];
		// the new target reference/customer
		var targetRefCus = _textModule.currentTextSource[correctedTarget];
		/* Check if the target element (from the text source) has assets in it.
   * This also means that we have to check if the current level is either
   * 1 or 2 and also that the current text source has an assets property
   * in it. If the assets array is empty, set the knob state to disabled
   * by changing it's alpha channel to 0.75
  */
		if (targetRefCus.assets) {
			toggleKnobDisableState(targetRefCus.assets.length === 0);
		}
		// update the knob position
		// update the reference object
		(0, _dataModule.updateRefObject)(_textModule.currentTextSource[correctedTarget]);
		// set the color of the target element to white
		TweenLite.set(targetPathArray[target], {
			className: "+=selected-segment"
		});
		// set the color of the current level to the default
		TweenLite.set(targetPathArray[currentLevelElement], {
			className: "-=selected-segment"
		});
		// set the current level var
		exports.currentLevelElement = currentLevelElement = target;
		// update the text for the level
		(0, _textModule.applyElementText)(false, correctedTarget);
		// update the image source
		(0, _imageModule.updateImageSrc)(_textModule.currentTextSource[correctedTarget].thumb);
	}
};

/** Method to update the second level drag Timeline.
 * Updates the timeline using the x position of the draggable instance
 * and the width of the drag path container.
 * @private
*/
var updateHorDragLine = function updateHorDragLine() {
	var dragVal = this.x;
	var pathContainerDim = dragPathContainer.getBBox().width;
	var currentProgress = dragVal / pathContainerDim;
	// update the progress of the GSAP instances
	dragLine.progress(currentProgress);
	pathRevealTween.progress(currentProgress);
	var targetElement = Math.floor(currentProgress * (_levelsModule.currentLevelSegmentsAmount - 1) + 0.4);
	// update the segment
	updateLevelSegment(targetElement);
};

/** Method to update the third level drag Timeline.
 * This updates the progress of the drag timeline using the y value
 * of the draggable instance and the height of the drag path container.
 * Since the knob in this level goes from the bottom of the path to the
 * top, we use the negative value of the instance.
 * @private
*/
var updateVertDragLine = function updateVertDragLine() {
	var dragVal = -this.y;
	var pathContainerDim = dragPathContainer.getBBox().height;
	var currentProgress = dragVal / pathContainerDim;
	// update the progress of the GSAP instances
	dragLine.progress(currentProgress);
	pathRevealTween.progress(currentProgress);
	var targetElement = Math.floor(currentProgress * (_levelsModule.currentLevelSegmentsAmount - 1) + 0.4);
	// update the segment
	updateLevelSegment(targetElement);
};

/** Method to update the assets drag Timeline.
 * This updates the progress of the drag timeline using the y value
 * of the instance and the height of the drag path container.
 * Since the knob goes from the top to the bottom, we use the positive
 * vale of the instance.
 * @private
*/
var updateAssetsDragLine = function updateAssetsDragLine() {
	var dragVal = this.y;
	var pathContainerDim = dragPathContainer.getBBox().height;
	var currentProgress = dragVal / pathContainerDim;
	// update the progress of the GSAP instances
	dragLine.progress(currentProgress);
	pathRevealTween.progress(currentProgress);
	var targetElement = Math.floor(currentProgress * _assetsMenuModule.totalAssets + 0.25);
	// check that the target is different from the current selected element
	if (targetElement > 0 && targetElement !== currentAssetsElement) {
		TweenLite.set(_assetsMenuModule.assetsLines[targetElement], { className: "+=selected" });
		TweenLite.set(_assetsMenuModule.assetsLines[currentAssetsElement], { className: "-=selected" });
		(0, _assetsMenuModule.updateTrianglePos)(targetElement);
		exports.currentAssetsElement = currentAssetsElement = targetElement;
		// update the reference object
		(0, _dataModule.updateRefObject)(selectedRef.assets[targetElement - 1]);
		// update the image with the asset thumb
		(0, _imageModule.updateImageSrc)(selectedRef.assets[targetElement - 1].thumb);
	}
};

/** Method to create the Drag bounds.
 * Sets the drag bounds of the knob draggable instance, depending
 * on the level and if the assets menu is visible or not.
 * @param {boolean} assets if the assets menu is visible or not
 * @returns {object} the object with the bounds
 * @private
*/
var createDragBounds = function createDragBounds(assets) {
	if (_levelsModule.currentLevel === 1) {
		return {
			minX: 0, maxX: dragPathContainer.getBBox().width,
			minY: 0, maxY: dragPathContainer.getBBox().height
		};
	} else if (_levelsModule.currentLevel > 1 && !assets) {
		return {
			minX: 0, maxX: dragPathContainer.getBBox().width,
			minY: -dragPathContainer.getBBox().height, maxY: 0
		};
	} else if (_levelsModule.currentLevel > 1 && assets) {
		return {
			minX: 0, maxX: dragPathContainer.getBBox().width,
			minY: 0, maxY: dragPathContainer.getBBox().height
		};
	}
};

/** Method to place the knob
 * This is used to place the knob when a new level is available.
 * After a new level is made available, call this method to place
 * the knob at the starting point of that level's drag path.
 * After placing the knob, create the draggable instance and then
 * create the live-snap array values considering the amount of elements
 * of that level.
 * The param passed indicates if the string and array for the path and
 * bezier are the ones of the menu level or the assets for that particular
 * level.
 * @param {bool} assets wheater the assets menu will be visible
 * @private
*/
var placeKnob = exports.placeKnob = function placeKnob(assets) {
	/* update the reference object only if the level is 1
  * if the level is not 1, the ref object will be updated in the
  * knob click handler, so there's no need to run that code here
  * as well, since creates a duplicate.
 */
	if (_levelsModule.currentLevel === 1) (0, _dataModule.updateRefObject)(_textModule.currentTextSource[currentLevelElement]);
	/* Check if the assets menu should be shown.
  * Check the current level, in order to set the start position
  * of the knob.
 */
	var dragBezierArray = void 0,
	    dragPathCurve = void 0;
	if (assets) {
		// set the selected reference
		selectedRef = _textModule.currentTextSource[currentLevelElement];
		// reset the current asset element
		exports.currentAssetsElement = currentAssetsElement = 0;
		// set the target bezier curves
		dragBezierArray = _assetsMenuModule.assetsDragArrays[_levelsModule.currentLevel];
		dragPathCurve = _assetsMenuModule.assetsDragPaths[_levelsModule.currentLevel];
		// create the assets menu
		(0, _assetsMenuModule.createAssetsMenu)(selectedRef.assets, selectedRef.asset_count);
	} else {
		dragBezierArray = dragPathsArrays[_levelsModule.currentLevel];
		dragPathCurve = dragPathStrings[_levelsModule.currentLevel];
		// reset the current level element index
		exports.currentLevelElement = currentLevelElement = 0;
	}
	/* In order to avoid an issue when the drag knob jumps to an unexpected position
  * clear the drag dummy props. If a new Draggable instance is created and the
  * position of the knob at that moment is not the 
 */
	TweenLite.set([dragDummy], { clearProps: "all" });
	// place the knob in the start of the drag path
	TweenLite.set(draggableKnob, {
		x: dragBezierArray[0].x,
		y: dragBezierArray[0].y
	});
	// add the string to the d attribute in the drag path containers
	dragPathTrack.setAttribute("d", dragPathCurve);
	dragPathReveal.setAttribute("d", dragPathCurve);
	// clear the start values of the drag reveal tween
	TweenLite.set(dragPathReveal, { drawSVG: "100%" });
	pathRevealTween.kill();
	pathRevealTween = TweenLite.from(dragPathReveal, 1, {
		drawSVG: "0%", ease: Linear.easeNone, paused: true
	});
	/* Clear and assign the drag timeline to the new instances
  * based on the target level
 */
	dragLine.kill().clear();
	pathRevealTween.pause(0);
	// test a bezier tween
	dragLine.to(draggableKnob, 1, {
		bezier: { type: "cubic", values: dragBezierArray },
		ease: Linear.easeNone
	}, 0);
	//
	// depending on the level, set the callback of the instance
	var dragCallback = _levelsModule.currentLevel === 1 ? updateHorDragLine : _levelsModule.currentLevel > 1 && !assets ? updateVertDragLine : updateAssetsDragLine;
	// now create the draggable instance
	Draggable.create(dragDummy, {
		trigger: draggableKnob,
		// dragResistance: 0.05,
		onClick: knobClickHandler,
		type: "x, y",
		// bounds: dragBounds,
		bounds: createDragBounds(assets),
		onDrag: dragCallback
	}); // draggable
	// position the close button
	(0, _buttonsModule.setCloseButton)();
	// now show the drag elements
	showDragElements();
};
// the instances that show and hide the drag elements
// we include the close button
var showDragTween = void 0;

/** Method to hide the drag path and knob
 * 
*/
var showDragElements = function showDragElements() {
	// reset the close button bool
	(0, _buttonsModule._setClosedClicked)(false);
	// show the drag elements
	showDragTween.play();
};

/** Method to show the drag path and knob
 * 
*/
var hideDragElements = exports.hideDragElements = function hideDragElements() {
	return showDragTween.reverse();
};

/** Draggable Knob Click Handler
 * Increases the current level index.
 * The knob is visible starting at the second level and depending
 * on the first level selection, the type of third level it'll create.
 * If the first level selection is "references" then the third level
 * will be the assets of that particular reference. If the first level
 * selection is customer or highlight, then the third level will be either
 * the references of that customer, or all references.
 * If the current level is the third then the click handler will create the
 * fourth level with the assets of that particular reference.
 * @private
*/
var knobClickHandler = exports.knobClickHandler = function knobClickHandler() {
	/* Update the reference object of the global element
  * If the assets menu is visible, then update with the corresponding
  * asset. If the assets menu is not visible, update with the currently
  * selected reference.
  * Also if the assets menu is visible, hide the pagination controls.
 */
	if (_assetsMenuModule.isAssetVisible) return (0, _dataModule.updateRefObject)(selectedRef);
	// if the level is 1 or 2 store the index of the selected segment
	// also store the selected image url
	if (_levelsModule.currentLevel === 1 || _levelsModule.currentLevel === 2) {
		levelsSegmentIndex[_levelsModule.currentLevel] = currentLevelElement;
		// update the image level in the object
		_imageModule.selectedLevelImages[_levelsModule.currentLevel] = _textModule.currentTextSource[currentLevelElement].thumb;
	}
	// increase the level
	(0, _levelsModule._changeCurrentLevelValue)(true);
	// hide the drag knob/paths and the close button
	hideDragElements();
	// the first level is refs or the current level is the third
	// show the assets menu for the selected item
	if (_levelsModule.currentFirstLevelSelection === "references" || _levelsModule.currentLevel > 2) {
		// update the reference object
		(0, _dataModule.updateRefObject)(_textModule.currentTextSource[currentLevelElement]);
		// if the current element doesn't have assets, don't create the menu
		// and show an alert
		if (_textModule.currentTextSource[currentLevelElement].assets.length === 0) {
			(0, _levelsModule._changeCurrentLevelValue)(false);
			return showDragElements();
		}
		// toggle the assets visible bool
		(0, _assetsMenuModule._resetAssetVisible)(true);
		// hide the pagination controls
		(0, _paginationModule.showPaginationControls)();
		// place the drag elements and close button
		return placeKnob(true);
	}

	/* Set the items for the level depending on the first level selection.
  * For costumers, get the customer id and get the customer's references.
  * For highlight get the index of the current level, create a new array for the
  * references with the selection as the first element (which will be removed)
 */
	/* If the selection is highlight, create a new references array
  * Since the first element in the next level will be the same reference
  * there is no need to update the reference object
 */
	if (_levelsModule.currentFirstLevelSelection === "highlight") {
		(0, _dataModule.createNewRefs)();
	}
	// if the selection is customer set the selected customer
	if (_levelsModule.currentFirstLevelSelection === "customer") {
		// set the selected customer id to the selected element
		(0, _dataModule._setSelectedCustomer)(_textModule.currentTextSource[currentLevelElement].id);
		/* Update the reference object, using the first ref from the 
   * references of the currently selected customer. All the customers
   * references are placed in an object. Get the first array element
   * of the selected customer in the object
  */
		(0, _dataModule.updateRefObject)(_dataModule.customersReferences[_dataModule.selectedCustomer][0]);
	}

	// update the text source
	(0, _textModule.setTextSource)();
	// before creating the segments set the pagination data
	// in case the current selection has more than 40 elements
	(0, _paginationModule.setPaginationData)();
	// create the segments of the current level
	(0, _levelsModule.createArcSegments)();
	// place the drag elements and close button
	placeKnob();
};

/** Method to init the knob module.
 * Sets the initial state of the knob and creates the GSAP instance
 * to show/hide the drag elements and the close button
 * @private
*/
var _initKnobModule = exports._initKnobModule = function _initKnobModule() {
	// create the DOM elements vars
	// draggable knob
	draggableKnob = document.getElementById("draggable-knob");
	// drag path container
	dragPathContainer = document.getElementById("drag-path-container");
	// drag path track, this is always visible
	dragPathTrack = document.getElementById("drag-path-track");
	// drag path reveal, this is revealed as the user drags the knob
	dragPathReveal = document.getElementById("drag-path-reveal");
	// the path reveal tween
	pathRevealTween = TweenLite.from(dragPathReveal, 1, {
		drawSVG: "0%", ease: Linear.easeNone, paused: true
	});
	// by default the drag path container and the knob are hidden
	TweenLite.set([draggableKnob, dragPathContainer, _buttonsModule.closeButton], {
		autoAlpha: 0
	});
	// create the instance to show the elements
	showDragTween = TweenLite.to([draggableKnob, dragPathContainer, _buttonsModule.closeButton], 0.2, {
		autoAlpha: 1, paused: true
	});
};

/***/ }),

/***/ "./src/image-module.js":
/*!*****************************!*\
  !*** ./src/image-module.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._initImageModule = exports.updateImageSrc = exports.selectedLevelImages = exports.isImageVisible = exports.refImageTween = undefined;

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _dataModule = __webpack_require__(/*! ./data-module */ "./src/data-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

// the image wrapper

// data module
/*
*******************************************************************************************
		IMAGE MODULE
*******************************************************************************************
*/
// levels module
var imageWrapper = void 0;
// the actual image, the src attribute will be updated

// draggable knob module

// text module
var referenceImage = void 0;

/* The GSAP instance that shows/hide the image wrapper
 * When the user selects a reference and the image wrapper is not visible
 * we show it after updating the src attribute.
*/
var refImageTween = exports.refImageTween = void 0;

/* Bool to indicate if the image wrapper is visible or not.
 * In some cases the image wrapper could be visible and there's
 * no need to play the image tween, after selecting a reference,
 * just update the image src attribute.
 * False by default.
*/
var isImageVisible = exports.isImageVisible = false;

/* Object to store the image url of each level.
 * The user can select highlight for the first level and then an image
 * will be visible, but if selects a different reference for the next level
 * and goes to the assets menu of that reference, we need to update the 
 * image according to the selected refs when the user goes back to a previous
 * level.
*/
var selectedLevelImages = exports.selectedLevelImages = {
  1: "",
  2: ""
};

/** Method to update the Image SRC
 * Changes the src attribute of the image based on the thumb
 * for the current reference.
 * @param {string} src the image source
 * @private
*/
var updateImageSrc = exports.updateImageSrc = function updateImageSrc(src) {
  // console.log( "-------------------------\nupdate image src" );
  // console.log( "level => ", currentLevel );
  referenceImage.setAttribute("src", src);
};

/** Method to Init the Module
 * Creates the reference of the DOM element in the variable and
 * the GSAP instance to show/hide the element.
 * @private
*/
var _initImageModule = exports._initImageModule = function _initImageModule() {
  imageWrapper = document.getElementById("image-wrapper");
  referenceImage = document.getElementById("reference-image");
  exports.refImageTween = refImageTween = TweenLite.to(imageWrapper, 0.2, {
    autoAlpha: 1, paused: true
  });
};

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initSVGMenu = undefined;

var _dataModule = __webpack_require__(/*! ./data-module */ "./src/data-module.js");

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

var _buttonsModule = __webpack_require__(/*! ./buttons-module */ "./src/buttons-module.js");

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

var _imageModule = __webpack_require__(/*! ./image-module */ "./src/image-module.js");

var _resetModule = __webpack_require__(/*! ./reset-module */ "./src/reset-module.js");

var _paginationModule = __webpack_require__(/*! ./pagination-module */ "./src/pagination-module.js");

/** General Init Method
 * Starts the app.
 * @param {object} data the data location in the global window object
 * @private
*/

// reset modult

// assets menu init method

// buttons init method

// get the levels init method
var _initSVGMenu = exports._initSVGMenu = function _initSVGMenu(data) {
	// get the data
	// _fetchServerData("js/sample-data.json");
	(0, _dataModule._fetchServerData)(data);
	// init the assets menu
	(0, _assetsMenuModule._initAssetsMenu)();
	// init the text module
	(0, _textModule._initTextModule)();
	// init the image module
	(0, _imageModule._initImageModule)();
	// init the levels
	(0, _levelsModule._initLevelsModule)();
	// init the buttons module
	(0, _buttonsModule._initBtnModule)();
	// init the draggable knob
	(0, _draggableKnobModule._initKnobModule)();
	// init pagination module
	(0, _paginationModule._initPagination)();
};
// pagination module

// image module init method

// text init method

// get the method to init the knob module
// get the data module


_initSVGMenu(window.SVGMenuData);

// create the property in the global object
window.SVGRingMenu = { _initSVGMenu: _initSVGMenu, resetSVGMenu: _resetModule.resetSVGMenu };

/***/ }),

/***/ "./src/levels-module.js":
/*!******************************!*\
  !*** ./src/levels-module.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initLevelsModule = exports.reverseLevelsTweens = exports.currentFirstLevelSelection = exports.createArcSegments = exports.menuTweensArray = exports.menuLevelsContainers = exports.levelArcContainers = exports._changeCurrentLevelValue = exports.currentLevel = exports.clearLevelPathsArray = exports.levelPathsArrays = exports.defaultLevelElementColor = exports._setCurrentLevelSegmentsAmount = exports.currentLevelSegmentsAmount = undefined;

var _buttonsModule = __webpack_require__(/*! ./buttons-module */ "./src/buttons-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

var _imageModule = __webpack_require__(/*! ./image-module */ "./src/image-module.js");

var _dataModule = __webpack_require__(/*! ./data-module */ "./src/data-module.js");

var _paginationModule = __webpack_require__(/*! ./pagination-module */ "./src/pagination-module.js");

// the segments of the current level. Used to update the color as
// the user drags the knob along the path.

// data module

// assets module

// draggable module
var currentLevelSegmentsAmount = exports.currentLevelSegmentsAmount = 0;

/** Method to set the current level segments.
 * @private
*/

// pagination module

// image module

// text module
/*
*******************************************************************************************
		LEVELS MODULE
*******************************************************************************************
*/
// buttons
var _setCurrentLevelSegmentsAmount = exports._setCurrentLevelSegmentsAmount = function _setCurrentLevelSegmentsAmount() {
	return exports.currentLevelSegmentsAmount = currentLevelSegmentsAmount = getLevelSegments();
};

// the default color of the segments of each level
var defaultLevelElementColor = exports.defaultLevelElementColor = "rgba(255,255,255,0.8)";

/* array with all the paths for each level each level has it's own
 * array with the paths. When the close button is clicked, the current
 * level is emptied before reducing the level index.
 * this works one level behind the current level, so the array for the
 * first level has an index 0.
*/
var levelPathsArrays = exports.levelPathsArrays = [[], [], []];

/** Method to clean the current level's paths array
 * @param {bool} all if all the levels should be cleared
 * @private
*/
var clearLevelPathsArray = exports.clearLevelPathsArray = function clearLevelPathsArray(all) {
	if (all) {
		exports.levelPathsArrays = levelPathsArrays = [[], [], []];
		return;
	}
	// get the array for the current level
	levelPathsArrays[currentLevel] = [];
};

// the current menu level
var currentLevel = exports.currentLevel = 0;

/** Method to update the current level value
 * @param {boolean} increase if the value should increase or not
 * @private
*/
var _changeCurrentLevelValue = exports._changeCurrentLevelValue = function _changeCurrentLevelValue(increase) {
	return increase ? (exports.currentLevel = currentLevel += 1, currentLevel - 1) : (exports.currentLevel = currentLevel -= 1, currentLevel + 1);
};

/* The app structure is circular and we need the radius
 * of each level in order to calculate and position the elements
 * of each level on those cirular patterns and draw the rest of 
 * the guide circle 
*/
var levelRadius = [null, 150, 220, 290];
/* Each level of the app is positioned in different starting and
 * ending angles. 
*/
var levelAngles = [
// level 0 is always visible
null,
// second level
{
	small: {
		segments: [-58, 116], bottom: [61, 300]
	},
	big: {
		segments: [-118, 236], bottom: [121, 240]
	}
},
// third level
{
	small: {
		segments: [212, -32], bottom: [-29, 210]
	},
	big: {
		segments: [-208, 28], bottom: [31, 150]
	}
},
// fourth level
{
	small: {
		segments: [32, 148], bottom: [-209, 30]
	},
	big: {
		segments: [-28, 208], bottom: [-149, -30]
	}
}];
/* The containers for each level's segments
 * These are the DOM elements (<g> tags) that will hold the segments.
 * The containers are cleared before adding the new elements and
 * showing them.
*/
var secondLevelSegments = void 0,
    secondLevelBottom = void 0,
    thirdLevelSegments = void 0,
    thirdLevelBottom = void 0,
    fourthLevelSegments = void 0,
    fourthLevelBottom = void 0;
//			
var levelArcContainers = exports.levelArcContainers = void 0;
/* Level menu containers
 * This have the segments and bottom circles. This are the ones that
 * will be animated in and out (zoom and fade)
*/
var secondLevelMenu = void 0,
    thirdLevelMenu = void 0;

// the menu containers for each level, this will hold the segments and
// the close bottom arc
var menuLevelsContainers = exports.menuLevelsContainers = void 0;

/** Method to create the animations of each menu level.
 * Creates the scale and fade animation for each level that's played
 * after creating and adding the segments and close arc for the level.
 * Also creates the animation of the level's text wrap, in order to
 * animate all the level's elements at the same time.
 * @param {number} index the target element
 * @returns {object} the GSAP Timeline instance 
 * @private
*/
var createMenuTween = function createMenuTween(index) {
	// the first level menu is always visible, stop code
	if (index === 0) return;
	// create the timeline instance
	// when the timeline is complete, position the drag knob
	// the method to position the knob will show the drag path and knob
	var tl = new TimelineLite({
		paused: true,
		onComplete: _draggableKnobModule.placeKnob,
		onReverseComplete: function onReverseComplete() {
			// check if the instance was reversed because of the close button or
			// because a first level selector. If the close button was clicked
			// then call the method to position the drag elements and then show them
			if (_buttonsModule.isCloseClicked && currentLevel > 0) (0, _draggableKnobModule.placeKnob)();
		}
	});
	// always get the level container
	var levelContainer = menuLevelsContainers[index];
	/* The second and third level have menu elements and text, while
  * the foruth level has just menu elements. Create a timeline only
  * with the menu elements for the fourth level (index = 3) and a 
  * timeline with the menu elements and text for second and third.
 */
	if (index === 3) {
		tl.to(levelContainer, 0.2, {
			autoAlpha: 1, scale: 1, transformOrigin: "center", paused: true
		});
		// tl
	} else if (index === 1 || index === 2) {
		// the level is 1 or 2, get the text wrap
		var textWrap = index === 1 ? _textModule.secondLevelTextWrap : _textModule.thirdLevelTextWrap;
		tl.to(levelContainer, 0.2, {
			autoAlpha: 1, scale: 1, transformOrigin: "center"
		}).to(textWrap, 0.2, {
			autoAlpha: 1
		}, 0.1);
		// tl
	}
	// add the timeline to the menu tweens array
	menuTweensArray.push(tl);
	// return the timeline
	return tl;
};
// an array to contain the menu levels animations
// this allows to play and reverse the level animations
// when needed
var menuTweensArray = exports.menuTweensArray = [null];

/** Method to create the arc string.
	 * Takes the circle center, radius and angles, and returns an arc
	 * string that is used in the <path> tag as the "d" attribute.
	 * @param {number} centerX the x coordinate of the circle center
	 * @param {number} centerY the y coordinate of the circle center
	 * @param {number} radius the circle radius
	 * @param {number} startAngle the angle in degs where the arc starts
	 * @param {number} endAngle the angle in degs where the arc ends
	 * @returns {string} the arc string
	 * @private
	*/
var createArcString = function createArcString(centerX, centerY, radius, startAngle, endAngle) {
	// the angle should be in rad
	var correctedAngleStart = (startAngle - 90) * Math.PI / 180;
	var correctedAngleEnd = (endAngle - 90) * Math.PI / 180;
	// first get the points
	var startX = centerX + radius * Math.cos(correctedAngleEnd);
	var endX = centerX + radius * Math.cos(correctedAngleStart);
	var startY = centerY + radius * Math.sin(correctedAngleEnd);
	var endY = centerY + radius * Math.sin(correctedAngleStart);
	// the arc sweep
	var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

	// set the arc string
	var arcString = "M " + startX + " " + startY + " A " + radius + " " + radius + " 0 " + arcSweep + " 0 " + endX + " " + endY;

	return arcString;
};

/** Method to create the Path tag
 * Creates the path tag with the corresponding d attribute for the arc
 * string
 * @param {number} centerX the x coordinate of the circle center
 * @param {number} centerY the y coordinate of the circle center
 * @param {number} radius the circle radius
 * @param {number} startAngle the angle in degs where the arc starts
 * @param {number} endAngle the angle in degs where the arc ends
 * @returns {object} the path tag
 * @private
*/
var createArcPath = function createArcPath(centerX, centerY, radius, startAngle, endAngle, close) {
	// set the stroke width
	var strokeWidth = close ? " 1;" : " 10;";
	// create the path element
	var newPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
	newPath.setAttribute("style", "fill: none; stroke: rgba(255,255,255,0.8); stroke-width:" + strokeWidth);
	newPath.setAttribute("d", createArcString(centerX, centerY, radius, startAngle, endAngle));
	newPath.setAttribute("class", "segment-el");
	// add the path to the container
	// acrContainer.appendChild(newPath);
	// add the paths to the array
	close ? null : levelPathsArrays[currentLevel].push(newPath);
	return newPath;
};

/** Method to set the level segements.
 * Depending on the level and the first level selection, returns the amount
 * of segments for the current level and selection.
 * @returns {number} the number of segements
 * @private
*/
var getLevelSegments = function getLevelSegments() {
	if (currentLevel === 1) {
		// get the segments of the corresponding selection
		return _dataModule.firstLevel[currentFirstLevelSelection];
	} else if (currentLevel === 2 && currentFirstLevelSelection === "customer") {
		// the first level is customer, get the selected customer and it's refs
		return _dataModule.customersReferences[_dataModule.selectedCustomer].length;
	} else if (currentLevel === 2 && currentFirstLevelSelection === "highlight") {
		// the first level is highlight, get the refs amount
		// return referencesCount; 
		return _dataModule.newHighlightRefsCount;
	}
};

/** Method to create the segments.
 * Gets the data of the current level and loops through the elements
 * and creates a <path> tag for each one and adds it to the parent
 * target.
 * In this method check the pagination data to set the number of elements
 * to show. Basically update the level segments based on the pagination
 * data, that is the group number, group amount and total elements.
 * If the method is called from a pagination change, don't run the get
 * level segements method, just 
 * @param {boolean} paginate if the method is called because a pagination
 * 				change or not
 * @private
*/
var createArcSegments = exports.createArcSegments = function createArcSegments(paginate) {
	// console.log( "---------------\ncreate arcs" );
	// console.log( paginationData[currentLevel] );
	// console.log( "segments >", getSegmentsAmount() );
	// set the level segments
	var levelSegments = exports.currentLevelSegmentsAmount = currentLevelSegmentsAmount = (0, _paginationModule.getSegmentsAmount)();
	// if the method is being executed from a pagination method
	// reset the current level paths array
	paginate ? clearLevelPathsArray() : null;
	// use the current level to get the angles and radius for the segments
	// and close arc, depending on the amount of segments
	var currLevelRadius = levelRadius[currentLevel];
	var currLevelAngles = levelSegments > 20 ? levelAngles[currentLevel].big : levelAngles[currentLevel].small;
	// depending on the segments amount the angles to be used
	var segmentAngles = currLevelAngles.segments;
	// the total angle for each segment
	var arcAngle = ((levelSegments > 20 ? 236 : 116) - (levelSegments - 2)) / levelSegments;
	// the parent element for each segment
	var segmentsParent = levelArcContainers[currentLevel].segments;
	// remove all the segments from the parent element
	segmentsParent.innerHTML = "";
	// with the amount of segments and the angles create the segments
	// and add them to the parent element
	for (var i = 0; i < levelSegments; i++) {
		var currentAngle = segmentAngles[0] + i * arcAngle + i;
		var newArc = createArcPath(512, 384, currLevelRadius, currentAngle, currentAngle + arcAngle);
		segmentsParent.appendChild(newArc);
	} // loop
	// then create the closing arc for the current level and add it to the container
	var closeArc = createArcPath(512, 384, currLevelRadius, currLevelAngles.bottom[0], currLevelAngles.bottom[1], true);
	var closeArcContainer = levelArcContainers[currentLevel].close;
	// add the bottom arc to the container
	closeArcContainer.innerHTML = "";
	closeArcContainer.appendChild(closeArc);
	/* reset the current level element before applying the text
  * of the fist element of the new level.
  * Run the method to get the index value of the new group if any.
 */
	(0, _draggableKnobModule._resetCurrentLevelElement)((0, _paginationModule.getNewGroupFirstIndex)());
	// console.log( "----------------------\ncreate arcs" );
	// console.log( "current level element", currentLevelElement );
	/* If the method was executed from a pagination instance then after
  * adding the new group of segments, play the tween to show the segments
  * wrapper.
 */
	if (paginate) {
		(0, _paginationModule.showHideSegmentsWrapper)(true);
	}
	// now apply the text to the container
	(0, _textModule.applyElementText)(false, (0, _paginationModule.getNewGroupFirstIndex)());
	// update the image source
	(0, _imageModule.updateImageSrc)(_textModule.currentTextSource[_draggableKnobModule.currentLevelElement].thumb);
	// by default the first element of the current level should be white
	TweenLite.set(levelPathsArrays[currentLevel][0], {
		stroke: "#fff", className: "+=selected-segment"
	});
	// animate the entire level menu
	menuTweensArray[currentLevel].play();
	// if the current level is 1 and the selection is not customer show the image wrappper
	if (currentLevel === 1 && currentFirstLevelSelection !== "customer") {
		_imageModule.refImageTween.play();
	}
	// if the current level is 2 and the selection is customer show the image wrapper
	if (currentLevel > 1 && currentFirstLevelSelection === "customer") {
		_imageModule.refImageTween.play();
	}
}; // create arc segments

/*
	*******************************************************************************************
			FIRST LEVEL CLICK AND ANIMATION
	*******************************************************************************************
	*/
// the angles for each rotation
var firstLevelAngles = {
	highlight: 0,
	references: -120,
	customer: 120
};
// the current selected element
var currentFirstLevelSelection = exports.currentFirstLevelSelection = "";
// the previous first level selected element
var previousFirstLevelSelection = "#highlight";
// the first level main wrap
var firstLevelWrap = void 0;
// the elements of the first level
var firstLevelElements = void 0;
/** Method to create the rotation animation.
 * Since we might need to reverse other animations before starting the
 * first level rotation, we use this method to create and play the animation.
 * @param {number} angle the angle of rotation
 * @private
*/
var createRotationTween = function createRotationTween(angle) {
	// set the current level to 1
	exports.currentLevel = currentLevel = 1;
	// set the current level source
	(0, _textModule.setTextSource)();
	// set the pagination data
	(0, _paginationModule.setPaginationData)();
	// add/remove the first level selected class
	TweenLite.set("#" + currentFirstLevelSelection, { className: "+=selected" });
	TweenLite.set(previousFirstLevelSelection, { className: "-=selected" });
	// set the new target as the previous
	previousFirstLevelSelection = "#" + currentFirstLevelSelection;
	// create the rotation tween applyElementText
	TweenLite.to(firstLevelWrap, 0.2, {
		rotation: angle + "_short", transformOrigin: "50% 54%",
		onComplete: createArcSegments
		// onComplete: applyElementText
	});
};

/** Method to reverse the levels animations.
 * Checks the current level and reverses the animations of all the
 * visible levels.
*/
var reverseLevelsTweens = exports.reverseLevelsTweens = function reverseLevelsTweens(angle, close) {
	// if the close button was clicked, then we don't have to create the rotation tweens
	if (close) {
		// empty the paths array of the current closed level
		clearLevelPathsArray();
		// hide the drag elements
		(0, _draggableKnobModule.hideDragElements)();
		// reverse the menu GSAP instance
		menuTweensArray[currentLevel].reverse();
		return;
	}
	// empty all the paths arrays
	clearLevelPathsArray(true);
	// the close button wasn't clicked, but a first level selector
	// reverse all the levels instances
	menuTweensArray.forEach(function (e) {
		return e ? e.reverse() : null;
	});
	// then create the rotation tween for the selected element
	TweenLite.delayedCall(0.35, createRotationTween, [angle]);
};

/** Method to init the levels module
 * Creates the animations for each menu level and also attaches the
 * click event to every first level element.
 * Sets the initial state of the 
 * @private
*/
var _initLevelsModule = exports._initLevelsModule = function _initLevelsModule() {
	// create the module variables for DOM elements
	// level segments containers
	secondLevelSegments = document.getElementById("second-level-segments");
	secondLevelBottom = document.getElementById("second-level-bottom");
	thirdLevelSegments = document.getElementById("third-level-segments");
	thirdLevelBottom = document.getElementById("third-level-bottom");
	fourthLevelSegments = document.getElementById("fourth-level-segments");
	fourthLevelBottom = document.getElementById("fourth-level-bottom");
	// level menu containers
	secondLevelMenu = document.getElementById("second-level-menu");
	thirdLevelMenu = document.getElementById("third-level-menu");
	// first level menu wrap
	firstLevelWrap = document.getElementById("first-level-wrap");
	// first level elements
	firstLevelElements = document.querySelectorAll(".first-level-element");
	// the menu level containers
	exports.menuLevelsContainers = menuLevelsContainers = [null, secondLevelMenu, thirdLevelMenu, _assetsMenuModule.fourthLevelMenu];
	// level arc containers
	exports.levelArcContainers = levelArcContainers = [null, { segments: secondLevelSegments, close: secondLevelBottom }, { segments: thirdLevelSegments, close: thirdLevelBottom }, { segments: fourthLevelSegments, close: fourthLevelBottom }];

	// loop through the first level elements and add the click events
	firstLevelElements.forEach(function (e) {
		var targetEl = e.getAttribute("data-target");
		/* When the app first runs, check if the amount of elements of each 
   * type is 0, in that case add a class to the element in order to 
   * create a visual aid of the element being disabled
  */
		if (_dataModule.firstLevel[targetEl] === 0) {
			TweenLite.set(e, { className: "+=disabled" });
		}
		e.onclick = function () {
			// get the target element
			// if the target doesn't have any elements, don't execute the code
			// and send an alert to the user
			if (_dataModule.firstLevel[targetEl] === 0) return; // alert("The selected target is empty!!");
			// if the target is not the selected one then animate
			// and if the current first level is not empty
			if (targetEl !== currentFirstLevelSelection && currentFirstLevelSelection !== "") {
				exports.currentFirstLevelSelection = currentFirstLevelSelection = targetEl;
				// hide the drag elements
				(0, _draggableKnobModule.hideDragElements)();
				// hide the assets menu
				_assetsMenuModule.assetsMenuTween.reverse();
				// set the asset menu visible bool to false
				(0, _assetsMenuModule._resetAssetVisible)(false);
				// hide the image wrapper
				_imageModule.refImageTween.reverse();
				// hide the pagination controls
				(0, _paginationModule.hidePaginationControls)();
				/* First we check if the current level is more than 0.
     * If the level is more than 0, then we reverse all the animations
     * of the current visible levels.
     * After reversing the animations, we should remove all the segments and
     * close arcs from their containers.
     * Then we run the rest of the code to create the new level.
    */
				if (currentLevel > 0) return reverseLevelsTweens(firstLevelAngles[targetEl]);
				// create the rotation animation
				createRotationTween(firstLevelAngles[targetEl]);
			} // selected target conditional
			// if the current level is empty and the target is highlight
			// then just create the arc segments
			if (currentFirstLevelSelection === "") {
				// set the current level to 1
				exports.currentLevel = currentLevel = 1;
				exports.currentFirstLevelSelection = currentFirstLevelSelection = targetEl;
				// set the current level source
				(0, _textModule.setTextSource)();
				// set the pagination data
				(0, _paginationModule.setPaginationData)();
				if (targetEl == "highlight") return createArcSegments();
				createRotationTween(firstLevelAngles[targetEl]);
			}
			if (currentLevel === 0 && currentFirstLevelSelection === targetEl) {
				exports.currentLevel = currentLevel = 1;
				exports.currentFirstLevelSelection = currentFirstLevelSelection = targetEl;
				// set the current level source
				(0, _textModule.setTextSource)();
				// set the pagination data
				(0, _paginationModule.setPaginationData)();
				createArcSegments();
			}
		}; // click event
	}); // for each loop

	/* Set the initial state of the level containers to animate them when that
  * element of the menu is selected.
  * After the animation is complete, then we add the text of the first element
  * of the level to the text component. Then we animate the 
 */
	// initial state of the text wraps
	TweenLite.set([_textModule.secondLevelTextWrap, _textModule.thirdLevelTextWrap], {
		autoAlpha: 0
	});
	// initial state of the menu containers
	TweenLite.set([secondLevelMenu, thirdLevelMenu], {
		autoAlpha: 0, scale: 0.01, transformOrigin: "center", x: 507, y: 379
	});

	TweenLite.set(_assetsMenuModule.fourthLevelMenu, {
		autoAlpha: 0
	});

	for (var i = 1; i < 3; i++) {
		createMenuTween(i);
	}
};

/***/ }),

/***/ "./src/pagination-module.js":
/*!**********************************!*\
  !*** ./src/pagination-module.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initPagination = exports.showPaginationControls = exports.hidePaginationControls = exports.getNewGroupFirstIndex = exports.getSegmentsAmount = exports.showHideSegmentsWrapper = exports.updatePaginationGroup = exports.setPaginationData = exports.resetPaginationData = exports.paginationData = exports.resetSegmentWrappersTween = exports.prevGroupBtn = exports.nextGroupBtn = exports.nextGroup = exports.previousGroup = undefined;

var _textModule = __webpack_require__(/*! ./text-module */ "./src/text-module.js");

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

// pagination buttons

// levels module
var previousGroup = exports.previousGroup = void 0,
    nextGroup = exports.nextGroup = void 0,
    nextGroupBtn = exports.nextGroupBtn = void 0,
    prevGroupBtn = exports.prevGroupBtn = void 0;

/** Method to reset the fade animations.
 * This method kills the GSAP instances of the segments wrappers
 * for level 1 and 2 and clear the props of the target elements.
 * @private
*/

// assets module
/*
*******************************************************************************************
		PAGINATION MODULE
*******************************************************************************************
*/
// text module
var resetSegmentWrappersTween = exports.resetSegmentWrappersTween = function resetSegmentWrappersTween() {
	TweenLite.set([_levelsModule.menuLevelsContainers[1], _levelsModule.menuLevelsContainers[2]], {
		clearProps: "opacity"
	});
};

/* Pagination Data Object
 * Has the data for the amount of groups for each menu level.
 * When it comes to pagination the levels affected by pagination are
 * level 1 and 2.
 * Has the current pagination group for each level.
 * The data should be updated when a new element is selected and the
 * current level is either 1 or 2, and when the user goes to a next or
 * previous group in the current selection.
 * Level Containers Animations.
 * GSAP instances to fade in/out level 1 and 2 segment containers.
 * By default the segments wrappers are visible, so the animation
 * should hide the elements.
 * This data is checked before updating the segments in the current level.
 * To calculate the amount of segments for the level check if the group amount
 * is bigger from the current group. Then if the current group is less than the
 * amount, use 40. If it's equal use the final amount instead.
*/
var paginationData = exports.paginationData = {
	// level 1
	1: {
		groupAmount: 0,
		currentGroup: 1,
		finalAmount: 0,
		controlsPos: [332, 691] // this are the x coords, the y coord is 384
	},
	// level 2
	2: {
		groupAmount: 0,
		currentGroup: 1,
		finalAmount: 0,
		controlsPos: [604, 164] // this are the y coords, the x coord is 512
	}
};

/** Method to reset the pagination data.
 * Sets the values of the pagination data to it default.
 * @private
*/
var resetPaginationData = exports.resetPaginationData = function resetPaginationData() {
	var lev1 = paginationData[1],
	    lev2 = paginationData[2];

	lev1.groupAmount = 0;
	lev1.currentGroup = 1;
	lev1.finalAmount = 0;
	lev2.groupAmount = 0;
	lev2.currentGroup = 1;
	lev2.finalAmount = 0;
	// also hide the controls
	showPaginationControls(true);
};

/** Method to set the pagination data
 * Sets the group amount and current group of the current level
 * based on the current text source and level.
 * When creating the pagination data we check if there group amount
 * for the current selection is more than one, place and show the
 * pagination controls, if the group amount is 1 don't show the
 * pagination controls.
 * @param {boolean} close if the pagination is set from the close
 * 				button handler
 * @private
*/
var setPaginationData = exports.setPaginationData = function setPaginationData(close) {
	/* If the method is executed from the close button handler,
  * check if the current level is 0. If it's 0 then reset the
  * pagination data to it defaults values. Also if the level is 
  * 0 hide the pagination buttons
  */
	if (_levelsModule.currentLevel === 0) {
		showPaginationControls(close);
		return close ? resetPaginationData() : null;
	}
	// check the amount of elements and 
	var groupAmount = Math.ceil(_textModule.currentTextSource.length / 40);
	// the amount of items for the final group
	var finalGroupAmount = _textModule.currentTextSource.length % 40;
	// update the pagination data
	paginationData[_levelsModule.currentLevel].groupAmount = groupAmount;
	paginationData[_levelsModule.currentLevel].finalAmount = finalGroupAmount;
	// after setting the pagination data
	showPaginationControls();
};

/** Method to update the current group.
 * Changes the current group index value of the current selection.
 * If the user goes to a previous or next group, the data of the
 * current level is updated.
 * @param {boolean} next if true, go to the next level, false go to the previous
 * @private
*/
var updatePaginationGroup = exports.updatePaginationGroup = function updatePaginationGroup(next) {
	// if the current level is 0 don't run the code
	if (_levelsModule.currentLevel === 0 || _levelsModule.currentLevel === 3) return;
	var _paginationData$curre = paginationData[_levelsModule.currentLevel],
	    groupAmount = _paginationData$curre.groupAmount,
	    currentGroup = _paginationData$curre.currentGroup;
	// going to the next group, check if the current group is 
	// equal to the group amount

	if (next && currentGroup < groupAmount) {
		// the user is selected see the next group and the current group index
		// is less than the group amount, therefore there is another group to show
		// increase the current group index
		paginationData[_levelsModule.currentLevel].currentGroup = currentGroup + 1;
		// after updating the current group hide the current level segments wrapper
		showHideSegmentsWrapper(false);
	} else if (!next && currentGroup > 1) {
		// the user selected the previous group and the current group index
		// is more than 0, so there's a previous group to show decrease
		// the current group index
		paginationData[_levelsModule.currentLevel].currentGroup = currentGroup - 1;
		// after updating the current group hide the current level segments wrapper
		showHideSegmentsWrapper(false);
	}
};

/** Method to show/hide the segments wrappers
 * Show/hide the current level's segments wrapper.
 * Depending on the param value, the tween of the current level segments
 * wrapper will be played or reversed.
 * @param {boolean} show true: show, false: hide
*/
var showHideSegmentsWrapper = exports.showHideSegmentsWrapper = function showHideSegmentsWrapper(show) {
	// get the current segment wrapper tween based on the menu level
	if (show === false) {
		TweenLite.to(_levelsModule.menuLevelsContainers[_levelsModule.currentLevel], 0.2, {
			opacity: 0,
			onComplete: _levelsModule.createArcSegments, onCompleteParams: [true]
		});
	} else if (show === true) {
		TweenLite.to(_levelsModule.menuLevelsContainers[_levelsModule.currentLevel], 0.2, {
			opacity: 1
		});
		// we're showing the segments wrapper from the levels module
		// after creating the segments for a new pagination group
		// so we show/hide the controls
		showPaginationControls();
	}
	// after running the tween, create the segments for the new group
};

/** Method to get the segments amount.
 * Uses the pagination data and the current level, to set the amount of
 * segments for the current level.
 * This method should be called after updating the pagination data
 * @private
*/
var getSegmentsAmount = exports.getSegmentsAmount = function getSegmentsAmount() {
	// the target
	var _paginationData$curre2 = paginationData[_levelsModule.currentLevel],
	    groupAmount = _paginationData$curre2.groupAmount,
	    currentGroup = _paginationData$curre2.currentGroup,
	    finalAmount = _paginationData$curre2.finalAmount;
	// if the current group is less than the group amount the number of
	// segments is 40. If the current group is equal to the group amount
	// the number of segments is the final amount

	if (currentGroup < groupAmount) {
		return 40;
	} else if (currentGroup === groupAmount) {
		return finalAmount;
	}
};

/** Method to set the target index of the next/prev group.
 * When a new group is requested, the create segments method in the 
 * levels module will reset the current level element index, normally
 * the set value is 0 for a new level, but for a new group we need to
 * the index value of the first element in the target group in the data
 * array, that has all the elements of all the groups.
 * THIS METHOD SHOULD BE CALLED AFTER THE PAGINATION DATA HAS BEEN UPDATED!!
 * @returns {number} the index value of the first element of the new group
 * @returns {null} if the groups amount is 1 return null
 * @private
*/
var getNewGroupFirstIndex = exports.getNewGroupFirstIndex = function getNewGroupFirstIndex() {
	// get the current group and group amount
	var _paginationData$curre3 = paginationData[_levelsModule.currentLevel],
	    groupAmount = _paginationData$curre3.groupAmount,
	    currentGroup = _paginationData$curre3.currentGroup;
	// console.log( "------------------------\nget new group index", groupAmount, currentGroup );
	// check if there are two or more groups

	if (groupAmount > 1) {
		/* The index of the first element of the new group is the limit (40)
   * multiplied by the current group minus 1. If there are 3 groups and
   * the current group is 2 then the index is 40, so the app will display
   * the data for the 40th element in the array.
  */
		// console.log( "new group index", 40 * ( currentGroup - 1 ) );
		return 40 * (currentGroup - 1);
	}
	// if the group amount is 1, means there's no pagination for this level
	// and selection, in that case return null
	if (groupAmount === 1) return null;

	return null;
};

/** Method to Hide the pagination controls.
 * This method hides both buttons.
 * @private
*/
var hidePaginationControls = exports.hidePaginationControls = function hidePaginationControls() {
	TweenLite.to([nextGroupBtn, prevGroupBtn], 0.1, { autoAlpha: 0 });
};

/** Method to Show/Hide the pagination buttons.
 * Shows or hides the pagination buttons depending on the current
 * group and the groups amount.
 * Also places the pagination controls depending on the level.
 * @param {boolean} close if the method is called from the close handler
 * @private
*/
var showPaginationControls = exports.showPaginationControls = function showPaginationControls(close) {
	// if the method is called from the close handler and is the first level
	// hide the controls
	if (_assetsMenuModule.isAssetVisible || close && _levelsModule.currentLevel === 0) {
		return hidePaginationControls();
	}
	var _paginationData$curre4 = paginationData[_levelsModule.currentLevel],
	    groupAmount = _paginationData$curre4.groupAmount,
	    currentGroup = _paginationData$curre4.currentGroup;
	// if the current selection has only one group hide all controls

	if (groupAmount === 1) {
		return hidePaginationControls();
	}
	// depending on the group amount and the current group we show/hide
	// the different buttons
	// we're on the first group, show the next button and hide the prev
	if (currentGroup === 1) {
		TweenLite.to(prevGroupBtn, 0.1, { autoAlpha: 0 });
		TweenLite.to(nextGroupBtn, 0.1, { autoAlpha: 1 });
	}
	// the current group is not 1 and less than the group amount
	// show both buttons
	if (currentGroup > 1 && currentGroup < groupAmount) {
		TweenLite.to([nextGroupBtn, prevGroupBtn], 0.1, { autoAlpha: 1 });
	}
	// the current group is the last one, show the prev button and
	// hide the next button
	if (currentGroup === groupAmount) {
		TweenLite.to(nextGroupBtn, 0.1, { autoAlpha: 0 });
		TweenLite.to(prevGroupBtn, 0.1, { autoAlpha: 1 });
	}
};

/** Pagination module init
 * Starts the pagination module
 * @private
*/
var _initPagination = exports._initPagination = function _initPagination() {
	// previousGroup = document.getElementById("previous");
	// nextGroup = document.getElementById("next");
	exports.nextGroupBtn = nextGroupBtn = document.getElementById("next-group-btn");
	exports.prevGroupBtn = prevGroupBtn = document.getElementById("prev-group-btn");
	// for this test we use a single position for the buttons
	// depends on feedback
	TweenLite.set([nextGroupBtn, prevGroupBtn], { transformOrigin: "center" });
	TweenLite.set(prevGroupBtn, { x: 400, y: 680 });
	TweenLite.set(nextGroupBtn, { x: 609, y: 680 });
	// attach click handlers
	// nextGroup.onclick = updatePaginationGroup.bind(null, true);
	// previousGroup.onclick = updatePaginationGroup.bind(null, false);
	nextGroupBtn.onclick = updatePaginationGroup.bind(null, true);
	prevGroupBtn.onclick = updatePaginationGroup.bind(null, false);
};

/***/ }),

/***/ "./src/reset-module.js":
/*!*****************************!*\
  !*** ./src/reset-module.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.resetSVGMenu = undefined;

var _assetsMenuModule = __webpack_require__(/*! ./assets-menu-module */ "./src/assets-menu-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

var _imageModule = __webpack_require__(/*! ./image-module */ "./src/image-module.js");

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _index = __webpack_require__(/*! ./index */ "./src/index.js");

/** Reset Menu Method
 * Resets the menu, setting it's state to the initial defaults
 * and updates the menu's data.
 * @param {object} data the new data to be applied to the menu
 * @public
*/

/* LEVELS MODULE */

/* DRAGGABLE KNOB */
var resetSVGMenu = exports.resetSVGMenu = function resetSVGMenu(data) {
	// assets menu
	_assetsMenuModule.assetsMenuTween.reverse();
	(0, _assetsMenuModule._resetAssetVisible)(false);
	// draggable knob
	(0, _draggableKnobModule.resetCurrentAssetsElement)();
	(0, _draggableKnobModule._resetCurrentLevelElement)();
	(0, _draggableKnobModule.hideDragElements)();
	// ref image
	_imageModule.refImageTween.reverse();
	// levels tween
	(0, _levelsModule.reverseLevelsTweens)(0, true);
	// finally restart the menu
	(0, _index._initSVGMenu)(data);
};
// get the main init menu

/* IMAGE MODULE */
/*
*******************************************************************************************
		RESET MODULE
*******************************************************************************************
*/
/* The reset module reverts all the menu state to it start default. For doing this,
 * the module needs to import all the app's reset and animation reverse
 * methods.
 * Reverses all animations.
 * Finally this module creates the menu reset method that then is added to the global
 * scope in the index file.
*/

/* ASSETS MENU */

/***/ }),

/***/ "./src/text-module.js":
/*!****************************!*\
  !*** ./src/text-module.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports._initTextModule = exports.applyElementText = exports.setTextSource = exports.currentTextSource = exports.thirdLevelText = exports.secondLevelText = exports.thirdLevelTextWrap = exports.secondLevelTextWrap = undefined;

var _levelsModule = __webpack_require__(/*! ./levels-module */ "./src/levels-module.js");

var _dataModule = __webpack_require__(/*! ./data-module */ "./src/data-module.js");

var _draggableKnobModule = __webpack_require__(/*! ./draggable-knob-module */ "./src/draggable-knob-module.js");

/* When a new level is selected the level's segments are visible.
	 * By defatul the first segmente is selected and it's text is displayed
	 * along the level's text guide path.
	*/
/* Level menu text containers
 * These wrap the text elements and will be animated with the menu
 * containers after a selection is made.
*/

// get the references and customers
var secondLevelTextWrap = exports.secondLevelTextWrap = void 0,
    thirdLevelTextWrap = exports.thirdLevelTextWrap = void 0,
    secondLevelText = exports.secondLevelText = void 0,
    thirdLevelText = exports.thirdLevelText = void 0;

/* Depending on the first level selection, the text source for the 
 * current level of the menu. We store the target data source in order
 * to prevent selecting it everytime the drag update selects a new
 * item of the current level
*/

// get the current level element
/*
*******************************************************************************************
		TEXT MODULE
*******************************************************************************************
*/
// get the levels module
var currentTextSource = exports.currentTextSource = null;

var logTextSource = function logTextSource() {
	// console.log( "---------------\nset text source" );
	// console.log( "current level => ", currentLevel );
	// console.log( currentTextSource );
};

/** Method to set the current text source.
 * Depending on the first level selection the text source.
 * Everytime a new first level selection is made the text source
 * is updated.
 * @private
*/
var setTextSource = exports.setTextSource = function setTextSource() {
	// for the second and third level the text source will be either
	// the modified highlight references or the selected customer
	// references
	if (_levelsModule.currentLevel > 1 && _levelsModule.currentFirstLevelSelection === "customer") {
		exports.currentTextSource = currentTextSource = _dataModule.customersReferences[_dataModule.selectedCustomer];
		return logTextSource();
	}
	if (_levelsModule.currentLevel > 1 && _levelsModule.currentFirstLevelSelection === "highlight") {
		exports.currentTextSource = currentTextSource = _dataModule.newHighlightRefs;
		return logTextSource();
	}
	switch (_levelsModule.currentFirstLevelSelection) {
		case "references":
			exports.currentTextSource = currentTextSource = _dataModule.appReferences;
			return logTextSource();
		case "highlight":
			exports.currentTextSource = currentTextSource = _dataModule.highlightRefs;
			return logTextSource();
		case "customer":
			exports.currentTextSource = currentTextSource = _dataModule.appCustomers;
			return logTextSource();
	} // switch
};

/** Method to update the current level's text
 * Gets the current level and the current level element or segmen
 * and applies the string from the data collection corresponding to 
 * the level and the element.
 * The index param is beacuse with the pagination system in place 
 * @param {boolean} close
 * @param {number} index the index value.
 * @private
*/
var applyElementText = exports.applyElementText = function applyElementText(close, target) {
	// first get the target text element depending on the level
	var targetTextContainer = void 0;
	if (_levelsModule.currentLevel === 1) {
		targetTextContainer = secondLevelText;
	} else if (_levelsModule.currentLevel === 2) {
		targetTextContainer = thirdLevelText;
	} // level conditional
	// clear the text of the target
	targetTextContainer.innerHTML = "";
	// get the text for the current element of the level
	// for the index value use either 0(when the method is executed from the close
	// handler) or the passed target (for a pagiantion )
	// const targetText = secondLevelDummyText[currentFirstLevelSelection];
	var targetText = currentTextSource[close ? 0 : target || _draggableKnobModule.currentLevelElement].name;
	// apply the text to the container
	targetTextContainer.innerHTML = targetText;
}; // apply element text

/** Method to Init the Text Module
 * Creates the variables after the DOM nodes are present
 * @private 
*/
var _initTextModule = exports._initTextModule = function _initTextModule() {
	exports.secondLevelTextWrap = secondLevelTextWrap = document.getElementById("second-level-text-wrap");
	exports.thirdLevelTextWrap = thirdLevelTextWrap = document.getElementById("third-level-text-wrap");
	exports.secondLevelText = secondLevelText = document.getElementById("second-level-text");
	exports.thirdLevelText = thirdLevelText = document.getElementById("third-level-text");
};

/***/ })

/******/ });
//# sourceMappingURL=app-ceacc73460bb3d260419.js.map