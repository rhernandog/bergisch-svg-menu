"use strict";
// http://sra-backend.dev2.bergisch-media.net/sets/6
(window => {
	window.SVGMenuData = {
		"id": 10,
		"name": "Felix Test 22",
		"description": null,
		"thumb": "public/reference_assets/thumb_9d862c6d4d9b91359fce0c3cc527f004.jpg",
		"is_template": 0,
		"asset_count": {
			"360": 0,
			"image": 15,
			"video": 4,
			"document": 0
		},
		"created_at": "2018-06-12 11:28:40",
		"updated_at": "2018-08-13 08:46:58",
		"deleted_at": null,
		"allCustomers": [
			{
				"id": 73,
				"name": "Pervouralsky Novotrubny Works OJSC",
				"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
				"websiteurl": "http://www.pntz.ru",
				"modifiedon": "2018-03-16 13:39:10",
				"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
				"landidname": "Russia",
				"konzernidname": null,
				"city": "Pervouralsk",
				"line1": "Torgovaya Street 1",
				"line2": null,
				"stateorprovince": "Sverdlovsk region",
				"parentaccountid": null,
				"parentaccountidname": null,
				"shortname": "PNTZ"
			}
		],
		"pivot": {
			"sync_list_id": 2,
			"reference_set_id": 10,
			"created_at": "2018-08-13 12:38:27",
			"updated_at": "2018-08-13 12:38:27"
		},
		"tags": [],
		"references": [
			{
				"id": 268,
				"customer_id": 73,
				"location_id": 76,
				"main_plant_id": 104,
				"thumb": "public/reference_assets/thumb_9d862c6d4d9b91359fce0c3cc527f004.jpg",
				"asset_count": {
					"360": 0,
					"image": 15,
					"video": 4,
					"document": 0
				},
				"type": "highlight",
				"name": "OCTG Finishing Center and Heat Treatment Line",
				"description": null,
				"modifiedon": null,
				"productgroup": null,
				"productgroupname": null,
				"annualcapacity": null,
				"highlight_description": "Pervouralsk is  home to Europe’s leading tube manufacturer, Pervouralsky Novotrubny Works, a subsidiary of the ChTPZ Group. The company produces over 25,000 tube types in a wide range of different dimensions. For their high-quality finished products for oil and gas exploration, the Russian tube specialists have had a complete finishing centre installed by SMS group. Depending on their intended purpose, the tubes can either pass through all the finishing lines, starting from the upsetting line through to the tubing/casing line in-line and without intermediate storage or each finishing line can be operated independently of the others.",
				"typeofprojectname": "New Plant / Equipment",
				"typeofproject": "1",
				"yearoforder": 2008,
				"yearofstartup": 2010,
				"anlageid": "1d9a645a-b931-e811-acd8-00155d351b2c",
				"anlagenstatusname": null,
				"anlagentypid": null,
				"anlagentypidname": null,
				"bemerkungen": null,
				"datummodernisierung": null,
				"hauptanlageid": null,
				"hauptanlageidname": null,
				"hauptlieferantidname": null,
				"ibndatum": null,
				"created_at": "2018-04-09 15:03:35",
				"updated_at": "2018-06-18 10:19:49",
				"latestSpecifications": [],
				"references": [
					{
						"id": 104,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "complex",
						"name": "OCTG Finishing Center",
						"description": null,
						"modifiedon": "2018-04-06 14:31:33",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Heat treatment lines",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": null,
						"hauptanlageidname": null,
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:48",
						"latestSpecifications": {
							"13": {
								"groupName": "Level 3 Automation",
								"specs": [
									{
										"name": "Manufacturing Execution System",
										"value": null,
										"valueMax": null,
										"unit": null,
										"abbreviation": "MES",
										"specification_type_id": 48
									}
								]
							},
							"100": {
								"groupName": "Plant Data - Mechanics",
								"specs": [
									{
										"name": "Additional Information",
										"value": "Finishing and Heat Treatment ",
										"valueMax": null,
										"unit": null,
										"abbreviation": null,
										"specification_type_id": 6
									},
									{
										"name": "Production Capacity",
										"value": "100000",
										"valueMax": null,
										"unit": "t/y",
										"abbreviation": "Capacity",
										"specification_type_id": 42
									}
								]
							},
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							},
							"500": {
								"groupName": "Products and Applications",
								"specs": [
									{
										"name": "Applications",
										"value": "High-quality finished products for oil and gas exploration: API 5 CT,GOST 632-80 and GOST 633-80 ",
										"valueMax": null,
										"unit": null,
										"abbreviation": null,
										"specification_type_id": 44
									}
								]
							}
						},
						"references": [
							{
								"id": 116,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Hardness testing unit",
								"description": null,
								"modifiedon": "2018-04-06 14:32:24",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "15c0c9a6-9e31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:52",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 116
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 208,
										"reference_id": 116,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Hardness testing unit",
										"lieferundleistungsumfangid": "536ad7bf-9e31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 126,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Bundling station",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "9c2ec11f-a231-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "9b1f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Bundling station",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:55",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 126
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 217,
										"reference_id": 126,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Bundling station",
										"lieferundleistungsumfangid": "2eb34d69-a231-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 125,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Induction heater",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "022687cf-a131-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "716dfb3a-4873-e111-8317-0018fe861c3e",
								"anlagentypidname": "Induction heating",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:55",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 125
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 216,
										"reference_id": 125,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Induction heater",
										"lieferundleistungsumfangid": "fe3537e6-a131-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 124,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Hydrostatic pipe tester",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "16be4f97-a131-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "132017e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Hydrostatic Testing Machine",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:54",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "25",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 124
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 219,
										"reference_id": 124,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Hydrostatic pipe tester",
										"lieferundleistungsumfangid": "7c618453-b631-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "25",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 123,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Short drifter",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "79286947-a131-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:54",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "600.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 123
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 215,
										"reference_id": 123,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Short drifter",
										"lieferundleistungsumfangid": "992ba56f-a131-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "600.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 122,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Coupling applicator station",
								"description": null,
								"modifiedon": "2018-04-06 14:32:24",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "2ceafd10-a131-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:54",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 122
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 214,
										"reference_id": 122,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Coupling applicator station",
										"lieferundleistungsumfangid": "b28afd26-a131-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 121,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Magnetic powder testing facility (2x)",
								"description": null,
								"modifiedon": "2018-04-06 14:32:24",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "8ffda1ab-a031-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:53",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 121
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 213,
										"reference_id": 121,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Magnetic powder testing machine",
										"lieferundleistungsumfangid": "747c2dcb-a031-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 120,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Thread cutting machine (2x)",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "09a45a47-a031-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:53",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 120
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 212,
										"reference_id": 120,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Thread cutting machine",
										"lieferundleistungsumfangid": "1c163e6a-a031-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 119,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Long drifter",
								"description": null,
								"modifiedon": "2018-04-06 14:32:23",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "5d5312dc-9f31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:53",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 119
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 211,
										"reference_id": 119,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Long drifter",
										"lieferundleistungsumfangid": "204b56fb-9f31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "177.8",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.24",
														"valueMax": "15",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 118,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Ultrasonic and stray flux measurement unit",
								"description": null,
								"modifiedon": "2018-04-06 14:32:26",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "e745518a-9f31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "7dc54fdc-2329-e811-acd8-00155d351b2c",
								"anlagentypidname": "Ultrasonic Testing",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:52",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 118
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 210,
										"reference_id": 118,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Ultrasonic and stray flux measurement",
										"lieferundleistungsumfangid": "d75b9db0-9f31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 117,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Full-body testing unit",
								"description": null,
								"modifiedon": "2018-04-06 14:32:24",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "ba7ceb04-9f31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:52",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 117
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 209,
										"reference_id": 117,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Full-body testing unit",
										"lieferundleistungsumfangid": "24249936-9f31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 105,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Induction furnace (3x)",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "2d33f6f9-9331-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "716dfb3a-4873-e111-8317-0018fe861c3e",
								"anlagentypidname": "Induction heating",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:48",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "127",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.8",
												"valueMax": "12.7",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 105
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 196,
										"reference_id": 105,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Induction furnace",
										"lieferundleistungsumfangid": "dd573e37-9431-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "127",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.8",
														"valueMax": "12.7",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 115,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Tube inside flushing station",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "4e8f32ed-9d31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:52",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 115
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 207,
										"reference_id": 115,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Tube inside flushing station",
										"lieferundleistungsumfangid": "5299b112-9e31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 114,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "chain-type cooling bed",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "7d31dbad-9d31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "b51f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Cooling Bed",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:51",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 114
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 206,
										"reference_id": 114,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "chain-type cooling bed",
										"lieferundleistungsumfangid": "cb5660c4-9d31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 113,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "6-roller hot straightener",
								"description": null,
								"modifiedon": "2018-04-06 14:32:26",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "0b04906b-9d31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "9d2017e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Straightening Machine",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:51",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 113
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 205,
										"reference_id": 113,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "6-roller hot straightener",
										"lieferundleistungsumfangid": "624ca48a-9d31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 112,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Annealing furnace",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "201fb0d0-9c31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Heat treatment lines",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:51",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 112
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 204,
										"reference_id": 112,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Annealing furnace",
										"lieferundleistungsumfangid": "df6d32f5-9c31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 111,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Sample Saw",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "eaa484cb-9b31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:50",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.3",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 111
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 203,
										"reference_id": 111,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Sample Saw",
										"lieferundleistungsumfangid": "9a47ef58-9c31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.3",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 110,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Quenching unit and quenching bath",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "7b7b0f52-9b31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:50",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 110
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 202,
										"reference_id": 110,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Quenching unit and quenching bath",
										"lieferundleistungsumfangid": "a46fe47f-9b31-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 109,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "High-pressure water descaling unit",
								"description": null,
								"modifiedon": "2018-04-06 14:32:24",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "ea19db00-9b31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:50",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "10",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 109
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 200,
										"reference_id": 109,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "High-pressure water descaling unit",
										"lieferundleistungsumfangid": "a2ac0176-9631-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "10",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 108,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Inspection stand",
								"description": null,
								"modifiedon": "2018-04-06 14:32:24",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "dd6b786a-9a31-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Additional Equipment",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:49",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "127",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.8",
												"valueMax": "12.7",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 108
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 198,
										"reference_id": 108,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Inspection stand",
										"lieferundleistungsumfangid": "2577e6bc-9531-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "127",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.8",
														"valueMax": "12.7",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 107,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Hardening furnace",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "cea7cefa-9531-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Heat treatment lines",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:49",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 107
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 199,
										"reference_id": 107,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Hardening furnace",
										"lieferundleistungsumfangid": "f2814a29-9631-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "219.1",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4",
														"valueMax": "25.4",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											}
										}
									}
								],
								"assets": []
							},
							{
								"id": 106,
								"customer_id": 73,
								"location_id": 76,
								"main_plant_id": null,
								"thumb": null,
								"asset_count": {
									"360": 0,
									"image": 0,
									"video": 0,
									"document": 0
								},
								"type": "reference",
								"name": "Upsetting press",
								"description": null,
								"modifiedon": "2018-04-06 14:32:25",
								"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgroupname": "Reheating Furnaces",
								"annualcapacity": null,
								"highlight_description": null,
								"typeofprojectname": null,
								"typeofproject": null,
								"yearoforder": null,
								"yearofstartup": 2010,
								"anlageid": "e397a282-9431-e811-acd8-00155d351b2c",
								"anlagenstatusname": "In Operation",
								"anlagentypid": "532017e5-e26b-e411-bb7d-00155d18200b",
								"anlagentypidname": "Pipe End Upsetting Presses",
								"bemerkungen": null,
								"datummodernisierung": null,
								"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
								"hauptanlageidname": "OCTG Finishing Center",
								"hauptlieferantidname": "SMS group GmbH",
								"ibndatum": "2010-06-30 00:00:00",
								"created_at": "2018-04-09 15:03:28",
								"updated_at": "2018-06-18 10:18:49",
								"latestSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "127",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.8",
												"valueMax": "12.7",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									},
									"-1": {
										"groupName": "More",
										"specs": [
											{
												"name": "Tube ends/h",
												"value": "160",
												"valueMax": null,
												"unit": null,
												"abbreviation": null,
												"specification_type_id": 67
											}
										]
									}
								},
								"references": [],
								"pivot": {
									"reference_parent_id": 104,
									"reference_child_id": 106
								},
								"location": {
									"id": 76,
									"modifiedon": "2016-01-07 14:49:11",
									"description": null,
									"city": "Pervouralsk",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"latitude": 56.91,
									"longitude": 59.95,
									"line1": "1, Torgovaya Street",
									"line2": null,
									"line3": null,
									"name": "Pervouralsk",
									"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
									"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
								},
								"customer": {
									"id": 73,
									"name": "Pervouralsky Novotrubny Works OJSC",
									"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
									"websiteurl": "http://www.pntz.ru",
									"modifiedon": "2018-03-16 13:39:10",
									"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
									"landidname": "Russia",
									"konzernidname": null,
									"city": "Pervouralsk",
									"line1": "Torgovaya Street 1",
									"line2": null,
									"stateorprovince": "Sverdlovsk region",
									"parentaccountid": null,
									"parentaccountidname": null,
									"shortname": "PNTZ"
								},
								"main_plant": null,
								"scopes": [
									{
										"id": 197,
										"reference_id": 106,
										"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
										"productgrouppgidname": "Reheating Furnaces",
										"typeofproject": 1,
										"typeofprojectname": "New Plant / Equipment",
										"datumibn": 2010,
										"name": "Upsetting press",
										"lieferundleistungsumfangid": "93a7f6a7-9431-e811-acd8-00155d351b2c",
										"description": null,
										"groupedSpecifications": {
											"300": {
												"groupName": "Product Dimensions",
												"specs": [
													{
														"name": "Diameter",
														"value": "60.3",
														"valueMax": "127",
														"unit": "mm",
														"abbreviation": "Ø",
														"specification_type_id": 34
													},
													{
														"name": "Wall Thicknesses",
														"value": "4.8",
														"valueMax": "12.7",
														"unit": "mm",
														"abbreviation": "Thicknesses",
														"specification_type_id": 5
													},
													{
														"name": "Pipe Length (max.)",
														"value": "12",
														"valueMax": null,
														"unit": "m",
														"abbreviation": "Length",
														"specification_type_id": 1
													}
												]
											},
											"-1": {
												"groupName": "More",
												"specs": [
													{
														"name": "Tube ends/h",
														"value": "160",
														"valueMax": null,
														"unit": null,
														"abbreviation": null,
														"specification_type_id": 67
													}
												]
											}
										}
									}
								],
								"assets": []
							}
						],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 104
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 201,
								"reference_id": 104,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "OCTG Finishing Center",
								"lieferundleistungsumfangid": "092a6a34-9a31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"100": {
										"groupName": "Plant Data - Mechanics",
										"specs": [
											{
												"name": "Additional Information",
												"value": "Finishing and Heat Treatment ",
												"valueMax": null,
												"unit": null,
												"abbreviation": null,
												"specification_type_id": 6
											},
											{
												"name": "Production Capacity",
												"value": "100000",
												"valueMax": null,
												"unit": "t/y",
												"abbreviation": "Capacity",
												"specification_type_id": 42
											}
										]
									},
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									},
									"500": {
										"groupName": "Products and Applications",
										"specs": [
											{
												"name": "Applications",
												"value": "High-quality finished products for oil and gas exploration: API 5 CT,GOST 632-80 and GOST 633-80 ",
												"valueMax": null,
												"unit": null,
												"abbreviation": null,
												"specification_type_id": 44
											}
										]
									}
								}
							},
							{
								"id": 218,
								"reference_id": 104,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Electric & Automation",
								"lieferundleistungsumfangid": "77f95e53-a331-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"13": {
										"groupName": "Level 3 Automation",
										"specs": [
											{
												"name": "Manufacturing Execution System",
												"value": null,
												"valueMax": null,
												"unit": null,
												"abbreviation": "MES",
												"specification_type_id": 48
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 105,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Induction furnace (3x)",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "2d33f6f9-9331-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "716dfb3a-4873-e111-8317-0018fe861c3e",
						"anlagentypidname": "Induction heating",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:48",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "127",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.8",
										"valueMax": "12.7",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 105
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 196,
								"reference_id": 105,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Induction furnace",
								"lieferundleistungsumfangid": "dd573e37-9431-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "127",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.8",
												"valueMax": "12.7",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 106,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Upsetting press",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "e397a282-9431-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "532017e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Pipe End Upsetting Presses",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:49",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "127",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.8",
										"valueMax": "12.7",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							},
							"-1": {
								"groupName": "More",
								"specs": [
									{
										"name": "Tube ends/h",
										"value": "160",
										"valueMax": null,
										"unit": null,
										"abbreviation": null,
										"specification_type_id": 67
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 106
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 197,
								"reference_id": 106,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Upsetting press",
								"lieferundleistungsumfangid": "93a7f6a7-9431-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "127",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.8",
												"valueMax": "12.7",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									},
									"-1": {
										"groupName": "More",
										"specs": [
											{
												"name": "Tube ends/h",
												"value": "160",
												"valueMax": null,
												"unit": null,
												"abbreviation": null,
												"specification_type_id": 67
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 108,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Inspection stand",
						"description": null,
						"modifiedon": "2018-04-06 14:32:24",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "dd6b786a-9a31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:49",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "127",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.8",
										"valueMax": "12.7",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 108
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 198,
								"reference_id": 108,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Inspection stand",
								"lieferundleistungsumfangid": "2577e6bc-9531-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "127",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.8",
												"valueMax": "12.7",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 107,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Hardening furnace",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "cea7cefa-9531-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Heat treatment lines",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:49",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 107
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 199,
								"reference_id": 107,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Hardening furnace",
								"lieferundleistungsumfangid": "f2814a29-9631-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 109,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "High-pressure water descaling unit",
						"description": null,
						"modifiedon": "2018-04-06 14:32:24",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "ea19db00-9b31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:50",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "10",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 109
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 200,
								"reference_id": 109,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "High-pressure water descaling unit",
								"lieferundleistungsumfangid": "a2ac0176-9631-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "10",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 110,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Quenching unit and quenching bath",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "7b7b0f52-9b31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:50",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 110
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 202,
								"reference_id": 110,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Quenching unit and quenching bath",
								"lieferundleistungsumfangid": "a46fe47f-9b31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 111,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Sample Saw",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "eaa484cb-9b31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:50",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.3",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 111
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 203,
								"reference_id": 111,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Sample Saw",
								"lieferundleistungsumfangid": "9a47ef58-9c31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.3",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 112,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Annealing furnace",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "201fb0d0-9c31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Heat treatment lines",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:51",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 112
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 204,
								"reference_id": 112,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Annealing furnace",
								"lieferundleistungsumfangid": "df6d32f5-9c31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 113,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "6-roller hot straightener",
						"description": null,
						"modifiedon": "2018-04-06 14:32:26",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "0b04906b-9d31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "9d2017e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Straightening Machine",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:51",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 113
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 205,
								"reference_id": 113,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "6-roller hot straightener",
								"lieferundleistungsumfangid": "624ca48a-9d31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 114,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "chain-type cooling bed",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "7d31dbad-9d31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "b51f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Cooling Bed",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:51",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 114
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 206,
								"reference_id": 114,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "chain-type cooling bed",
								"lieferundleistungsumfangid": "cb5660c4-9d31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 115,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Tube inside flushing station",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "4e8f32ed-9d31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:52",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 115
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 207,
								"reference_id": 115,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Tube inside flushing station",
								"lieferundleistungsumfangid": "5299b112-9e31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 116,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Hardness testing unit",
						"description": null,
						"modifiedon": "2018-04-06 14:32:24",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "15c0c9a6-9e31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:52",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 116
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 208,
								"reference_id": 116,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Hardness testing unit",
								"lieferundleistungsumfangid": "536ad7bf-9e31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 117,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Full-body testing unit",
						"description": null,
						"modifiedon": "2018-04-06 14:32:24",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "ba7ceb04-9f31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:52",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 117
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 209,
								"reference_id": 117,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Full-body testing unit",
								"lieferundleistungsumfangid": "24249936-9f31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 118,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Ultrasonic and stray flux measurement unit",
						"description": null,
						"modifiedon": "2018-04-06 14:32:26",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "e745518a-9f31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "7dc54fdc-2329-e811-acd8-00155d351b2c",
						"anlagentypidname": "Ultrasonic Testing",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:52",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "219.1",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4",
										"valueMax": "25.4",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 118
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 210,
								"reference_id": 118,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Ultrasonic and stray flux measurement",
								"lieferundleistungsumfangid": "d75b9db0-9f31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "219.1",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4",
												"valueMax": "25.4",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 119,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Long drifter",
						"description": null,
						"modifiedon": "2018-04-06 14:32:23",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "5d5312dc-9f31-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:53",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 119
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 211,
								"reference_id": 119,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Long drifter",
								"lieferundleistungsumfangid": "204b56fb-9f31-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 120,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Thread cutting machine (2x)",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "09a45a47-a031-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:53",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 120
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 212,
								"reference_id": 120,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Thread cutting machine",
								"lieferundleistungsumfangid": "1c163e6a-a031-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 121,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Magnetic powder testing facility (2x)",
						"description": null,
						"modifiedon": "2018-04-06 14:32:24",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "8ffda1ab-a031-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:53",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 121
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 213,
								"reference_id": 121,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Magnetic powder testing machine",
								"lieferundleistungsumfangid": "747c2dcb-a031-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 122,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Coupling applicator station",
						"description": null,
						"modifiedon": "2018-04-06 14:32:24",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "2ceafd10-a131-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:54",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 122
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 214,
								"reference_id": 122,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Coupling applicator station",
								"lieferundleistungsumfangid": "b28afd26-a131-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 123,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Short drifter",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "79286947-a131-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Additional Equipment",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:54",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "600.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 123
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 215,
								"reference_id": 123,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Short drifter",
								"lieferundleistungsumfangid": "992ba56f-a131-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "600.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 124,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Hydrostatic pipe tester",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "16be4f97-a131-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "132017e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Hydrostatic Testing Machine",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:54",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "25",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 124
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 219,
								"reference_id": 124,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Hydrostatic pipe tester",
								"lieferundleistungsumfangid": "7c618453-b631-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "25",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 125,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Induction heater",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "022687cf-a131-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "716dfb3a-4873-e111-8317-0018fe861c3e",
						"anlagentypidname": "Induction heating",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:55",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 125
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 216,
								"reference_id": 125,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Induction heater",
								"lieferundleistungsumfangid": "fe3537e6-a131-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					},
					{
						"id": 126,
						"customer_id": 73,
						"location_id": 76,
						"main_plant_id": null,
						"thumb": null,
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"type": "reference",
						"name": "Bundling station",
						"description": null,
						"modifiedon": "2018-04-06 14:32:25",
						"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
						"productgroupname": "Reheating Furnaces",
						"annualcapacity": null,
						"highlight_description": null,
						"typeofprojectname": null,
						"typeofproject": null,
						"yearoforder": null,
						"yearofstartup": 2010,
						"anlageid": "9c2ec11f-a231-e811-acd8-00155d351b2c",
						"anlagenstatusname": "In Operation",
						"anlagentypid": "9b1f17e5-e26b-e411-bb7d-00155d18200b",
						"anlagentypidname": "Bundling station",
						"bemerkungen": null,
						"datummodernisierung": null,
						"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
						"hauptanlageidname": "OCTG Finishing Center",
						"hauptlieferantidname": "SMS group GmbH",
						"ibndatum": "2010-06-30 00:00:00",
						"created_at": "2018-04-09 15:03:28",
						"updated_at": "2018-06-18 10:18:55",
						"latestSpecifications": {
							"300": {
								"groupName": "Product Dimensions",
								"specs": [
									{
										"name": "Diameter",
										"value": "60.3",
										"valueMax": "177.8",
										"unit": "mm",
										"abbreviation": "Ø",
										"specification_type_id": 34
									},
									{
										"name": "Wall Thicknesses",
										"value": "4.24",
										"valueMax": "15",
										"unit": "mm",
										"abbreviation": "Thicknesses",
										"specification_type_id": 5
									},
									{
										"name": "Pipe Length (max.)",
										"value": "12",
										"valueMax": null,
										"unit": "m",
										"abbreviation": "Length",
										"specification_type_id": 1
									}
								]
							}
						},
						"references": [],
						"pivot": {
							"reference_parent_id": 268,
							"reference_child_id": 126
						},
						"location": {
							"id": 76,
							"modifiedon": "2016-01-07 14:49:11",
							"description": null,
							"city": "Pervouralsk",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"latitude": 56.91,
							"longitude": 59.95,
							"line1": "1, Torgovaya Street",
							"line2": null,
							"line3": null,
							"name": "Pervouralsk",
							"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
							"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
						},
						"customer": {
							"id": 73,
							"name": "Pervouralsky Novotrubny Works OJSC",
							"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
							"websiteurl": "http://www.pntz.ru",
							"modifiedon": "2018-03-16 13:39:10",
							"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
							"landidname": "Russia",
							"konzernidname": null,
							"city": "Pervouralsk",
							"line1": "Torgovaya Street 1",
							"line2": null,
							"stateorprovince": "Sverdlovsk region",
							"parentaccountid": null,
							"parentaccountidname": null,
							"shortname": "PNTZ"
						},
						"main_plant": null,
						"scopes": [
							{
								"id": 217,
								"reference_id": 126,
								"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
								"productgrouppgidname": "Reheating Furnaces",
								"typeofproject": 1,
								"typeofprojectname": "New Plant / Equipment",
								"datumibn": 2010,
								"name": "Bundling station",
								"lieferundleistungsumfangid": "2eb34d69-a231-e811-acd8-00155d351b2c",
								"description": null,
								"groupedSpecifications": {
									"300": {
										"groupName": "Product Dimensions",
										"specs": [
											{
												"name": "Diameter",
												"value": "60.3",
												"valueMax": "177.8",
												"unit": "mm",
												"abbreviation": "Ø",
												"specification_type_id": 34
											},
											{
												"name": "Wall Thicknesses",
												"value": "4.24",
												"valueMax": "15",
												"unit": "mm",
												"abbreviation": "Thicknesses",
												"specification_type_id": 5
											},
											{
												"name": "Pipe Length (max.)",
												"value": "12",
												"valueMax": null,
												"unit": "m",
												"abbreviation": "Length",
												"specification_type_id": 1
											}
										]
									}
								}
							}
						],
						"assets": []
					}
				],
				"pivot": {
					"reference_set_id": 10,
					"reference_id": 268,
					"created_at": "2018-06-15 12:03:51",
					"updated_at": "2018-08-13 08:46:58",
					"sorting": 0
				},
				"location": {
					"id": 76,
					"modifiedon": "2016-01-07 14:49:11",
					"description": null,
					"city": "Pervouralsk",
					"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
					"landidname": "Russia",
					"latitude": 56.91,
					"longitude": 59.95,
					"line1": "1, Torgovaya Street",
					"line2": null,
					"line3": null,
					"name": "Pervouralsk",
					"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
					"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
					"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
				},
				"customer": {
					"id": 73,
					"name": "Pervouralsky Novotrubny Works OJSC",
					"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
					"websiteurl": "http://www.pntz.ru",
					"modifiedon": "2018-03-16 13:39:10",
					"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
					"landidname": "Russia",
					"konzernidname": null,
					"city": "Pervouralsk",
					"line1": "Torgovaya Street 1",
					"line2": null,
					"stateorprovince": "Sverdlovsk region",
					"parentaccountid": null,
					"parentaccountidname": null,
					"shortname": "PNTZ"
				},
				"main_plant": {
					"id": 104,
					"customer_id": 73,
					"location_id": 76,
					"main_plant_id": null,
					"thumb": null,
					"asset_count": {
						"360": 0,
						"image": 0,
						"video": 0,
						"document": 0
					},
					"type": "complex",
					"name": "OCTG Finishing Center",
					"description": null,
					"modifiedon": "2018-04-06 14:31:33",
					"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
					"productgroupname": "Reheating Furnaces",
					"annualcapacity": null,
					"highlight_description": null,
					"typeofprojectname": null,
					"typeofproject": null,
					"yearoforder": null,
					"yearofstartup": 2010,
					"anlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
					"anlagenstatusname": "In Operation",
					"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
					"anlagentypidname": "Heat treatment lines",
					"bemerkungen": null,
					"datummodernisierung": null,
					"hauptanlageid": null,
					"hauptanlageidname": null,
					"hauptlieferantidname": "SMS group GmbH",
					"ibndatum": "2010-06-30 00:00:00",
					"created_at": "2018-04-09 15:03:28",
					"updated_at": "2018-06-18 10:18:48",
					"latestSpecifications": {
						"13": {
							"groupName": "Level 3 Automation",
							"specs": [
								{
									"name": "Manufacturing Execution System",
									"value": null,
									"valueMax": null,
									"unit": null,
									"abbreviation": "MES",
									"specification_type_id": 48
								}
							]
						},
						"100": {
							"groupName": "Plant Data - Mechanics",
							"specs": [
								{
									"name": "Additional Information",
									"value": "Finishing and Heat Treatment ",
									"valueMax": null,
									"unit": null,
									"abbreviation": null,
									"specification_type_id": 6
								},
								{
									"name": "Production Capacity",
									"value": "100000",
									"valueMax": null,
									"unit": "t/y",
									"abbreviation": "Capacity",
									"specification_type_id": 42
								}
							]
						},
						"300": {
							"groupName": "Product Dimensions",
							"specs": [
								{
									"name": "Diameter",
									"value": "60.3",
									"valueMax": "219.1",
									"unit": "mm",
									"abbreviation": "Ø",
									"specification_type_id": 34
								},
								{
									"name": "Wall Thicknesses",
									"value": "4",
									"valueMax": "25.4",
									"unit": "mm",
									"abbreviation": "Thicknesses",
									"specification_type_id": 5
								},
								{
									"name": "Pipe Length (max.)",
									"value": "12",
									"valueMax": null,
									"unit": "m",
									"abbreviation": "Length",
									"specification_type_id": 1
								}
							]
						},
						"500": {
							"groupName": "Products and Applications",
							"specs": [
								{
									"name": "Applications",
									"value": "High-quality finished products for oil and gas exploration: API 5 CT,GOST 632-80 and GOST 633-80 ",
									"valueMax": null,
									"unit": null,
									"abbreviation": null,
									"specification_type_id": 44
								}
							]
						}
					},
					"references": [
						{
							"id": 116,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Hardness testing unit",
							"description": null,
							"modifiedon": "2018-04-06 14:32:24",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "15c0c9a6-9e31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:52",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 116
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 208,
									"reference_id": 116,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Hardness testing unit",
									"lieferundleistungsumfangid": "536ad7bf-9e31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 126,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Bundling station",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "9c2ec11f-a231-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "9b1f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Bundling station",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:55",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 126
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 217,
									"reference_id": 126,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Bundling station",
									"lieferundleistungsumfangid": "2eb34d69-a231-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 125,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Induction heater",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "022687cf-a131-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "716dfb3a-4873-e111-8317-0018fe861c3e",
							"anlagentypidname": "Induction heating",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:55",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 125
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 216,
									"reference_id": 125,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Induction heater",
									"lieferundleistungsumfangid": "fe3537e6-a131-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 124,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Hydrostatic pipe tester",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "16be4f97-a131-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "132017e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Hydrostatic Testing Machine",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:54",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "25",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 124
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 219,
									"reference_id": 124,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Hydrostatic pipe tester",
									"lieferundleistungsumfangid": "7c618453-b631-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "25",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 123,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Short drifter",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "79286947-a131-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:54",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "600.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 123
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 215,
									"reference_id": 123,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Short drifter",
									"lieferundleistungsumfangid": "992ba56f-a131-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "600.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 122,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Coupling applicator station",
							"description": null,
							"modifiedon": "2018-04-06 14:32:24",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "2ceafd10-a131-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:54",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 122
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 214,
									"reference_id": 122,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Coupling applicator station",
									"lieferundleistungsumfangid": "b28afd26-a131-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 121,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Magnetic powder testing facility (2x)",
							"description": null,
							"modifiedon": "2018-04-06 14:32:24",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "8ffda1ab-a031-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:53",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 121
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 213,
									"reference_id": 121,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Magnetic powder testing machine",
									"lieferundleistungsumfangid": "747c2dcb-a031-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 120,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Thread cutting machine (2x)",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "09a45a47-a031-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:53",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 120
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 212,
									"reference_id": 120,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Thread cutting machine",
									"lieferundleistungsumfangid": "1c163e6a-a031-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 119,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Long drifter",
							"description": null,
							"modifiedon": "2018-04-06 14:32:23",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "5d5312dc-9f31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:53",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "177.8",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.24",
											"valueMax": "15",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 119
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 211,
									"reference_id": 119,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Long drifter",
									"lieferundleistungsumfangid": "204b56fb-9f31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "177.8",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.24",
													"valueMax": "15",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 118,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Ultrasonic and stray flux measurement unit",
							"description": null,
							"modifiedon": "2018-04-06 14:32:26",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "e745518a-9f31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "7dc54fdc-2329-e811-acd8-00155d351b2c",
							"anlagentypidname": "Ultrasonic Testing",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:52",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 118
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 210,
									"reference_id": 118,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Ultrasonic and stray flux measurement",
									"lieferundleistungsumfangid": "d75b9db0-9f31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 117,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Full-body testing unit",
							"description": null,
							"modifiedon": "2018-04-06 14:32:24",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "ba7ceb04-9f31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:52",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 117
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 209,
									"reference_id": 117,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Full-body testing unit",
									"lieferundleistungsumfangid": "24249936-9f31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 105,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Induction furnace (3x)",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "2d33f6f9-9331-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "716dfb3a-4873-e111-8317-0018fe861c3e",
							"anlagentypidname": "Induction heating",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:48",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "127",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.8",
											"valueMax": "12.7",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 105
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 196,
									"reference_id": 105,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Induction furnace",
									"lieferundleistungsumfangid": "dd573e37-9431-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "127",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.8",
													"valueMax": "12.7",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 115,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Tube inside flushing station",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "4e8f32ed-9d31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:52",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 115
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 207,
									"reference_id": 115,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Tube inside flushing station",
									"lieferundleistungsumfangid": "5299b112-9e31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 114,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "chain-type cooling bed",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "7d31dbad-9d31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "b51f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Cooling Bed",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:51",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 114
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 206,
									"reference_id": 114,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "chain-type cooling bed",
									"lieferundleistungsumfangid": "cb5660c4-9d31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 113,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "6-roller hot straightener",
							"description": null,
							"modifiedon": "2018-04-06 14:32:26",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "0b04906b-9d31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "9d2017e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Straightening Machine",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:51",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 113
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 205,
									"reference_id": 113,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "6-roller hot straightener",
									"lieferundleistungsumfangid": "624ca48a-9d31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 112,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Annealing furnace",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "201fb0d0-9c31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Heat treatment lines",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:51",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 112
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 204,
									"reference_id": 112,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Annealing furnace",
									"lieferundleistungsumfangid": "df6d32f5-9c31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 111,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Sample Saw",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "eaa484cb-9b31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:50",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.3",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 111
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 203,
									"reference_id": 111,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Sample Saw",
									"lieferundleistungsumfangid": "9a47ef58-9c31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.3",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 110,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Quenching unit and quenching bath",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "7b7b0f52-9b31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:50",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 110
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 202,
									"reference_id": 110,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Quenching unit and quenching bath",
									"lieferundleistungsumfangid": "a46fe47f-9b31-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 109,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "High-pressure water descaling unit",
							"description": null,
							"modifiedon": "2018-04-06 14:32:24",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "ea19db00-9b31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:50",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "10",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 109
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 200,
									"reference_id": 109,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "High-pressure water descaling unit",
									"lieferundleistungsumfangid": "a2ac0176-9631-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "10",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 108,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Inspection stand",
							"description": null,
							"modifiedon": "2018-04-06 14:32:24",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "dd6b786a-9a31-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "831f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Additional Equipment",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:49",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "127",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.8",
											"valueMax": "12.7",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 108
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 198,
									"reference_id": 108,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Inspection stand",
									"lieferundleistungsumfangid": "2577e6bc-9531-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "127",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.8",
													"valueMax": "12.7",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 107,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Hardening furnace",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "cea7cefa-9531-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "f71f17e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Heat treatment lines",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:49",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 107
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 199,
									"reference_id": 107,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Hardening furnace",
									"lieferundleistungsumfangid": "f2814a29-9631-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "219.1",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4",
													"valueMax": "25.4",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										}
									}
								}
							],
							"assets": []
						},
						{
							"id": 106,
							"customer_id": 73,
							"location_id": 76,
							"main_plant_id": null,
							"thumb": null,
							"asset_count": {
								"360": 0,
								"image": 0,
								"video": 0,
								"document": 0
							},
							"type": "reference",
							"name": "Upsetting press",
							"description": null,
							"modifiedon": "2018-04-06 14:32:25",
							"productgroup": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgroupname": "Reheating Furnaces",
							"annualcapacity": null,
							"highlight_description": null,
							"typeofprojectname": null,
							"typeofproject": null,
							"yearoforder": null,
							"yearofstartup": 2010,
							"anlageid": "e397a282-9431-e811-acd8-00155d351b2c",
							"anlagenstatusname": "In Operation",
							"anlagentypid": "532017e5-e26b-e411-bb7d-00155d18200b",
							"anlagentypidname": "Pipe End Upsetting Presses",
							"bemerkungen": null,
							"datummodernisierung": null,
							"hauptanlageid": "49c2a8ef-9131-e811-acd8-00155d351b2c",
							"hauptanlageidname": "OCTG Finishing Center",
							"hauptlieferantidname": "SMS group GmbH",
							"ibndatum": "2010-06-30 00:00:00",
							"created_at": "2018-04-09 15:03:28",
							"updated_at": "2018-06-18 10:18:49",
							"latestSpecifications": {
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "127",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4.8",
											"valueMax": "12.7",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								},
								"-1": {
									"groupName": "More",
									"specs": [
										{
											"name": "Tube ends/h",
											"value": "160",
											"valueMax": null,
											"unit": null,
											"abbreviation": null,
											"specification_type_id": 67
										}
									]
								}
							},
							"references": [],
							"pivot": {
								"reference_parent_id": 104,
								"reference_child_id": 106
							},
							"location": {
								"id": 76,
								"modifiedon": "2016-01-07 14:49:11",
								"description": null,
								"city": "Pervouralsk",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"latitude": 56.91,
								"longitude": 59.95,
								"line1": "1, Torgovaya Street",
								"line2": null,
								"line3": null,
								"name": "Pervouralsk",
								"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
								"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
							},
							"customer": {
								"id": 73,
								"name": "Pervouralsky Novotrubny Works OJSC",
								"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
								"websiteurl": "http://www.pntz.ru",
								"modifiedon": "2018-03-16 13:39:10",
								"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
								"landidname": "Russia",
								"konzernidname": null,
								"city": "Pervouralsk",
								"line1": "Torgovaya Street 1",
								"line2": null,
								"stateorprovince": "Sverdlovsk region",
								"parentaccountid": null,
								"parentaccountidname": null,
								"shortname": "PNTZ"
							},
							"main_plant": null,
							"scopes": [
								{
									"id": 197,
									"reference_id": 106,
									"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
									"productgrouppgidname": "Reheating Furnaces",
									"typeofproject": 1,
									"typeofprojectname": "New Plant / Equipment",
									"datumibn": 2010,
									"name": "Upsetting press",
									"lieferundleistungsumfangid": "93a7f6a7-9431-e811-acd8-00155d351b2c",
									"description": null,
									"groupedSpecifications": {
										"300": {
											"groupName": "Product Dimensions",
											"specs": [
												{
													"name": "Diameter",
													"value": "60.3",
													"valueMax": "127",
													"unit": "mm",
													"abbreviation": "Ø",
													"specification_type_id": 34
												},
												{
													"name": "Wall Thicknesses",
													"value": "4.8",
													"valueMax": "12.7",
													"unit": "mm",
													"abbreviation": "Thicknesses",
													"specification_type_id": 5
												},
												{
													"name": "Pipe Length (max.)",
													"value": "12",
													"valueMax": null,
													"unit": "m",
													"abbreviation": "Length",
													"specification_type_id": 1
												}
											]
										},
										"-1": {
											"groupName": "More",
											"specs": [
												{
													"name": "Tube ends/h",
													"value": "160",
													"valueMax": null,
													"unit": null,
													"abbreviation": null,
													"specification_type_id": 67
												}
											]
										}
									}
								}
							],
							"assets": []
						}
					],
					"location": {
						"id": 76,
						"modifiedon": "2016-01-07 14:49:11",
						"description": null,
						"city": "Pervouralsk",
						"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
						"landidname": "Russia",
						"latitude": 56.91,
						"longitude": 59.95,
						"line1": "1, Torgovaya Street",
						"line2": null,
						"line3": null,
						"name": "Pervouralsk",
						"parentaccountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
						"parentaccountidname": "Pervouralsky Novotrubny Works OJSC",
						"standortid": "90232685-1e8c-db11-b5d0-0018fe861c3e"
					},
					"customer": {
						"id": 73,
						"name": "Pervouralsky Novotrubny Works OJSC",
						"accountid": "287c316a-138c-db11-b5d0-0018fe861c3e",
						"websiteurl": "http://www.pntz.ru",
						"modifiedon": "2018-03-16 13:39:10",
						"landid": "05b8e99d-108c-db11-b5d0-0018fe861c3e",
						"landidname": "Russia",
						"konzernidname": null,
						"city": "Pervouralsk",
						"line1": "Torgovaya Street 1",
						"line2": null,
						"stateorprovince": "Sverdlovsk region",
						"parentaccountid": null,
						"parentaccountidname": null,
						"shortname": "PNTZ"
					},
					"main_plant": null,
					"scopes": [
						{
							"id": 201,
							"reference_id": 104,
							"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgrouppgidname": "Reheating Furnaces",
							"typeofproject": 1,
							"typeofprojectname": "New Plant / Equipment",
							"datumibn": 2010,
							"name": "OCTG Finishing Center",
							"lieferundleistungsumfangid": "092a6a34-9a31-e811-acd8-00155d351b2c",
							"description": null,
							"groupedSpecifications": {
								"100": {
									"groupName": "Plant Data - Mechanics",
									"specs": [
										{
											"name": "Additional Information",
											"value": "Finishing and Heat Treatment ",
											"valueMax": null,
											"unit": null,
											"abbreviation": null,
											"specification_type_id": 6
										},
										{
											"name": "Production Capacity",
											"value": "100000",
											"valueMax": null,
											"unit": "t/y",
											"abbreviation": "Capacity",
											"specification_type_id": 42
										}
									]
								},
								"300": {
									"groupName": "Product Dimensions",
									"specs": [
										{
											"name": "Diameter",
											"value": "60.3",
											"valueMax": "219.1",
											"unit": "mm",
											"abbreviation": "Ø",
											"specification_type_id": 34
										},
										{
											"name": "Wall Thicknesses",
											"value": "4",
											"valueMax": "25.4",
											"unit": "mm",
											"abbreviation": "Thicknesses",
											"specification_type_id": 5
										},
										{
											"name": "Pipe Length (max.)",
											"value": "12",
											"valueMax": null,
											"unit": "m",
											"abbreviation": "Length",
											"specification_type_id": 1
										}
									]
								},
								"500": {
									"groupName": "Products and Applications",
									"specs": [
										{
											"name": "Applications",
											"value": "High-quality finished products for oil and gas exploration: API 5 CT,GOST 632-80 and GOST 633-80 ",
											"valueMax": null,
											"unit": null,
											"abbreviation": null,
											"specification_type_id": 44
										}
									]
								}
							}
						},
						{
							"id": 218,
							"reference_id": 104,
							"productgrouppgid": "10599e62-da6b-e411-bb7d-00155d18200b",
							"productgrouppgidname": "Reheating Furnaces",
							"typeofproject": 1,
							"typeofprojectname": "New Plant / Equipment",
							"datumibn": 2010,
							"name": "Electric & Automation",
							"lieferundleistungsumfangid": "77f95e53-a331-e811-acd8-00155d351b2c",
							"description": null,
							"groupedSpecifications": {
								"13": {
									"groupName": "Level 3 Automation",
									"specs": [
										{
											"name": "Manufacturing Execution System",
											"value": null,
											"valueMax": null,
											"unit": null,
											"abbreviation": "MES",
											"specification_type_id": 48
										}
									]
								}
							}
						}
					],
					"assets": []
				},
				"scopes": [],
				"assets": [
					{
						"id": 406,
						"reference_id": 268,
						"filename": "public/reference_assets/9d862c6d4d9b91359fce0c3cc527f004.mp4",
						"name": "Pervour. _ OCTG Finishing Center (upsetting line)",
						"thumb": "public/reference_assets/thumb_9d862c6d4d9b91359fce0c3cc527f004.jpg",
						"type": "video",
						"sorting": 0,
						"created_at": "2018-04-12 16:13:28",
						"updated_at": "2018-04-20 10:42:19",
						"low": null,
						"video_thumb_time": "00:00:05",
						"video_poster": "public/reference_assets/video_poster_c74f16a45d63859dab12cf13dd6be502.jpg"
					},
					{
						"id": 407,
						"reference_id": 268,
						"filename": "public/reference_assets/fb779c007673c179d567635656e388d3.mp4",
						"name": "Pervour. _OCTG Finishing Center (heat treatment line)",
						"thumb": "public/reference_assets/thumb_fb779c007673c179d567635656e388d3.jpg",
						"type": "video",
						"sorting": 1,
						"created_at": "2018-04-12 16:13:41",
						"updated_at": "2018-04-20 10:42:19",
						"low": null,
						"video_thumb_time": "00:00:05",
						"video_poster": "public/reference_assets/video_poster_b621bf6622c0c9cd7b430af36c6518e2.jpg"
					},
					{
						"id": 408,
						"reference_id": 268,
						"filename": "public/reference_assets/6324780da1379b53c6bb6b5dd7aaf3d0.mp4",
						"name": "Pervour._ OCTG Finishing Center (Tubing_Casing line)",
						"thumb": "public/reference_assets/thumb_6324780da1379b53c6bb6b5dd7aaf3d0.jpg",
						"type": "video",
						"sorting": 2,
						"created_at": "2018-04-12 16:14:06",
						"updated_at": "2018-04-20 10:42:20",
						"low": null,
						"video_thumb_time": "00:00:05",
						"video_poster": "public/reference_assets/video_poster_a04874276db84ee93dc5dad2bccc732a.jpg"
					},
					{
						"id": 166,
						"reference_id": 268,
						"filename": "public/reference_assets/60e68660a642250ebb7918f3cca224e0.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (15)",
						"thumb": "public/reference_assets/thumb_60e68660a642250ebb7918f3cca224e0.jpg",
						"type": "image",
						"sorting": 3,
						"created_at": "2018-04-10 09:22:17",
						"updated_at": "2018-04-24 15:31:14",
						"low": "public/reference_assets/low_60e68660a642250ebb7918f3cca224e0.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 158,
						"reference_id": 268,
						"filename": "public/reference_assets/92cc21895d0edf80f9567120c7e73f86.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (7)",
						"thumb": "public/reference_assets/thumb_92cc21895d0edf80f9567120c7e73f86.jpg",
						"type": "image",
						"sorting": 4,
						"created_at": "2018-04-10 09:22:09",
						"updated_at": "2018-04-24 15:30:57",
						"low": "public/reference_assets/low_92cc21895d0edf80f9567120c7e73f86.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 156,
						"reference_id": 268,
						"filename": "public/reference_assets/c271c4fef7075808e71dfe62f977608c.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (5)",
						"thumb": "public/reference_assets/thumb_c271c4fef7075808e71dfe62f977608c.jpg",
						"type": "image",
						"sorting": 5,
						"created_at": "2018-04-10 09:22:07",
						"updated_at": "2018-04-24 15:30:52",
						"low": "public/reference_assets/low_c271c4fef7075808e71dfe62f977608c.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 157,
						"reference_id": 268,
						"filename": "public/reference_assets/9f2e5125886e9b4fa24ac063626edb75.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (6)",
						"thumb": "public/reference_assets/thumb_9f2e5125886e9b4fa24ac063626edb75.jpg",
						"type": "image",
						"sorting": 6,
						"created_at": "2018-04-10 09:22:08",
						"updated_at": "2018-04-24 15:30:55",
						"low": "public/reference_assets/low_9f2e5125886e9b4fa24ac063626edb75.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 160,
						"reference_id": 268,
						"filename": "public/reference_assets/40a4fe0fdf4bde0d09956b0cf43406ff.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (9)",
						"thumb": "public/reference_assets/thumb_40a4fe0fdf4bde0d09956b0cf43406ff.jpg",
						"type": "image",
						"sorting": 7,
						"created_at": "2018-04-10 09:22:12",
						"updated_at": "2018-04-24 15:31:02",
						"low": "public/reference_assets/low_40a4fe0fdf4bde0d09956b0cf43406ff.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 161,
						"reference_id": 268,
						"filename": "public/reference_assets/640a94b74c2969646da4ae95deb16977.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (10)",
						"thumb": "public/reference_assets/thumb_640a94b74c2969646da4ae95deb16977.jpg",
						"type": "image",
						"sorting": 8,
						"created_at": "2018-04-10 09:22:12",
						"updated_at": "2018-04-24 15:31:04",
						"low": "public/reference_assets/low_640a94b74c2969646da4ae95deb16977.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 154,
						"reference_id": 268,
						"filename": "public/reference_assets/f0fa108e0d0ad7a56814e3beafc0ba9b.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (3)",
						"thumb": "public/reference_assets/thumb_f0fa108e0d0ad7a56814e3beafc0ba9b.jpg",
						"type": "image",
						"sorting": 9,
						"created_at": "2018-04-10 09:22:06",
						"updated_at": "2018-04-24 15:30:50",
						"low": "public/reference_assets/low_f0fa108e0d0ad7a56814e3beafc0ba9b.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 162,
						"reference_id": 268,
						"filename": "public/reference_assets/756a8566f8efab3aff7ef75ea71574e2.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (11)",
						"thumb": "public/reference_assets/thumb_756a8566f8efab3aff7ef75ea71574e2.jpg",
						"type": "image",
						"sorting": 10,
						"created_at": "2018-04-10 09:22:13",
						"updated_at": "2018-04-24 15:31:06",
						"low": "public/reference_assets/low_756a8566f8efab3aff7ef75ea71574e2.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 163,
						"reference_id": 268,
						"filename": "public/reference_assets/a353270fbc46057cd9c21de5543a768c.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (12)",
						"thumb": "public/reference_assets/thumb_a353270fbc46057cd9c21de5543a768c.jpg",
						"type": "image",
						"sorting": 11,
						"created_at": "2018-04-10 09:22:14",
						"updated_at": "2018-04-24 15:31:08",
						"low": "public/reference_assets/low_a353270fbc46057cd9c21de5543a768c.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 155,
						"reference_id": 268,
						"filename": "public/reference_assets/02e078c117110d9b7443b4e08f564d6b.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (4)",
						"thumb": "public/reference_assets/thumb_02e078c117110d9b7443b4e08f564d6b.jpg",
						"type": "image",
						"sorting": 12,
						"created_at": "2018-04-10 09:22:07",
						"updated_at": "2018-04-24 15:30:52",
						"low": "public/reference_assets/low_02e078c117110d9b7443b4e08f564d6b.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 159,
						"reference_id": 268,
						"filename": "public/reference_assets/1bd9303d26de048e891074f4bd4c6bc6.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (8)",
						"thumb": "public/reference_assets/thumb_1bd9303d26de048e891074f4bd4c6bc6.jpg",
						"type": "image",
						"sorting": 13,
						"created_at": "2018-04-10 09:22:10",
						"updated_at": "2018-04-24 15:30:59",
						"low": "public/reference_assets/low_1bd9303d26de048e891074f4bd4c6bc6.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 164,
						"reference_id": 268,
						"filename": "public/reference_assets/1ca1106369d11c2d98cd535e8da35c41.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (13)",
						"thumb": "public/reference_assets/thumb_1ca1106369d11c2d98cd535e8da35c41.jpg",
						"type": "image",
						"sorting": 14,
						"created_at": "2018-04-10 09:22:15",
						"updated_at": "2018-04-24 15:31:09",
						"low": "public/reference_assets/low_1ca1106369d11c2d98cd535e8da35c41.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 165,
						"reference_id": 268,
						"filename": "public/reference_assets/49304f8ef15b1963ec81157942479711.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (14)",
						"thumb": "public/reference_assets/thumb_49304f8ef15b1963ec81157942479711.jpg",
						"type": "image",
						"sorting": 15,
						"created_at": "2018-04-10 09:22:16",
						"updated_at": "2018-04-24 15:31:12",
						"low": "public/reference_assets/low_49304f8ef15b1963ec81157942479711.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 152,
						"reference_id": 268,
						"filename": "public/reference_assets/af8026fcca62785381d554fbed1ed79c.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (1)",
						"thumb": "public/reference_assets/thumb_af8026fcca62785381d554fbed1ed79c.jpg",
						"type": "image",
						"sorting": 16,
						"created_at": "2018-04-10 09:22:04",
						"updated_at": "2018-04-24 15:30:46",
						"low": "public/reference_assets/low_af8026fcca62785381d554fbed1ed79c.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 153,
						"reference_id": 268,
						"filename": "public/reference_assets/378dd7ba83b7c1d33ebccebf27031a35.jpg",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line (2)",
						"thumb": "public/reference_assets/thumb_378dd7ba83b7c1d33ebccebf27031a35.jpg",
						"type": "image",
						"sorting": 17,
						"created_at": "2018-04-10 09:22:05",
						"updated_at": "2018-04-24 15:30:48",
						"low": "public/reference_assets/low_378dd7ba83b7c1d33ebccebf27031a35.jpg",
						"video_thumb_time": "00:00:05",
						"video_poster": null
					},
					{
						"id": 13,
						"reference_id": 268,
						"filename": "public/reference_assets/eda901f712299ce544601243f3347938.mp4",
						"name": "Pervour._ OCTG Finishing Center and Heat Treatment Line",
						"thumb": "public/reference_assets/thumb_eda901f712299ce544601243f3347938.jpg",
						"type": "video",
						"sorting": 18,
						"created_at": "2018-04-10 08:32:57",
						"updated_at": "2018-04-20 10:42:15",
						"low": null,
						"video_thumb_time": "00:00:05",
						"video_poster": "public/reference_assets/video_poster_f5165c80ab6e80e99eee112fffdbec02.jpg"
					}
				]
			}
		]
	};
	// the reference object
	window.referenceObj = {};
})(window);
