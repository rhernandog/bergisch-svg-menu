"use strict";
// http://sra-backend.dev2.bergisch-media.net/sets/6
(window => {
	window.SVGMenuData = {
		"allCustomers": [
			{
				"id": "2afdeebd6e95986c47b62796c2be5007",
				"name": "Kamba"
			},
			{
				"id": "79dbc2a1aab76b35529be3adc7300043",
				"name": "Dabvine"
			},
			{
				"id": "2956da162da0a61e2fabb50e27b99358",
				"name": "Shuffletag"
			},
			{
				"id": "b656508b8f723ee8778e0ae6933be071",
				"name": "Jamia"
			},
			{
				"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
				"name": "Brainbox"
			}
		],
		"references": [
			{
				"id": "4e436a9372ab676810c74ba1e3564bc6",
				"name": "Holdlamis",
				"thumb": "public/reference_assets/4.jpg",
				"type": "highlight",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 1,
					"image": 2,
					"video": 1,
					"document": 1
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.mp4",
						"thumb": "public/reference_assets/3.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_3.jpg"
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "document",
						"sorting": 4,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "b46f2f823327278bcc2aaa7606868407",
						"name": "Biodex",
						"thumb": "public/reference_assets/1.jpg",
						"type": "complex",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "b46f2f823327278bcc2aaa7606868407"
						}
					},
					{
						"id": "06506bf81c09b6d6e357d434e8cfaad6",
						"name": "Stronghold",
						"thumb": "public/reference_assets/12.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "06506bf81c09b6d6e357d434e8cfaad6"
						}
					},
					{
						"id": "65dd0cdd684e1f9b0af879b9a44b7260",
						"name": "Zamit",
						"thumb": "public/reference_assets/2.jpg",
						"type": "complex",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "65dd0cdd684e1f9b0af879b9a44b7260"
						}
					},
					{
						"id": "900f75743e3536a67f7f5b7c509cd62f",
						"name": "Bitchip",
						"thumb": "public/reference_assets/14.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "900f75743e3536a67f7f5b7c509cd62f"
						}
					},
					{
						"id": "a5426a6654825efda7ce6056f249d964",
						"name": "Cardify",
						"thumb": "public/reference_assets/15.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "a5426a6654825efda7ce6056f249d964"
						}
					},
					{
						"id": "cfeacc10eba9a33f9a35afed8ea3f2d8",
						"name": "Home Ing",
						"thumb": "public/reference_assets/1.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "cfeacc10eba9a33f9a35afed8ea3f2d8"
						}
					},
					{
						"id": "ad95775bcc2269ae16a1326d540ca64c",
						"name": "Asoka",
						"thumb": "public/reference_assets/5.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "4e436a9372ab676810c74ba1e3564bc6",
							"reference_child_id": "ad95775bcc2269ae16a1326d540ca64c"
						}
					}
				]
			},
			{
				"id": "ea4c8b402bf15daeb060dc5870cd45a9",
				"name": "Duobam",
				"thumb": "public/reference_assets/12.jpg",
				"type": "highlight",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 1,
					"image": 1,
					"video": 3,
					"document": 0
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.mp4",
						"thumb": "public/reference_assets/2.jpg",
						"type": "video",
						"sorting": 1,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_2.jpg"
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.mp4",
						"thumb": "public/reference_assets/3.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_3.jpg"
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.mp4",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "360",
						"sorting": 4,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "b46f2f823327278bcc2aaa7606868407",
						"name": "Biodex",
						"thumb": "public/reference_assets/1.jpg",
						"type": "complex",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "b46f2f823327278bcc2aaa7606868407"
						}
					},
					{
						"id": "06506bf81c09b6d6e357d434e8cfaad6",
						"name": "Stronghold",
						"thumb": "public/reference_assets/12.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "06506bf81c09b6d6e357d434e8cfaad6"
						}
					},
					{
						"id": "65dd0cdd684e1f9b0af879b9a44b7260",
						"name": "Zamit",
						"thumb": "public/reference_assets/2.jpg",
						"type": "complex",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "65dd0cdd684e1f9b0af879b9a44b7260"
						}
					},
					{
						"id": "900f75743e3536a67f7f5b7c509cd62f",
						"name": "Bitchip",
						"thumb": "public/reference_assets/14.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "900f75743e3536a67f7f5b7c509cd62f"
						}
					},
					{
						"id": "a5426a6654825efda7ce6056f249d964",
						"name": "Cardify",
						"thumb": "public/reference_assets/15.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "a5426a6654825efda7ce6056f249d964"
						}
					},
					{
						"id": "cfeacc10eba9a33f9a35afed8ea3f2d8",
						"name": "Home Ing",
						"thumb": "public/reference_assets/1.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "cfeacc10eba9a33f9a35afed8ea3f2d8"
						}
					},
					{
						"id": "ad95775bcc2269ae16a1326d540ca64c",
						"name": "Asoka",
						"thumb": "public/reference_assets/5.jpg",
						"type": "reference",
						"customer": {
							"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
							"name": "Brainbox"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "ea4c8b402bf15daeb060dc5870cd45a9",
							"reference_child_id": "ad95775bcc2269ae16a1326d540ca64c"
						}
					}
				]
			},
			{
				"id": "b46f2f823327278bcc2aaa7606868407",
				"name": "Biodex",
				"thumb": "public/reference_assets/1.jpg",
				"type": "complex",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 2,
					"image": 2,
					"video": 2,
					"document": 0
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": "public/reference_assets/video_poster_2.jpg"
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_3.jpg"
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					}
				],
				"references": []
			},
			{
				"id": "06506bf81c09b6d6e357d434e8cfaad6",
				"name": "Stronghold",
				"thumb": "public/reference_assets/12.jpg",
				"type": "reference",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 3,
					"image": 3,
					"video": 0,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "360",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "65dd0cdd684e1f9b0af879b9a44b7260",
				"name": "Zamit",
				"thumb": "public/reference_assets/2.jpg",
				"type": "complex",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 0,
					"image": 1,
					"video": 1,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.mp4",
						"thumb": "public/reference_assets/2.jpg",
						"type": "video",
						"sorting": 1,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_2.jpg"
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.pdf",
						"thumb": "public/reference_assets/3.jpg",
						"type": "document",
						"sorting": 2,
						"low": null,
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.pdf",
						"thumb": "public/reference_assets/4.jpg",
						"type": "document",
						"sorting": 3,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "900f75743e3536a67f7f5b7c509cd62f",
				"name": "Bitchip",
				"thumb": "public/reference_assets/14.jpg",
				"type": "reference",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 0,
					"image": 2,
					"video": 3,
					"document": 3
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.mp4",
						"thumb": "public/reference_assets/3.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_3.jpg"
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.mp4",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.mp4",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "a5426a6654825efda7ce6056f249d964",
				"name": "Cardify",
				"thumb": "public/reference_assets/15.jpg",
				"type": "reference",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 2,
					"image": 5,
					"video": 2,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.mp4",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.mp4",
						"thumb": "public/reference_assets/7.jpg",
						"type": "video",
						"sorting": 6,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_7.jpg"
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.jpg",
						"thumb": "public/reference_assets/8.jpg",
						"type": "360",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.jpg",
						"thumb": "public/reference_assets/9.jpg",
						"type": "360",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 9,
						"low": null,
						"video_poster": null
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.pdf",
						"thumb": "public/reference_assets/11.jpg",
						"type": "document",
						"sorting": 10,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "cfeacc10eba9a33f9a35afed8ea3f2d8",
				"name": "Home Ing",
				"thumb": "public/reference_assets/1.jpg",
				"type": "reference",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 2,
					"image": 5,
					"video": 2,
					"document": 0
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.mp4",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.mp4",
						"thumb": "public/reference_assets/7.jpg",
						"type": "video",
						"sorting": 6,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_7.jpg"
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.jpg",
						"thumb": "public/reference_assets/8.jpg",
						"type": "360",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.jpg",
						"thumb": "public/reference_assets/9.jpg",
						"type": "360",
						"sorting": 8,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "ad95775bcc2269ae16a1326d540ca64c",
				"name": "Asoka",
				"thumb": "public/reference_assets/5.jpg",
				"type": "reference",
				"customer": {
					"id": "fdbb3bc19efd3fa44ab5f6ec4cf8ecad",
					"name": "Brainbox"
				},
				"asset_count": {
					"360": 0,
					"image": 4,
					"video": 2,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.mp4",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.mp4",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "0dda15320f549f33551cc99c386bf0e8",
				"name": "Zamit",
				"thumb": "public/reference_assets/3.jpg",
				"type": "highlight",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 1,
					"image": 2,
					"video": 1,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.pdf",
						"thumb": "public/reference_assets/3.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_3.jpg"
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "document",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "3045d35d008c3174874c393628826c93",
						"name": "Stronghold",
						"thumb": "public/reference_assets/6.jpg",
						"type": "reference",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "0dda15320f549f33551cc99c386bf0e8",
							"reference_child_id": "3045d35d008c3174874c393628826c93"
						}
					},
					{
						"id": "6b89bb18db207183587848297b27fdde",
						"name": "Ronstring",
						"thumb": "public/reference_assets/1.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "0dda15320f549f33551cc99c386bf0e8",
							"reference_child_id": "6b89bb18db207183587848297b27fdde"
						}
					},
					{
						"id": "211b6534e7bda640691e4fcf85b871ea",
						"name": "Flexidy",
						"thumb": "public/reference_assets/3.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "0dda15320f549f33551cc99c386bf0e8",
							"reference_child_id": "211b6534e7bda640691e4fcf85b871ea"
						}
					},
					{
						"id": "f0aaa13335e0c80c1a15a0ac978242e4",
						"name": "Lotlux",
						"thumb": "public/reference_assets/6.jpg",
						"type": "reference",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "0dda15320f549f33551cc99c386bf0e8",
							"reference_child_id": "f0aaa13335e0c80c1a15a0ac978242e4"
						}
					},
					{
						"id": "f4234a84c6fd3149c023fe1f54ea110d",
						"name": "Transcof",
						"thumb": "public/reference_assets/8.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "0dda15320f549f33551cc99c386bf0e8",
							"reference_child_id": "f4234a84c6fd3149c023fe1f54ea110d"
						}
					},
					{
						"id": "bc5af50e1f98ce4f5ca6b95a07985f3e",
						"name": "Holdlamis",
						"thumb": "public/reference_assets/13.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "0dda15320f549f33551cc99c386bf0e8",
							"reference_child_id": "bc5af50e1f98ce4f5ca6b95a07985f3e"
						}
					}
				]
			},
			{
				"id": "fde416edc5336a5b4cc7f27d761ef173",
				"name": "Tempsoft",
				"thumb": "public/reference_assets/5.jpg",
				"type": "highlight",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 3,
					"image": 7,
					"video": 0,
					"document": 4
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "image",
						"sorting": 6,
						"low": "public/reference_assets/low_7.jpg",
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.jpg",
						"thumb": "public/reference_assets/8.jpg",
						"type": "360",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.jpg",
						"thumb": "public/reference_assets/9.jpg",
						"type": "360",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.jpg",
						"thumb": "public/reference_assets/10.jpg",
						"type": "360",
						"sorting": 9,
						"low": null,
						"video_poster": null
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.pdf",
						"thumb": "public/reference_assets/11.jpg",
						"type": "document",
						"sorting": 10,
						"low": null,
						"video_poster": null
					},
					{
						"id": 12,
						"filename": "public/reference_assets/12.pdf",
						"thumb": "public/reference_assets/12.jpg",
						"type": "document",
						"sorting": 11,
						"low": null,
						"video_poster": null
					},
					{
						"id": 13,
						"filename": "public/reference_assets/13.pdf",
						"thumb": "public/reference_assets/13.jpg",
						"type": "document",
						"sorting": 12,
						"low": null,
						"video_poster": null
					},
					{
						"id": 14,
						"filename": "public/reference_assets/14.pdf",
						"thumb": "public/reference_assets/14.jpg",
						"type": "document",
						"sorting": 13,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "3045d35d008c3174874c393628826c93",
						"name": "Stronghold",
						"thumb": "public/reference_assets/6.jpg",
						"type": "reference",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "fde416edc5336a5b4cc7f27d761ef173",
							"reference_child_id": "3045d35d008c3174874c393628826c93"
						}
					},
					{
						"id": "6b89bb18db207183587848297b27fdde",
						"name": "Ronstring",
						"thumb": "public/reference_assets/1.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "fde416edc5336a5b4cc7f27d761ef173",
							"reference_child_id": "6b89bb18db207183587848297b27fdde"
						}
					},
					{
						"id": "211b6534e7bda640691e4fcf85b871ea",
						"name": "Flexidy",
						"thumb": "public/reference_assets/3.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "fde416edc5336a5b4cc7f27d761ef173",
							"reference_child_id": "211b6534e7bda640691e4fcf85b871ea"
						}
					},
					{
						"id": "f0aaa13335e0c80c1a15a0ac978242e4",
						"name": "Lotlux",
						"thumb": "public/reference_assets/6.jpg",
						"type": "reference",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "fde416edc5336a5b4cc7f27d761ef173",
							"reference_child_id": "f0aaa13335e0c80c1a15a0ac978242e4"
						}
					},
					{
						"id": "f4234a84c6fd3149c023fe1f54ea110d",
						"name": "Transcof",
						"thumb": "public/reference_assets/8.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "fde416edc5336a5b4cc7f27d761ef173",
							"reference_child_id": "f4234a84c6fd3149c023fe1f54ea110d"
						}
					},
					{
						"id": "bc5af50e1f98ce4f5ca6b95a07985f3e",
						"name": "Holdlamis",
						"thumb": "public/reference_assets/13.jpg",
						"type": "complex",
						"customer": {
							"id": "79dbc2a1aab76b35529be3adc7300043",
							"name": "Dabvine"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "fde416edc5336a5b4cc7f27d761ef173",
							"reference_child_id": "bc5af50e1f98ce4f5ca6b95a07985f3e"
						}
					}
				]
			},
			{
				"id": "3045d35d008c3174874c393628826c93",
				"name": "Stronghold",
				"thumb": "public/reference_assets/6.jpg",
				"type": "reference",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 1,
					"image": 7,
					"video": 3,
					"document": 3
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "image",
						"sorting": 6,
						"low": "public/reference_assets/low_7.jpg",
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "video",
						"sorting": 8,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_9.jpg"
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "video",
						"sorting": 9,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_10.jpg"
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.jpg",
						"thumb": "public/reference_assets/11.jpg",
						"type": "360",
						"sorting": 10,
						"low": null,
						"video_poster": null
					},
					{
						"id": 12,
						"filename": "public/reference_assets/12.pdf",
						"thumb": "public/reference_assets/12.jpg",
						"type": "document",
						"sorting": 11,
						"low": null,
						"video_poster": null
					},
					{
						"id": 13,
						"filename": "public/reference_assets/13.pdf",
						"thumb": "public/reference_assets/13.jpg",
						"type": "document",
						"sorting": 12,
						"low": null,
						"video_poster": null
					},
					{
						"id": 14,
						"filename": "public/reference_assets/14.pdf",
						"thumb": "public/reference_assets/14.jpg",
						"type": "document",
						"sorting": 13,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "6b89bb18db207183587848297b27fdde",
				"name": "Ronstring",
				"thumb": "public/reference_assets/1.jpg",
				"type": "complex",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 2,
					"image": 3,
					"video": 1,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.mp4",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "360",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "211b6534e7bda640691e4fcf85b871ea",
				"name": "Flexidy",
				"thumb": "public/reference_assets/3.jpg",
				"type": "complex",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 0,
					"image": 3,
					"video": 2,
					"document": 1
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.mp4",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.mp4",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "f0aaa13335e0c80c1a15a0ac978242e4",
				"name": "Lotlux",
				"thumb": "public/reference_assets/6.jpg",
				"type": "reference",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 3,
					"image": 0,
					"video": 1,
					"document": 1
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.mp4",
						"thumb": "public/reference_assets/1.jpg",
						"type": "video",
						"sorting": 0,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_1.jpg"
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "360",
						"sorting": 2,
						"low": null,
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "document",
						"sorting": 4,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "f4234a84c6fd3149c023fe1f54ea110d",
				"name": "Transcof",
				"thumb": "public/reference_assets/8.jpg",
				"type": "complex",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 2,
					"image": 6,
					"video": 3,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.mp4",
						"thumb": "public/reference_assets/7.jpg",
						"type": "video",
						"sorting": 6,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_7.jpg"
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.mp4",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.mp4",
						"thumb": "public/reference_assets/9.jpg",
						"type": "video",
						"sorting": 8,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_9.jpg"
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.jpg",
						"thumb": "public/reference_assets/10.jpg",
						"type": "360",
						"sorting": 9,
						"low": null,
						"video_poster": null
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.jpg",
						"thumb": "public/reference_assets/11.jpg",
						"type": "360",
						"sorting": 10,
						"low": null,
						"video_poster": null
					},
					{
						"id": 12,
						"filename": "public/reference_assets/12.pdf",
						"thumb": "public/reference_assets/12.jpg",
						"type": "document",
						"sorting": 11,
						"low": null,
						"video_poster": null
					},
					{
						"id": 13,
						"filename": "public/reference_assets/13.pdf",
						"thumb": "public/reference_assets/13.jpg",
						"type": "document",
						"sorting": 12,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "bc5af50e1f98ce4f5ca6b95a07985f3e",
				"name": "Holdlamis",
				"thumb": "public/reference_assets/13.jpg",
				"type": "complex",
				"customer": {
					"id": "79dbc2a1aab76b35529be3adc7300043",
					"name": "Dabvine"
				},
				"asset_count": {
					"360": 2,
					"image": 6,
					"video": 3,
					"document": 0
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.mp4",
						"thumb": "public/reference_assets/7.jpg",
						"type": "video",
						"sorting": 6,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_7.jpg"
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.mp4",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.mp4",
						"thumb": "public/reference_assets/9.jpg",
						"type": "video",
						"sorting": 8,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_9.jpg"
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.jpg",
						"thumb": "public/reference_assets/10.jpg",
						"type": "360",
						"sorting": 9,
						"low": null,
						"video_poster": null
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.jpg",
						"thumb": "public/reference_assets/11.jpg",
						"type": "360",
						"sorting": 10,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "24cd2a0309e81af637c5c5165b5a1d1a",
				"name": "Daltfresh",
				"thumb": "public/reference_assets/13.jpg",
				"type": "highlight",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 2,
					"image": 4,
					"video": 1,
					"document": 0
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.mp4",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "360",
						"sorting": 6,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "f538f5f326d7bc8021f7f0fb22bb69f2",
						"name": "Fix San",
						"thumb": "public/reference_assets/2.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "24cd2a0309e81af637c5c5165b5a1d1a",
							"reference_child_id": "f538f5f326d7bc8021f7f0fb22bb69f2"
						}
					},
					{
						"id": "18e7c9cc72f8bf9470b3ef91bdff017b",
						"name": "Transcof",
						"thumb": "public/reference_assets/4.jpg",
						"type": "complex",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "24cd2a0309e81af637c5c5165b5a1d1a",
							"reference_child_id": "18e7c9cc72f8bf9470b3ef91bdff017b"
						}
					},
					{
						"id": "f67bb721985b7361c6db4b30d2ab7d75",
						"name": "Zathin",
						"thumb": "public/reference_assets/7.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "24cd2a0309e81af637c5c5165b5a1d1a",
							"reference_child_id": "f67bb721985b7361c6db4b30d2ab7d75"
						}
					},
					{
						"id": "bd6bd9d46f2aed64090cc24a6c7b2b6d",
						"name": "Temp",
						"thumb": "public/reference_assets/9.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "24cd2a0309e81af637c5c5165b5a1d1a",
							"reference_child_id": "bd6bd9d46f2aed64090cc24a6c7b2b6d"
						}
					},
					{
						"id": "7d65c3b30e7950025b431c11634244dd",
						"name": "Stim",
						"thumb": "public/reference_assets/2.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "24cd2a0309e81af637c5c5165b5a1d1a",
							"reference_child_id": "7d65c3b30e7950025b431c11634244dd"
						}
					},
					{
						"id": "25a89ac69e99333fa19fb01fac70c66e",
						"name": "Zaam-Dox",
						"thumb": "public/reference_assets/7.jpg",
						"type": "complex",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "24cd2a0309e81af637c5c5165b5a1d1a",
							"reference_child_id": "25a89ac69e99333fa19fb01fac70c66e"
						}
					}
				]
			},
			{
				"id": "72fefb810b749e824c0670ce4fb6be8c",
				"name": "Sub-Ex",
				"thumb": "public/reference_assets/8.jpg",
				"type": "highlight",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 0,
					"image": 6,
					"video": 2,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.mp4",
						"thumb": "public/reference_assets/7.jpg",
						"type": "video",
						"sorting": 6,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_7.jpg"
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.mp4",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 9,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "f538f5f326d7bc8021f7f0fb22bb69f2",
						"name": "Fix San",
						"thumb": "public/reference_assets/2.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "72fefb810b749e824c0670ce4fb6be8c",
							"reference_child_id": "f538f5f326d7bc8021f7f0fb22bb69f2"
						}
					},
					{
						"id": "18e7c9cc72f8bf9470b3ef91bdff017b",
						"name": "Transcof",
						"thumb": "public/reference_assets/4.jpg",
						"type": "complex",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "72fefb810b749e824c0670ce4fb6be8c",
							"reference_child_id": "18e7c9cc72f8bf9470b3ef91bdff017b"
						}
					},
					{
						"id": "f67bb721985b7361c6db4b30d2ab7d75",
						"name": "Zathin",
						"thumb": "public/reference_assets/7.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "72fefb810b749e824c0670ce4fb6be8c",
							"reference_child_id": "f67bb721985b7361c6db4b30d2ab7d75"
						}
					},
					{
						"id": "bd6bd9d46f2aed64090cc24a6c7b2b6d",
						"name": "Temp",
						"thumb": "public/reference_assets/9.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "72fefb810b749e824c0670ce4fb6be8c",
							"reference_child_id": "bd6bd9d46f2aed64090cc24a6c7b2b6d"
						}
					},
					{
						"id": "7d65c3b30e7950025b431c11634244dd",
						"name": "Stim",
						"thumb": "public/reference_assets/2.jpg",
						"type": "reference",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "72fefb810b749e824c0670ce4fb6be8c",
							"reference_child_id": "7d65c3b30e7950025b431c11634244dd"
						}
					},
					{
						"id": "25a89ac69e99333fa19fb01fac70c66e",
						"name": "Zaam-Dox",
						"thumb": "public/reference_assets/7.jpg",
						"type": "complex",
						"customer": {
							"id": "b656508b8f723ee8778e0ae6933be071",
							"name": "Jamia"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "72fefb810b749e824c0670ce4fb6be8c",
							"reference_child_id": "25a89ac69e99333fa19fb01fac70c66e"
						}
					}
				]
			},
			{
				"id": "f538f5f326d7bc8021f7f0fb22bb69f2",
				"name": "Fix San",
				"thumb": "public/reference_assets/2.jpg",
				"type": "reference",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 2,
					"image": 1,
					"video": 1,
					"document": 5
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 10,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "18e7c9cc72f8bf9470b3ef91bdff017b",
				"name": "Transcof",
				"thumb": "public/reference_assets/4.jpg",
				"type": "complex",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 3,
					"image": 3,
					"video": 3,
					"document": 4
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.pdf",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 9,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "f67bb721985b7361c6db4b30d2ab7d75",
				"name": "Zathin",
				"thumb": "public/reference_assets/7.jpg",
				"type": "reference",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 2,
					"image": 4,
					"video": 0,
					"document": 4
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "360",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 9,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "bd6bd9d46f2aed64090cc24a6c7b2b6d",
				"name": "Temp",
				"thumb": "public/reference_assets/9.jpg",
				"type": "reference",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 2,
					"image": 5,
					"video": 0,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "360",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "7d65c3b30e7950025b431c11634244dd",
				"name": "Stim",
				"thumb": "public/reference_assets/2.jpg",
				"type": "reference",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 3,
					"image": 3,
					"video": 1,
					"document": 3
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.mp4",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "360",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "360",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 9,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "25a89ac69e99333fa19fb01fac70c66e",
				"name": "Zaam-Dox",
				"thumb": "public/reference_assets/7.jpg",
				"type": "complex",
				"customer": {
					"id": "b656508b8f723ee8778e0ae6933be071",
					"name": "Jamia"
				},
				"asset_count": {
					"360": 1,
					"image": 4,
					"video": 1,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.mp4",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "3f41cf75a5d7db68e2820bd776fd8475",
				"name": "Home Ing",
				"thumb": "public/reference_assets/11.jpg",
				"type": "highlight",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 2,
					"image": 0,
					"video": 0,
					"document": 1
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "360",
						"sorting": 0,
						"low": null,
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.pdf",
						"thumb": "public/reference_assets/3.jpg",
						"type": "document",
						"sorting": 2,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "63f1415d008458bd2e0c9c2b2ea59eaf",
						"name": "Tampflex",
						"thumb": "public/reference_assets/9.jpg",
						"type": "complex",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "3f41cf75a5d7db68e2820bd776fd8475",
							"reference_child_id": "63f1415d008458bd2e0c9c2b2ea59eaf"
						}
					},
					{
						"id": "971848ea71ec5e3acdd97987d1ce834e",
						"name": "Tresom",
						"thumb": "public/reference_assets/14.jpg",
						"type": "complex",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "3f41cf75a5d7db68e2820bd776fd8475",
							"reference_child_id": "971848ea71ec5e3acdd97987d1ce834e"
						}
					},
					{
						"id": "f514fe7866f5cd78114df91822f961ab",
						"name": "Namfix",
						"thumb": "public/reference_assets/11.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "3f41cf75a5d7db68e2820bd776fd8475",
							"reference_child_id": "f514fe7866f5cd78114df91822f961ab"
						}
					},
					{
						"id": "2f15bee83feb858f12e4084e0bc42188",
						"name": "Holdlamis",
						"thumb": "public/reference_assets/3.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "3f41cf75a5d7db68e2820bd776fd8475",
							"reference_child_id": "2f15bee83feb858f12e4084e0bc42188"
						}
					},
					{
						"id": "a823acf264a631f13015224c30b07f32",
						"name": "Bigtax",
						"thumb": "public/reference_assets/4.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "3f41cf75a5d7db68e2820bd776fd8475",
							"reference_child_id": "a823acf264a631f13015224c30b07f32"
						}
					}
				]
			},
			{
				"id": "c46cacb554f111189a285cda15533b52",
				"name": "Fintone",
				"thumb": "public/reference_assets/15.jpg",
				"type": "highlight",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 1,
					"image": 2,
					"video": 1,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "360",
						"sorting": 0,
						"low": null,
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.mp4",
						"thumb": "public/reference_assets/3.jpg",
						"type": "video",
						"sorting": 2,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_3.jpg"
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "document",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "63f1415d008458bd2e0c9c2b2ea59eaf",
						"name": "Tampflex",
						"thumb": "public/reference_assets/9.jpg",
						"type": "complex",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "c46cacb554f111189a285cda15533b52",
							"reference_child_id": "63f1415d008458bd2e0c9c2b2ea59eaf"
						}
					},
					{
						"id": "971848ea71ec5e3acdd97987d1ce834e",
						"name": "Tresom",
						"thumb": "public/reference_assets/14.jpg",
						"type": "complex",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "c46cacb554f111189a285cda15533b52",
							"reference_child_id": "971848ea71ec5e3acdd97987d1ce834e"
						}
					},
					{
						"id": "f514fe7866f5cd78114df91822f961ab",
						"name": "Namfix",
						"thumb": "public/reference_assets/11.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "c46cacb554f111189a285cda15533b52",
							"reference_child_id": "f514fe7866f5cd78114df91822f961ab"
						}
					},
					{
						"id": "2f15bee83feb858f12e4084e0bc42188",
						"name": "Holdlamis",
						"thumb": "public/reference_assets/3.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "c46cacb554f111189a285cda15533b52",
							"reference_child_id": "2f15bee83feb858f12e4084e0bc42188"
						}
					},
					{
						"id": "a823acf264a631f13015224c30b07f32",
						"name": "Bigtax",
						"thumb": "public/reference_assets/4.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "c46cacb554f111189a285cda15533b52",
							"reference_child_id": "a823acf264a631f13015224c30b07f32"
						}
					}
				]
			},
			{
				"id": "2db70ac0604dda6e24885e77c7de1b07",
				"name": "Tresom",
				"thumb": "public/reference_assets/6.jpg",
				"type": "highlight",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 2,
					"image": 5,
					"video": 0,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "360",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "360",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					}
				],
				"references": [
					{
						"id": "63f1415d008458bd2e0c9c2b2ea59eaf",
						"name": "Tampflex",
						"thumb": "public/reference_assets/9.jpg",
						"type": "complex",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "2db70ac0604dda6e24885e77c7de1b07",
							"reference_child_id": "63f1415d008458bd2e0c9c2b2ea59eaf"
						}
					},
					{
						"id": "971848ea71ec5e3acdd97987d1ce834e",
						"name": "Tresom",
						"thumb": "public/reference_assets/14.jpg",
						"type": "complex",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "2db70ac0604dda6e24885e77c7de1b07",
							"reference_child_id": "971848ea71ec5e3acdd97987d1ce834e"
						}
					},
					{
						"id": "f514fe7866f5cd78114df91822f961ab",
						"name": "Namfix",
						"thumb": "public/reference_assets/11.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "2db70ac0604dda6e24885e77c7de1b07",
							"reference_child_id": "f514fe7866f5cd78114df91822f961ab"
						}
					},
					{
						"id": "2f15bee83feb858f12e4084e0bc42188",
						"name": "Holdlamis",
						"thumb": "public/reference_assets/3.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "2db70ac0604dda6e24885e77c7de1b07",
							"reference_child_id": "2f15bee83feb858f12e4084e0bc42188"
						}
					},
					{
						"id": "a823acf264a631f13015224c30b07f32",
						"name": "Bigtax",
						"thumb": "public/reference_assets/4.jpg",
						"type": "reference",
						"customer": {
							"id": "2afdeebd6e95986c47b62796c2be5007",
							"name": "Kamba"
						},
						"asset_count": {
							"360": 0,
							"image": 0,
							"video": 0,
							"document": 0
						},
						"assets": [],
						"references": [],
						"pivot": {
							"reference_parent_id": "2db70ac0604dda6e24885e77c7de1b07",
							"reference_child_id": "a823acf264a631f13015224c30b07f32"
						}
					}
				]
			},
			{
				"id": "63f1415d008458bd2e0c9c2b2ea59eaf",
				"name": "Tampflex",
				"thumb": "public/reference_assets/9.jpg",
				"type": "complex",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 0,
					"image": 7,
					"video": 3,
					"document": 3
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "image",
						"sorting": 6,
						"low": "public/reference_assets/low_7.jpg",
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "video",
						"sorting": 8,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_9.jpg"
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "video",
						"sorting": 9,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_10.jpg"
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.pdf",
						"thumb": "public/reference_assets/11.jpg",
						"type": "document",
						"sorting": 10,
						"low": null,
						"video_poster": null
					},
					{
						"id": 12,
						"filename": "public/reference_assets/12.pdf",
						"thumb": "public/reference_assets/12.jpg",
						"type": "document",
						"sorting": 11,
						"low": null,
						"video_poster": null
					},
					{
						"id": 13,
						"filename": "public/reference_assets/13.pdf",
						"thumb": "public/reference_assets/13.jpg",
						"type": "document",
						"sorting": 12,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "971848ea71ec5e3acdd97987d1ce834e",
				"name": "Tresom",
				"thumb": "public/reference_assets/14.jpg",
				"type": "complex",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 1,
					"image": 0,
					"video": 1,
					"document": 1
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.mp4",
						"thumb": "public/reference_assets/1.jpg",
						"type": "video",
						"sorting": 0,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_1.jpg"
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.pdf",
						"thumb": "public/reference_assets/3.jpg",
						"type": "document",
						"sorting": 2,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "f514fe7866f5cd78114df91822f961ab",
				"name": "Namfix",
				"thumb": "public/reference_assets/11.jpg",
				"type": "reference",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 3,
					"image": 6,
					"video": 2,
					"document": 1
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.mp4",
						"thumb": "public/reference_assets/7.jpg",
						"type": "video",
						"sorting": 6,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_7.jpg"
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.mp4",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.jpg",
						"thumb": "public/reference_assets/9.jpg",
						"type": "360",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.jpg",
						"thumb": "public/reference_assets/10.jpg",
						"type": "360",
						"sorting": 9,
						"low": null,
						"video_poster": null
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.jpg",
						"thumb": "public/reference_assets/11.jpg",
						"type": "360",
						"sorting": 10,
						"low": null,
						"video_poster": null
					},
					{
						"id": 12,
						"filename": "public/reference_assets/12.pdf",
						"thumb": "public/reference_assets/12.jpg",
						"type": "document",
						"sorting": 11,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "2f15bee83feb858f12e4084e0bc42188",
				"name": "Holdlamis",
				"thumb": "public/reference_assets/3.jpg",
				"type": "reference",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 0,
					"image": 7,
					"video": 3,
					"document": 0
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 5,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "image",
						"sorting": 6,
						"low": "public/reference_assets/low_7.jpg",
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.mp4",
						"thumb": "public/reference_assets/8.jpg",
						"type": "video",
						"sorting": 7,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_8.jpg"
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.mp4",
						"thumb": "public/reference_assets/9.jpg",
						"type": "video",
						"sorting": 8,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_9.jpg"
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.mp4",
						"thumb": "public/reference_assets/10.jpg",
						"type": "video",
						"sorting": 9,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_10.jpg"
					}
				],
				"references": []
			},
			{
				"id": "a823acf264a631f13015224c30b07f32",
				"name": "Bigtax",
				"thumb": "public/reference_assets/4.jpg",
				"type": "reference",
				"customer": {
					"id": "2afdeebd6e95986c47b62796c2be5007",
					"name": "Kamba"
				},
				"asset_count": {
					"360": 1,
					"image": 1,
					"video": 0,
					"document": 4
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.pdf",
						"thumb": "public/reference_assets/3.jpg",
						"type": "document",
						"sorting": 2,
						"low": null,
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.pdf",
						"thumb": "public/reference_assets/4.jpg",
						"type": "document",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "document",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "793e983bd918a9d1aef5caf586f5460d",
				"name": "Aerified",
				"thumb": "public/reference_assets/7.jpg",
				"type": "complex",
				"customer": {
					"id": "2956da162da0a61e2fabb50e27b99358",
					"name": "Shuffletag"
				},
				"asset_count": {
					"360": 3,
					"image": 0,
					"video": 1,
					"document": 5
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.pdf",
						"thumb": "public/reference_assets/1.jpg",
						"type": "video",
						"sorting": 0,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_1.jpg"
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "360",
						"sorting": 1,
						"low": null,
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "360",
						"sorting": 2,
						"low": null,
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "360",
						"sorting": 3,
						"low": null,
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "document",
						"sorting": 4,
						"low": null,
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 5,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "5c6adaa3da67cb12d4d76898a91e943d",
				"name": "Solarbreeze",
				"thumb": "public/reference_assets/8.jpg",
				"type": "reference",
				"customer": {
					"id": "2956da162da0a61e2fabb50e27b99358",
					"name": "Shuffletag"
				},
				"asset_count": {
					"360": 0,
					"image": 3,
					"video": 3,
					"document": 4
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.pdf",
						"thumb": "public/reference_assets/4.jpg",
						"type": "video",
						"sorting": 3,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_4.jpg"
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.pdf",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.pdf",
						"thumb": "public/reference_assets/10.jpg",
						"type": "document",
						"sorting": 9,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "09982eef2de1b6f04b4b523038f479a5",
				"name": "Y-Solowarm",
				"thumb": "public/reference_assets/10.jpg",
				"type": "complex",
				"customer": {
					"id": "2956da162da0a61e2fabb50e27b99358",
					"name": "Shuffletag"
				},
				"asset_count": {
					"360": 3,
					"image": 8,
					"video": 3,
					"document": 3
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.jpg",
						"thumb": "public/reference_assets/6.jpg",
						"type": "image",
						"sorting": 7,
						"low": "public/reference_assets/low_6.jpg",
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.jpg",
						"thumb": "public/reference_assets/8.jpg",
						"type": "image",
						"sorting": 7,
						"low": "public/reference_assets/low_8.jpg",
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.mp4",
						"thumb": "public/reference_assets/9.jpg",
						"type": "video",
						"sorting": 8,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_9.jpg"
					},
					{
						"id": 10,
						"filename": "public/reference_assets/10.mp4",
						"thumb": "public/reference_assets/10.jpg",
						"type": "video",
						"sorting": 9,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_10.jpg"
					},
					{
						"id": 11,
						"filename": "public/reference_assets/11.mp4",
						"thumb": "public/reference_assets/11.jpg",
						"type": "video",
						"sorting": 10,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_11.jpg"
					},
					{
						"id": 12,
						"filename": "public/reference_assets/12.jpg",
						"thumb": "public/reference_assets/12.jpg",
						"type": "360",
						"sorting": 11,
						"low": null,
						"video_poster": null
					},
					{
						"id": 13,
						"filename": "public/reference_assets/13.jpg",
						"thumb": "public/reference_assets/13.jpg",
						"type": "360",
						"sorting": 12,
						"low": null,
						"video_poster": null
					},
					{
						"id": 14,
						"filename": "public/reference_assets/14.jpg",
						"thumb": "public/reference_assets/14.jpg",
						"type": "360",
						"sorting": 13,
						"low": null,
						"video_poster": null
					},
					{
						"id": 15,
						"filename": "public/reference_assets/6.pdf",
						"thumb": "public/reference_assets/6.jpg",
						"type": "document",
						"sorting": 14,
						"low": null,
						"video_poster": null
					},
					{
						"id": 16,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 15,
						"low": null,
						"video_poster": null
					},
					{
						"id": 17,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 16,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "95ff9c3f26c9899579c9e1f3a1e57505",
				"name": "Bitwolf",
				"thumb": "public/reference_assets/5.jpg",
				"type": "reference",
				"customer": {
					"id": "2956da162da0a61e2fabb50e27b99358",
					"name": "Shuffletag"
				},
				"asset_count": {
					"360": 1,
					"image": 5,
					"video": 1,
					"document": 3
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.jpg",
						"thumb": "public/reference_assets/5.jpg",
						"type": "image",
						"sorting": 4,
						"low": "public/reference_assets/low_5.jpg",
						"video_poster": null
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.mp4",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "360",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.pdf",
						"thumb": "public/reference_assets/7.jpg",
						"type": "document",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			},
			{
				"id": "366a3b46f719ad0f8909a092c96cba81",
				"name": "Trippledex",
				"thumb": "public/reference_assets/10.jpg",
				"type": "complex",
				"customer": {
					"id": "2956da162da0a61e2fabb50e27b99358",
					"name": "Shuffletag"
				},
				"asset_count": {
					"360": 1,
					"image": 4,
					"video": 2,
					"document": 2
				},
				"assets": [
					{
						"id": 1,
						"filename": "public/reference_assets/1.jpg",
						"thumb": "public/reference_assets/1.jpg",
						"type": "image",
						"sorting": 0,
						"low": "public/reference_assets/low_1.jpg",
						"video_poster": null
					},
					{
						"id": 2,
						"filename": "public/reference_assets/2.jpg",
						"thumb": "public/reference_assets/2.jpg",
						"type": "image",
						"sorting": 1,
						"low": "public/reference_assets/low_2.jpg",
						"video_poster": null
					},
					{
						"id": 3,
						"filename": "public/reference_assets/3.jpg",
						"thumb": "public/reference_assets/3.jpg",
						"type": "image",
						"sorting": 2,
						"low": "public/reference_assets/low_3.jpg",
						"video_poster": null
					},
					{
						"id": 4,
						"filename": "public/reference_assets/4.jpg",
						"thumb": "public/reference_assets/4.jpg",
						"type": "image",
						"sorting": 3,
						"low": "public/reference_assets/low_4.jpg",
						"video_poster": null
					},
					{
						"id": 5,
						"filename": "public/reference_assets/5.mp4",
						"thumb": "public/reference_assets/5.jpg",
						"type": "video",
						"sorting": 4,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_5.jpg"
					},
					{
						"id": 6,
						"filename": "public/reference_assets/6.mp4",
						"thumb": "public/reference_assets/6.jpg",
						"type": "video",
						"sorting": 5,
						"low": null,
						"video_poster": "public/reference_assets/video_poster_6.jpg"
					},
					{
						"id": 7,
						"filename": "public/reference_assets/7.jpg",
						"thumb": "public/reference_assets/7.jpg",
						"type": "360",
						"sorting": 6,
						"low": null,
						"video_poster": null
					},
					{
						"id": 8,
						"filename": "public/reference_assets/8.pdf",
						"thumb": "public/reference_assets/8.jpg",
						"type": "document",
						"sorting": 7,
						"low": null,
						"video_poster": null
					},
					{
						"id": 9,
						"filename": "public/reference_assets/9.pdf",
						"thumb": "public/reference_assets/9.jpg",
						"type": "document",
						"sorting": 8,
						"low": null,
						"video_poster": null
					}
				],
				"references": []
			}
		]
	};
	// the reference object
	window.referenceObj = {};
})(window);
